//
//  CCLabelTTFWithStroke.h
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 10/04/2014.
//
//

#import "CCLabelTTF.h"

@interface CCLabelTTFWithStroke : CCLabelTTF

-(void) setStringWithStroke:(NSString *)str;
-(void) setStringWithStroke:(NSString *)str color:(ccColor3B) color;


@end
