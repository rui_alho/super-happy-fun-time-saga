//
//  HelloWorldLayer.m
//  matchmania
//
//  Created by Rui Alho on 19/07/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#import "ClassicGameScene.h"
#import "AboutLayer.h"
#import "SimpleAudioEngine.h"
#import "AppSpecificValues.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "TimedGameLayer.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

@synthesize state = _state;

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
        
        
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        
        CCSprite * bg;
        if(IS_IPHONE_5){
            bg = [CCSprite spriteWithFile:@"startpage-iphone5.jpg"];
        } else {
            bg = [CCSprite spriteWithFile:@"startpage.jpg"];
        }
        [bg setPosition:ccp(size.width/2, size.height/2)];
        [self addChild:bg z:-100];

		AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
#ifndef ANDROID
        
        [app.tracker set:kGAIScreenName
                   value:@"Home"];
        
        [app.tracker send:[[GAIDictionaryBuilder createAppView] build]];
#else
//        [[DAGoogleAnalytics defaultTracker] sendAppView:@"Home"];
#endif
		//
		// Leaderboards and Achievements
		//
		
		// Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28*kScale];
		
		// Achievement Menu Item using blocks
		CCMenuItemFontWithStroke *itemAchievement = [CCMenuItemFontWithStroke itemWithString:@"Achievements" block:^(id sender) {

#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"Achievements"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"Achievements"];

#endif
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
            
            AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
            [app achievements];
			
		}
									   ];

		// Leaderboard Menu Item using blocks
		CCMenuItemFontWithStroke *itemLeaderboard = [CCMenuItemFontWithStroke itemWithString:@"Leaderboard" block:^(id sender) {
#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"Leaderboard"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"Leaderboard"];

#endif
			[[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
			
            AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
            [app leaderboard];
			
		}
									   ];		        
        

        
        CCMenuItemFontWithStroke *soundOnItem = [CCMenuItemFontWithStroke itemWithString:@"Sound on"];
        CCMenuItemFontWithStroke *soundOffItem = [CCMenuItemFontWithStroke itemWithString:@"Sound off"];
        
        
        BOOL  sound = [SimpleAudioEngine sharedEngine].mute;
        
        CCMenuItemToggle *soundToggleItem = [CCMenuItemToggle itemWithItems:[NSArray arrayWithObjects:soundOnItem,soundOffItem,nil] block:^(id sender) {
            
            
            [SimpleAudioEngine sharedEngine].mute =  !sound;            
            
//            [[SimpleAudioEngine sharedEngine] playEffect:@"Drip.mp3.m4a"];
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            
        }];
        
        if (sound) {
            [soundToggleItem setSelectedIndex:1];
        } else {
            [soundToggleItem setSelectedIndex:0];
        }

        
        
//        CCMenuItem *itemReset = [CCMenuItemFont itemWithString:@"Reset Achievements" block:^(id sender) {
//            [[SimpleAudioEngine sharedEngine] playEffect:@"Drip.mp3.m4a"];
//                    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//                    [app.gameCenterManager resetAchievements];
//
//            }];
        
        CCMenuItemFontWithStroke *itemMore = [CCMenuItemFontWithStroke itemWithString:@"More Apps" block:^(id sender) {
#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"More Apps"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"More Apps"];

#endif
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];

            
#ifndef ANDROID
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.com/apps/alhogames"]];
#else
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"market://search?q=pub:AlhoGames"]];
            
            
#endif
            
        }];
        
        CCMenuItemFontWithStroke *itemAbout = [CCMenuItemFontWithStroke itemWithString:@"About" block:^(id sender) {
#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"About"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"About"];

#endif
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            [[CCDirector sharedDirector] pushScene:[CCTransitionJumpZoom transitionWithDuration:1.0 scene:[AboutLayer scene]]];
            
        }];
        
        CCMenuItemFontWithStroke *timedGame = [CCMenuItemFontWithStroke itemWithString:@"Timed Mode" block:^(id sender) {
#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"Timed Mode"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"Timed Mode"];

#endif
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            [[CCDirector sharedDirector] pushScene:[CCTransitionJumpZoom transitionWithDuration:1.0 scene:[TimedGameLayer scene]]];
            
        }];
        
        CCMenuItemFontWithStroke *levelGame = [CCMenuItemFontWithStroke itemWithString:@"Play" block:^(id sender) {
#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"Levels Mode"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"Levels Mode"];

#endif
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setInteger:kGameModeLevels forKey:@"mode"];
            [defaults synchronize];
            
            app.currentLeaderBoard = kLeaderboardLevels_v1;
            
            [[CCDirector sharedDirector] pushScene:[CCTransitionJumpZoom transitionWithDuration:1.0 scene:[ClassicGameScene scene]]];
            
        }];
        
//        
//        CCMenuItem *itemReview = [CCMenuItemFont itemWithString:@"Review App" block:^(id sender) {
//            NSString* url = [NSString stringWithFormat: @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d", 443772111];
//            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
//            
//        }];
//
//        CCMenuItem *itemEmail = [CCMenuItemFont itemWithString:@"Contact us" block:^(id sender) {
//            NSString *mailurl=[NSString stringWithFormat:
//                               @"mailto:%@?subject=%@%@&body=%@%@",@"contact@alhogames.com",
//                               [NSString stringWithFormat:@"Super Happy Funtime Game v%@",                                
//                                [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]],
//                               @"",@"",@""];
//            [[UIApplication sharedApplication] openURL:
//             [NSURL URLWithString:[mailurl stringByAddingPercentEscapesUsingEncoding:
//                                   NSUTF8StringEncoding]]];
//
//        }];

        
        levelGame.color = ccRED;
        timedGame.color = ccGREEN;
        soundToggleItem.color = ccBLUE;
        itemLeaderboard.color = ccYELLOW;
        itemAchievement.color = ccc3(0, 255, 255);;
        itemAbout.color = ccORANGE;
        itemMore.color = ccMAGENTA;

		CCMenu *menu = [CCMenu menuWithItems:levelGame, timedGame, soundToggleItem, itemLeaderboard, itemAchievement, itemAbout,itemMore, nil];

		
		[menu alignItemsVerticallyWithPadding:10*kScale];
//        [menu alignItemsInColumns:
//            [NSNumber numberWithUnsignedInt:1],
//            [NSNumber numberWithUnsignedInt:2], 
//            [NSNumber numberWithUnsignedInt:1],
//            [NSNumber numberWithUnsignedInt:1], 
//            [NSNumber numberWithUnsignedInt:1], 
//            [NSNumber numberWithUnsignedInt:1], 
//            [NSNumber numberWithUnsignedInt:1], 
//            nil];
		[menu setPosition:ccp( size.width/2, size.height/2 - 70*kScale)];
		
		// Add the menu to the layer
		[self addChild:menu];
        
        
//        CCLayerColor* menuLayer = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 100) width:menu.boundingBox.size.width height:menu.contentSize.height];
//        
//        [menuLayer addChild:menu];
//        [self addChild:menuLayer];
        
	}
	return self;
}



//- (void)adWhirlWillPresentFullScreenModal {
//    
//    if (self.state == kGameStatePlaying) {
//        
//        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
//        
//        [[CCDirector sharedDirector] pause];
//    }
//}
//
//- (void)adWhirlDidDismissFullScreenModal {
//    
//    if (self.state == kGameStatePaused)
//        return;
//    
//    else {
//        self.state = kGameStatePlaying;
//        [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
//        [[CCDirector sharedDirector] resume];
//        
//    }
//}

//- (NSString *)adWhirlApplicationKey {
//    return @"079d13ff447248438d5e01d50a870e03"; //
//}

- (UIViewController *)viewControllerForPresentingModalView {
    //return viewController;    
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    return app.director;
}

//-(void)adjustAdSize {
//	//1
//	[UIView beginAnimations:@"AdResize" context:nil];
//	[UIView setAnimationDuration:0.2];
//	//2
//	CGSize adSize = [adWhirlView actualAdSize];
//	//3
//	CGRect newFrame = adWhirlView.frame;
//	//4
//	newFrame.size.height = adSize.height;
//	
//   	//5 
//    CGSize winSize = [CCDirector sharedDirector].winSize;
//    //6
//	newFrame.size.width = winSize.width;
//	//7
//	newFrame.origin.x = (self.adWhirlView.bounds.size.width - adSize.width)/2;
//    
//    //8 
//	newFrame.origin.y = 0;//(winSize.height - adSize.height);
//	//9
//	adWhirlView.frame = newFrame;
//	//10
//	[UIView commitAnimations];
//}
//
//- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlVieww {
//    //1
//    //[adWhirlView rotateToOrientation:UIInterfaceOrientationLandscapeRight];
//	//2    
//    [self adjustAdSize];
//    
//}

-(void)onEnter {
    //1
//    viewController = [(AppDelegate *)[[UIApplication sharedApplication] delegate] viewController];
    
//    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//
//    //2
//    self.adWhirlView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
//    //3
//    self.adWhirlView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
//    
//    //4
//    [adWhirlView updateAdWhirlConfig];
//    //5
//	CGSize adSize = [adWhirlView actualAdSize];
//    //6
//    CGSize winSize = [CCDirector sharedDirector].winSize;
//    //7
////	self.adWhirlView.frame = CGRectMake((winSize.width/2)-(adSize.width/2),winSize.height-adSize.height,winSize.width,adSize.height);
//    self.adWhirlView.frame = CGRectMake((winSize.width/2)-(adSize.width/2),0,winSize.width,adSize.height);
//    
//    
//    //8
//	self.adWhirlView.clipsToBounds = YES;
//    //9
////    [viewController.view addSubview:adWhirlView];
//    [app.director.view addSubview:adWhirlView];
//    
//    //10
////    [viewController.view bringSubviewToFront:adWhirlView];
//    [app.director.view bringSubviewToFront:adWhirlView];
    
    //11
    [super onEnter];
    
    [Xplode setupPromotionDockForBreakpoint:@"breakpoint2" atPosition:XPLPromotionDockingPositionBottom];
    [Xplode showPromotionDockAndBounce:YES];
}

-(void)onExit {
    //1
//    if (adWhirlView) {
//        [adWhirlView removeFromSuperview];
//        [adWhirlView replaceBannerViewWith:nil];
//        [adWhirlView ignoreNewAdRequests];
//        [adWhirlView setDelegate:nil];
//        self.adWhirlView = nil;
//    }
	[super onExit];
}


@end
