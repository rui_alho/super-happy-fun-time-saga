//
//  ClassicGameScene.h
//  Chainz
//
//  Created by Pedro Gomes on 12/13/11.
//  Copyright (c) 2011 Phluid Labs. All rights reserved.
//

#import "cocos2d.h"

#import "AppDelegate.h"
#import "GADBannerView.h"

@class GameBoard;

@interface ClassicGameScene : CCLayer 
{
	GameBoard	*_gameboard;	
    
    enum GameStatePP _state;
}

@property(nonatomic) enum GameStatePP state;
//@property(nonatomic, strong) GADBannerView *adBanner;

+ (CCScene *) scene;

@end
