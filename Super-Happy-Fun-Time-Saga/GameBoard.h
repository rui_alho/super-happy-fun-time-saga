//
//  GameBoard.h
//  Chainz
//
//  Created by Pedro Gomes on 12/13/11.
//  Copyright (c) 2011 Phluid Labs. All rights reserved.
//

#import "cocos2d.h"
#import "AppDelegate.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
typedef enum {
	GameboardMovementDirectionLeft,
	GameboardMovementDirectionRight,
	GameboardMovementDirectionUp,
	GameboardMovementDirectionDown,
	GameboardMovementDirectionInvalid,
} GameboardMovementDirection;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define GAMEBOARD_NUM_ROWS	8
#define GAMEBOARD_NUM_COLS	8
#define GEM_SPACING (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 40 : 80)

extern const NSInteger 	kGameboardNumberOfRows;
extern const NSInteger 	kGameboardNumberOfCols;
extern const CGSize		kGameboardCellSize;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
@class Gem;
@interface GameBoard : CCLayer
{
	BOOL					_initialized;
	NSInteger				_board[GAMEBOARD_NUM_COLS][GAMEBOARD_NUM_ROWS];
	NSMutableArray			*_gems;
	NSMutableDictionary		*_validMovesLookupTable; // stores all legal swaps for a given point
	NSMutableDictionary		*_legalMovesLookupTable; // stores the legality of every valid swap combination
	
	// test code
	NSMutableArray			*_gemDestructionQueue;
	NSMutableArray			*_gemDropdownQueue;
	NSMutableArray			*_gemGenerationQueue;
    
    CCLabelTTFWithStroke* startLabel;
    CCLabelTTF* endLabel;
    CCLabelTTFWithStroke* scoreLabel;
    CCLabelTTF* scoreLabelBack;
    CCLabelTTFWithStroke* highscoreLabel;
    CCLabelTTF* timerLabel;
    CCMenuItemFontWithStroke *quitItem;
    CCMenuItemFontWithStroke *restartItem;
    CCMenuItemToggle *pauseItem;
//    CCMenuItemFontWithStroke *continueItem;
//    CCMenu *menuEndGame;
    CCMenu *menuPause;
    CCLayerColor* pauseLayer;
    CCLayerColor* helpLayer;
    CCLayerGradient* levelLayer;
    CCLayerGradient* levelStartLayer;
    
    int _score;
    int _levelscore;
    int _updatescore;
    
    CCParticleSystem	*emitter_;
    
    NSTimer* timer;
    NSTimer* helperTimer;
    
    float elapsedTime;
    float vampireTime;
    float vampireAttackTime;
    
//    NSTimer* _updateTimer;
    
    BOOL startGame;
    BOOL endGame;
    BOOL _showLevelEnd;
    
    int _chains;
    
    CCProgressTimer* timeBar;
    
//    BOOL paused;
    
    AppController *app;
    CGSize s;
    
    NSNumberFormatter *formatter;
    
    uint seconds;
    uint minutes;
    uint hours;
    
    BOOL virgin;
    
    CCMenu* helpMenu;
    CCMenu* levelMenu;
    
    CCLabelTTFWithStroke* helpLabel;
    CCLabelTTFWithStroke* levelLabel1;
    CCLabelTTFWithStroke* levelLabel2;
    CCLabelTTFWithStroke* levelLabel3;
    
    int helpCounter;
    
    int levelTime;
    int mode;
    int currentHighScore;
    NSString* highScore;
    
    BOOL _isbusy;
}

@property (readwrite,retain) CCParticleSystem *emitter;
//@property (strong,nonatomic) NSTimer *updateTimer;
@property (nonatomic, readonly) BOOL isBusy;

- (void)resetGameBoard;
- (void)swapGemAtPoint:(CGPoint)gem1 withGemAtPoint:(CGPoint)gem2;
- (BOOL)moveGemAtPoint:(CGPoint)point withDirection:(GameboardMovementDirection)direction;

- (void)clearChain:(CGPoint)point sequence:(NSArray *)sequence;
- (void)generateGemsForClearedCells;

- (void)simulateGameplay;
- (void)printBoard;

-(BOOL)updateGemAtPoint:(CGPoint)point :(NSInteger)color;
-(void)updateBoardAtPoint:(CGPoint)point :(NSInteger)color;
//-(void) tapVampire:(Gem*) gem;
//-(void) tapNinja:(Gem*) gem;
-(void) checkSelections:(Gem*) gemIn;

@end
