//
//  CCDirector+addition.m
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 7/05/2014.
//
//

#import "CCDirector+addition.h"

@implementation CCDirector (addition)

- (void) popSceneWithTransition:(Class)transitionClass duration:(ccTime)t {
    [scenesStack_ removeLastObject];
    
    NSUInteger count = [scenesStack_ count];
    NSAssert(count > 0, @"Don't popScene when there aren't any!");
    
    CCScene* scene = [transitionClass transitionWithDuration:t scene:[scenesStack_ lastObject]];
    [self replaceScene:scene];
}

@end
