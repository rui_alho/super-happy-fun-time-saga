//
//  AppDelegate.m
//  matchmania
//
//  Created by Rui Alho on 19/07/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "cocos2d.h"

#import "AppDelegate.h"
#import "IntroLayer.h"
#import "Toast.h"
#import "SimpleAudioEngine.h"
#import "Appirater.h"
#import "AppSpecificValues.h"
#import "GAI.h"
#ifndef ANDROID
#import "ACTReporter.h"
#endif
#import "iTellAFriend.h"
#import <Parse/Parse.h>

static NSString *const kTrackingId = @"UA-37806946-5";

@implementation AppController

@synthesize window=window_, navController=navController_, director=director_;
@synthesize gameCenterManager;
@synthesize currentScore;
@synthesize currentLeaderBoard;
@synthesize paused = _paused;
@synthesize tracker = tracker_;
//@synthesize multiNinja;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	// Create the main window
	window_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

#ifdef ANDROID
    [UIScreen mainScreen].currentMode = [UIScreenMode emulatedMode:UIScreenBestEmulatedMode];
#endif
    
    /*
     Make sure that the Xplode SDK is initialized is at the top of the method,
     above the root view controller setup.
     */
    NSString *appHandle = @"SuperHappyFunTimeSaga";
    NSString *appSecret = @"f666896701c65a1a92a3eacae60b375d";
    [Xplode initializeWithAppHandle:appHandle appSecret:appSecret onCompletion:^(NSError *error) {
        /* Completion code */
    }];
    
    [Xplode cachePromotionForBreakpoint:@"breakpoint1"];
    [Xplode cachePromotionForBreakpoint:@"breakpoint2"];
    
    [self handleNote:launchOptions];
    
    // Let the device know we want to receive push notifications
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    [Appirater setAppId:@"874705889"];
    
    [Parse setApplicationId:@"eALeKCASaHpUYoaFTjYWwCW4bIfVFfpifj9sv2aT"
                  clientKey:@"C9b6abZYqXm0VX6hPHFDUR9nHDSFKQJuuyTSRizz"];
    
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
     UIRemoteNotificationTypeAlert|
     UIRemoteNotificationTypeSound];
    
#ifdef ANDROID
    // Paste instead 'UA-00000000-0' your trackerId value
//    [[DAGoogleAnalytics defaultTracker] setTrackerId:kTrackingId];
    
#else
    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];
    
#endif
        
	// Create an CCGLView with a RGB565 color buffer, and a depth buffer of 0-bits
	CCGLView *glView = [CCGLView viewWithFrame:[window_ bounds]
								   pixelFormat:kEAGLColorFormatRGBA8//kEAGLColorFormatRGB565	//kEAGLColorFormatRGBA8
								   depthFormat:0	//GL_DEPTH_COMPONENT24_OES
							preserveBackbuffer:NO
									sharegroup:nil
								 multiSampling:NO
							   numberOfSamples:0];
    

    [glView setMultipleTouchEnabled:NO];
    
	director_ = (CCDirectorIOS*) [CCDirector sharedDirector];

	director_.wantsFullScreenLayout = YES;

	// Display FSP and SPF
	[director_ setDisplayStats:NO];

	// set FPS at 60
	[director_ setAnimationInterval:1.0/60];

	// attach the openglView to the director
	[director_ setView:glView];

	// for rotation and other messages
	[director_ setDelegate:self];

	// 2D projection
	[director_ setProjection:kCCDirectorProjection2D];
//	[director_ setProjection:kCCDirectorProjection3D];

	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director_ enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");

	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

	// If the 1st suffix is not found and if fallback is enabled then fallback suffixes are going to searched. If none is found, it will try with the name without suffix.
	// On iPad HD  : "-ipadhd", "-ipad",  "-hd"
	// On iPad     : "-ipad", "-hd"
	// On iPhone HD: "-hd"
	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
	[sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
	[sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
	[sharedFileUtils setiPadSuffix:@"-ipad"];					// Default on iPad is "ipad"
	[sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"

	// Assume that PVR images have premultiplied alpha
	[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];

	// and add the scene to the stack. The director will run it when it automatically when the view is displayed.
	[director_ pushScene: [IntroLayer scene]]; 

	
	// Create a Navigation Controller with the Director
	navController_ = [[UINavigationController alloc] initWithRootViewController:director_];
	navController_.navigationBarHidden = YES;
    
    


    CGPoint origin = CGPointMake(0.0,
                                 0.0);
    // Use predefined GADAdSize constants to define the GADBannerView.
    self.adBanner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:origin];
    
    // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID before compiling.
#ifdef ANDROID
    self.adBanner.adUnitID = kAndroidBanner;
#else
    self.adBanner.adUnitID = kBannerAdUnitID;
#endif
    self.adBanner.delegate = self;
    self.adBanner.rootViewController = navController_;
    [navController_.view addSubview:self.adBanner];
    [self.adBanner loadRequest:[self request]];
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:(60.0*2) target:self selector:@selector(loadInterstitial) userInfo:nil repeats:YES];
    tellTimer = [NSTimer scheduledTimerWithTimeInterval:(60.0*60) target:self selector:@selector(showTellAFriend) userInfo:nil repeats:NO];
	
	// set the Navigation Controller as the root view controller
//	[window_ addSubview:navController_.view];	// Generates flicker.
	[window_ setRootViewController:navController_];
	
	// make main window visible
	[window_ makeKeyAndVisible];
    
    
    self.currentLeaderBoard = kLeaderboardLevels_v1;
    self.currentScore = 0;
    
    if ([GameCenterManager isGameCenterAvailable]) {
        
        self.gameCenterManager = [[GameCenterManager alloc] init];
        [self.gameCenterManager setDelegate:self];
        [self.gameCenterManager authenticateLocalUser];
        
        
    } else {
        
        // The current device does not support Game Center.
        
    }
    
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    self.currentScore = [defaults integerForKey:@"score"];

    
//    [[SimpleAudioEngine sharedEngine] preloadEffect:@"Steak.mp3"];
//	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Drip.mp3.m4a"];
//	[[SimpleAudioEngine sharedEngine] preloadEffect:@"fx-rotate-block.m4a"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Awesome];
	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Back];
	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Choose];
	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Cool];
	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Eheh];
//	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Play];
	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Sweet];
	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Swoosh];
	[[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Sword];
    [[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Woohoo];
    [[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Yeah];
    [[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Yipee];
    [[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Explosive];
    [[SimpleAudioEngine sharedEngine] preloadEffect:kSFX_Explode];
    
    
    
    
//    [Appirater setDebug:YES];
    
    [Appirater appLaunched:YES];

#ifndef ANDROID
    [ACTConversionReporter reportWithConversionID:@"980891078" label:@"VukICNqMiggQxuvc0wM" value:@"0.000000" isRepeatable:NO];
#endif
	
	return YES;
}

// Supported orientations: Landscape. Customize it for your own needs
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


// getting a call, pause the game
-(void) applicationWillResignActive:(UIApplication *)application
{
	if( [navController_ visibleViewController] == director_ && !self.paused)
		[director_ pause];
}

// call got rejected
-(void) applicationDidBecomeActive:(UIApplication *)application
{
	if( [navController_ visibleViewController] == director_ && !self.paused){
		[director_ resume];        
    }
    application.applicationIconBadgeNumber = 0;
    [Xplode presentPromotionForBreakpoint:@"breakpoint1" onCompletion:nil];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	if( [navController_ visibleViewController] == director_ )
		[director_ stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
    [Appirater appEnteredForeground:YES];
	if( [navController_ visibleViewController] == director_ )
		[director_ startAnimation];
}

// application will be killed
- (void)applicationWillTerminate:(UIApplication *)application
{
	CC_DIRECTOR_END();
    _adBanner.delegate = nil;
    interstitial_.delegate = nil;
}

// purge memory
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    NSLog(@"My token is: %@", deviceToken);
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"1 didReceiveRemoteNotification: %@", userInfo);
    
    [self handleNote:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"2 didReceiveRemoteNotification: %@", userInfo);
    
    [self handleNote:userInfo];
    
    
    
}


-(void) handleNote:(NSDictionary *)userInfo
{
    //example: { "alert": "Hello Rui", "alertID": "multiNinja1", "sound": "yipee.caf", "badge": "Increment" }
 
    NSDictionary* aps = [userInfo valueForKey:@"aps"];
    
    if(!aps) return;
    
    [PFPush handlePush:userInfo];
    
    NSString* alertID = [userInfo valueForKey:@"alertID"];
    
    NSString* alert = [aps valueForKey:@"alert"];
    
    //Only show alerts once
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(alertID && ![defaults boolForKey:alertID]){
        
        [defaults setBool:YES forKey:alertID];
        [defaults synchronize];
        
        
        if([alertID hasPrefix:@"multiNinja"]){
            
            int multiNinja = [defaults integerForKey:@"multiNinja"];
            
            
            NSString* number = [userInfo valueForKey:@"number"];
            
            if(number){
                multiNinja+=[number intValue];
            } else {
                multiNinja++;
            }
            
            [self addMultiNinja:multiNinja];
            
            
            UIAlertView* hello = [[UIAlertView alloc] initWithTitle:@"Booster Time!" message:alert delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [hello show];
            
#ifndef ANDROID
            [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"game_action"
                                                                      action:@"notification unlock"
                                                                       label:@"Multi Ninja"
                                                                       value:nil] build]];
#endif
            
        } else if([alertID hasPrefix:@"redNinja"]){
            
            int redNinja = [defaults integerForKey:@"redNinja"];
            NSString* number = [userInfo valueForKey:@"number"];
            
            if(number){
                redNinja+=[number intValue];
            } else {
                redNinja++;
            }
            
            [self addRedNinja:redNinja];
            
            UIAlertView* hello = [[UIAlertView alloc] initWithTitle:@"Booster Time!" message:alert delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [hello show];
            
#ifndef ANDROID
            [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"game_action"
                                                                       action:@"notification unlock"
                                                                        label:@"Red Ninja"
                                                                        value:nil] build]];
#endif
            
        } else if([alertID hasPrefix:@"greenNinja"]){
            
            int greenNinja = [defaults integerForKey:@"greenNinja"];
            NSString* number = [userInfo valueForKey:@"number"];
            
            if(number){
                greenNinja+=[number intValue];
            } else {
                greenNinja++;
            }
            
            [self addGreenNinja:greenNinja];
            
            UIAlertView* hello = [[UIAlertView alloc] initWithTitle:@"Booster Time!" message:alert delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [hello show];

#ifndef ANDROID
            [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"game_action"
                                                                       action:@"notification unlock"
                                                                        label:@"Green Ninja"
                                                                        value:nil] build]];
#endif
        }
        
        
    }

}

-(void) addMultiNinja:(int)number
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int multiNinja = [defaults integerForKey:@"multiNinja"];
    multiNinja+=number;
    [defaults setInteger:multiNinja forKey:@"multiNinja"];
    [defaults synchronize];
    NSLog(@"multiNinja == %d",multiNinja);

}

-(void) addRedNinja:(int)number
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int redNinja = [defaults integerForKey:@"redNinja"];
    redNinja+=number;
    [defaults setInteger:redNinja forKey:@"redNinja"];
    [defaults synchronize];
    NSLog(@"redNinja == %d",redNinja);
    
}

-(void) addGreenNinja:(int)number
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int greenNinja = [defaults integerForKey:@"greenNinja"];
    greenNinja+=number;
    [defaults setInteger:greenNinja forKey:@"greenNinja"];
    [defaults synchronize];
    NSLog(@"greenNinja == %d",greenNinja);
    
}

#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
    [[self navController] dismissModalViewControllerAnimated:YES];
    
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    [[self navController] dismissModalViewControllerAnimated:YES];
}

- (void) checkAchievements
{
    
}

-(void) leaderboard
{
 
    GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init];
    if (leaderboardController != NULL) {
        leaderboardController.category = self.currentLeaderBoard;
        leaderboardController.timeScope = GKLeaderboardTimeScopeWeek;
        leaderboardController.leaderboardDelegate = self; 
        [[self navController] presentModalViewController:leaderboardController animated:YES];
    }
    
}

-(void) achievements
{
    
    GKAchievementViewController *achievements = [[GKAchievementViewController alloc] init];
    if (achievements != NULL)
    {
        achievements.achievementDelegate = self;
        [[self navController] presentModalViewController:achievements animated:YES];
    }
    
}

- (void) submitScore
{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    self.currentScore = [defaults integerForKey:@"score"];
    
	if(self.currentScore > 0)
	{
		[self.gameCenterManager reportScore: self.currentScore forCategory: self.currentLeaderBoard];
	}
}

- (void) achievementSubmitted: (GKAchievement*) ach error:(NSError*) error;
{
    
	if((error == NULL) && (ach != NULL))
	{
		if (ach.percentComplete == 100.0) {
//			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Achievement Earned!" 
//                                                            message:(@"%@",ach.identifier)
//                                                           delegate:nil 
//                                                  cancelButtonTitle:@"OK" 
//                                                  otherButtonTitles:nil];
//			[alert show];
            
            
//            [self displayToast:@"Achievement Earned!" :CGPointMake(s.width/2, s.height/2)];
            
            CGSize s = [[CCDirector sharedDirector] winSize];
            NSString* message = [NSString stringWithFormat:@"Achievement\nEarned!"];
            Toast* label = [Toast labelWithString:message fontName:@"Marker Felt" fontSize:30*kScale];
            [label setStringWithStroke:message];
            label.position = CGPointMake(s.width/2, s.height/2);
            label.color = ccORANGE;
            [[[CCDirector sharedDirector] runningScene] addChild:label z:101];
            [label runAction: [CCSequence actions:
                               [CCFadeIn actionWithDuration:0.5],
                               [CCDelayTime actionWithDuration:1.0],
                               [CCFadeOut actionWithDuration:0.5],
                               [CCCallFunc actionWithTarget:label selector:@selector(removeFromParent)],
                               nil]];
            
            [self addRandomBooster];

#ifndef ANDROID
            [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"game_action"
                                                                       action:@"Achievement Earned"
                                                                        label:ach.description
                                                                        value:nil] build]];
#endif
            
		}
		
	}
	else
	{
		// Achievement Submission Failed.
        
	}
    
}

-(void) addRandomBooster
{
    int rand = arc4random()%3;
    CGSize s = [[CCDirector sharedDirector] winSize];
    
    
    Toast* label = [Toast labelWithString:@"" fontName:@"Marker Felt" fontSize:30*kScale];
    label.position = CGPointMake(s.width/2, s.height/3);
//    label.opacity = 0;

    
    switch (rand) {
        case 0:
            [self addMultiNinja:1];
            [label setStringWithStroke:@"+1 Multi Ninja\nBooster"];
            label.color = ccWHITE;
            [[[CCDirector sharedDirector] runningScene] addChild:label z:101];
            [label runAction: [CCSequence actions:
                               [CCFadeIn actionWithDuration:0.5],
                               [CCDelayTime actionWithDuration:1.0],
                               [CCFadeOut actionWithDuration:0.5],
                               [CCCallFunc actionWithTarget:label selector:@selector(removeFromParent)],
                               nil]];
            
#ifndef ANDROID
            [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"game_action"
                                                                      action:@"booster unlock"
                                                                       label:@"Multi Ninja"
                                                                       value:nil] build]];
#endif
            break;
        case 1:
            [self addRedNinja:1];
            [label setStringWithStroke:@"+1 Red Ninja\nBooster"];
            label.color = ccRED;
            [[[CCDirector sharedDirector] runningScene] addChild:label z:101];
            [label runAction: [CCSequence actions:
                               [CCFadeIn actionWithDuration:0.5],
                               [CCDelayTime actionWithDuration:1.0],
                               [CCFadeOut actionWithDuration:0.5],
                               [CCCallFunc actionWithTarget:label selector:@selector(removeFromParent)],
                               nil]];
#ifndef ANDROID
            [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"game_action"
                                                                      action:@"booster unlock"
                                                                       label:@"Red Ninja"
                                                                       value:nil] build]];
#endif
            break;
        case 2:
            [self addGreenNinja:1];
            [label setStringWithStroke:@"+1 Green Ninja\nBooster"];
            label.color = ccGREEN;
            [[[CCDirector sharedDirector] runningScene] addChild:label z:101];
            [label runAction: [CCSequence actions:
                               [CCFadeIn actionWithDuration:0.5],
                               [CCDelayTime actionWithDuration:1.0],
                               [CCFadeOut actionWithDuration:0.5],
                               [CCCallFunc actionWithTarget:label selector:@selector(removeFromParent)],
                               nil]];
#ifndef ANDROID
            [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"game_action"
                                                                      action:@"booster unlock"
                                                                       label:@"Green Ninja"
                                                                       value:nil] build]];
#endif
            break;
            
        default:
            break;
    }

}


#pragma mark GADRequest generation

//#ifndef ANDROID
- (GADRequest *)request {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as well as any devices
    // you want to receive test ads.
    request.testDevices = @[
                            // TODO: Add your device/simulator test identifiers here. Your device identifier is printed to
                            // the console when the app is launched.
                            GAD_SIMULATOR_ID,
//                            @"fcb57f4edf39255821ad45abbe51ae2c"
                            ];
    return request;
}
//#endif

#pragma mark GADBannerViewDelegate implementation

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    //    NSLog(@"Received ad successfully");
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    //    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
}

#pragma mark GADInterstitialDelegate implementation

- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
//    [self showInterstitial];
}

- (void)interstitial:(GADInterstitial *)interstitial didFailToReceiveAdWithError:(GADRequestError *)error {
    
    
}

-(void) interstitialDidDismissScreen:(GADInterstitial *)ad
{
    interstitial_ = nil;
}



- (void)loadInterstitial {
    
//#ifndef ANDROID
//    NSLog(@"loadInterstitial");
    // Create a new GADInterstitial each time. A GADInterstitial will only show one request in its
    // lifetime. The property will release the old one and set the new one.
    interstitial_ = [[GADInterstitial alloc] init];
    interstitial_.delegate = self;
    
    // Note: Edit SampleConstants.h to update kSampleAdUnitId with your interstitial ad unit id.
#ifdef ANDROID
    interstitial_.adUnitID = kAndroidInter;
#else
    interstitial_.adUnitID = kInterAdUnitID;
#endif
    
    [interstitial_ loadRequest:[self request]];
//#endif
}


//- (void)showInterstitialAd
//{
//#ifdef ANDROID
////    [[DAMmedia interstitial] showAdAndLoadNextAd];
//#endif
//}

- (void)showInterstitial {
//    NSLog(@"showInterstitial");
    // Show the interstitial.
//#ifndef ANDROID
    [interstitial_ presentFromRootViewController:navController_];
//#else
//    [self showInterstitialAd];
//#endif
}


-(void) showTellAFriend
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(![defaults boolForKey:@"tellafriend"]){
        if( [navController_ visibleViewController] == director_ && !self.paused)
        {
            [director_ pause];
        }
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Tell a friend" message:@"Would you like to tell your friends about Super Happy Fun Time Saga?" delegate:self cancelButtonTitle:@"No thanks" otherButtonTitles:@"Sure", nil];
        [alert show];
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"tellafriend"];
        [defaults synchronize];
        
        [self tellAFriend];
    } else if(buttonIndex == alertView.cancelButtonIndex){
        if( director_.isPaused ){
            [director_ resume];
        }
    }
}

-(void) tellAFriend
{
#ifndef ANDROID
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:@"tellAFriend"
                                                           value:nil] build]];
#else
//    [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                     action:@"button_press"
//                                                      label:@"tellAFriend"];

#endif
    if ([[iTellAFriend sharedInstance] canTellAFriend]) {
        UINavigationController* tellAFriendController = [[iTellAFriend sharedInstance] tellAFriendController];
        [self.navController presentModalViewController:tellAFriendController animated:YES];
    }
}


#ifdef ANDROID
- (void)buttonUpWithEvent:(UIEvent *)event
{
    switch (event.buttonCode)
    {
        case UIEventButtonCodeMenu:
            break;
            
        case UIEventButtonCodeBack:
            // Back button is pressed
            // Pop current scene
            //            [[CCDirector sharedDirector] popScene];
            if( [CCDirector sharedDirector].isPaused ){
                [[CCDirector sharedDirector] resume];
            }
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
            [[CCDirector sharedDirector] popSceneWithTransition:[CCTransitionJumpZoom class] duration:1.0];
            
            // Terminate the app if scene stack is empty
            if([[CCDirector sharedDirector] runningScene] == nil) {
                exit(0);
            }
            break;
        default:
            break;
    }
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}
#endif


@end

