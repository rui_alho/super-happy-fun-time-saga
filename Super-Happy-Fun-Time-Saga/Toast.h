//
//  Toast.h
//  matchmania
//
//  Created by Rui Alho on 23/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCLabelTTF.h"

@interface Toast : CCLabelTTFWithStroke

- (void) removeFromParent;

@end
