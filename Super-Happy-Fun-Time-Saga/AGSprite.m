//
//  AGSprite.m
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 11/04/2014.
//
//

#import "AGSprite.h"

@implementation AGSprite

- (void) removeFromParent{
    CCNode *parent=self.parent;
    [parent removeChild:self cleanup:YES];
}

@end
