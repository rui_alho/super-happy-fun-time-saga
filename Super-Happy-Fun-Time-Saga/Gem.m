//
//  Gem.m
//  Chainz
//
//  Created by Pedro Gomes on 12/13/11.
//  Copyright (c) 2011 Phluid Labs. All rights reserved.
//

#import "Gem.h"
#import "GameBoard.h"
#import "CCDrawingPrimitives.h"
#import "CCParticleSystem.h"
#import "SimpleAudioEngine.h"

////////////////////////////////////////////////////////////////////////////////
// Utility functions
////////////////////////////////////////////////////////////////////////////////
//NSString *GemKindString(GemKind kind) 
//{
//	switch(kind)
//	{
//		case GemKindNormal:			return @"normal";
//		case GemKindEmpty:			return @"empty";
//		case GemKindSpecialPower:	return @"special";
//		default: return [NSString stringWithFormat:@"unknown gem kind %d", kind];
//	}
//}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//NSString *GemColorString(GemColor color) 
//{
//	switch(color) 
//	{
//		case GemColorBlue: 		return @"blue";
//		case GemColorRed: 		return @"red";
//		case GemColorGreen: 	return @"green";
////		case GemColorMagenta: 	return @"magenta";
////		case GemColorOrange: 	return @"orange";
////		case GemColorPurple: 	return @"purple";
//		case GemColorWhite: 	return @"white";
//		case GemColorYellow: 	return @"yellow";
//        case GemColorBlack: 	return @"black";
//        case GemColorGrey:      return @"grey";
//		default: return [NSString stringWithFormat:@"unknown gem color %d", color];
//	}
//}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
@implementation Gem

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
@synthesize kind 		= _kind;
@synthesize gemColor 	= _color;
@synthesize attributes	= _attributes;
@synthesize point		= _point;
@synthesize selected	= _selected;
@synthesize emitter     = emitter_;
@synthesize active      = _active;
@synthesize points      = _points;
@synthesize spriteColor;
@synthesize streak      = _streak;
//@synthesize remove      = _remove;
@synthesize directionH = _directionH;
@synthesize needsUpdate = _needsUpdate;
@synthesize showVamp    = _showVamp;

#pragma mark - Dealloc and Initialization
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//- (void)dealloc
//{
//	[super dealloc];
//}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (id)initWithGameboard:(GameBoard *)gameboard position:(CGPoint)point kind:(GemKind)kind
{
	return [self initWithGameboard:gameboard position:point kind:kind color:arc4random()%GemColorCount attributes:GemBoostNone];
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (id)initWithGameboard:(GameBoard *)gameboard position:(CGPoint)point kind:(GemKind)kind color:(GemColor)color
{
	return [self initWithGameboard:gameboard position:point kind:kind color:color attributes:GemBoostNone];
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (id)initWithGameboard:(GameBoard *)gameboard position:(CGPoint)point kind:(GemKind)kind color:(GemColor)color attributes:(GemAttribute)attribute
{
	CCTexture2D *texture;// = [[CCTextureCache sharedTextureCache] addImage:@"Tile.png"];
    switch(color) {
        case GemColorRed:       texture = [[CCTextureCache sharedTextureCache] addImage:@"Red.png"]; break;
        case GemColorGreen:     texture = [[CCTextureCache sharedTextureCache] addImage:@"Green.png"]; break;
        case GemColorBlue:      texture = [[CCTextureCache sharedTextureCache] addImage:@"Blue.png"]; break;
        case GemColorYellow:    texture = [[CCTextureCache sharedTextureCache] addImage:@"Yellow.png"]; break;
        case GemColorBlack:     texture = [[CCTextureCache sharedTextureCache] addImage:@"Ninja.png"]; break;
        case GemColorMagenta:     texture = [[CCTextureCache sharedTextureCache] addImage:@"Magenta.png"]; break;
        case GemColorOrange:     texture = [[CCTextureCache sharedTextureCache] addImage:@"Orange.png"]; break;
        case GemColorCyan:     texture = [[CCTextureCache sharedTextureCache] addImage:@"Cyan.png"]; break;
        case GemColorPurple:     texture = [[CCTextureCache sharedTextureCache] addImage:@"Purple.png"]; break;
        case GemColorWhite:     texture = [[CCTextureCache sharedTextureCache] addImage:@"Vampire.png"]; break;
        case GemColorGrey:     texture = [[CCTextureCache sharedTextureCache] addImage:@"Skull.png"]; break;
        case GemColorRedNinja:     texture = [[CCTextureCache sharedTextureCache] addImage:@"NinjaRed.png"]; break;
        case GemColorGreenNinja:     texture = [[CCTextureCache sharedTextureCache] addImage:@"NinjaGreen.png"]; break;
        default: break;
    }
    
    
	if((self = [super initWithTexture:texture])) {
		// TODO: load the actual texture/image
		_gameboard 	= gameboard;
		_point 		= point;
		_color		= color;
		_attributes	= attribute;

        _points     = 100;
        
//        ccColor3B spriteColor;
        switch(color) {
            case GemColorRed:       spriteColor = ccRED; break;
            case GemColorGreen:     spriteColor = ccGREEN; break;
            case GemColorBlue:      spriteColor = ccBLUE; break;
            case GemColorYellow:    spriteColor = ccYELLOW; break;
            case GemColorOrange:
            {
                spriteColor = ccORANGE;
//                self.color = ccORANGE;
            }
                break;
            case GemColorMagenta:
            {
                spriteColor = ccMAGENTA;
//                self.color = ccMAGENTA;
            }
                break;
            case GemColorCyan:
            {
                spriteColor = ccc3(0, 255, 255);
//                self.color = ccc3(0, 255, 255);
            }
                break;
            case GemColorPurple:
            {
                spriteColor = ccc3(128, 0, 128);
//                self.color = ccc3(128, 0, 128);
            }
                break;
            case GemColorBlack:
            {
                spriteColor = ccBLACK;
                _kind = GemKindSpecialPower;
                
                CCTextureCache* cache = [CCTextureCache sharedTextureCache];
                CCTexture2D* newTexture = [cache textureForKey:[NSString stringWithFormat:@"Ninja.png"]];
                
                _streak = [CCMotionStreak streakWithFade:0.5 minSeg:2.0 width:self.boundingBox.size.width color:ccWHITE texture:newTexture];
//                _streak.fastMode = NO;
//                [self addChild:_streak z:1];
                _directionH = YES;
                
            }
                break;
            case GemColorRedNinja:
            {
                spriteColor = ccRED;
                _kind = GemKindSpecialPower;
                _directionH = YES;
                
            }
                break;
            case GemColorGreenNinja:
            {
                spriteColor = ccGREEN;
                _kind = GemKindSpecialPower;
                _directionH = YES;
                
            }
                break;
            case GemColorWhite:
            {
                spriteColor = ccWHITE;
                _kind = GemKindSpecialPower;
                _points     = 1000;
            }
                break;
                
            case GemColorGrey:
                {
                    spriteColor = ccGRAY; 
                    _points     = 1000;
                }
                break;
//            case GemColorWhite:     spriteColor = ccWHITE;
            default: break;
        }
        
//		[self setAnchorPoint:CGPointZero];
//		[self setColor:spriteColor];
//        label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", color] fontName:@"Marker Felt" fontSize:12];
//		[self addChild:label];
        
//        NSLog(@"kind == %d",_kind);
        _active = YES;
        
        
        selectionBlock = [CCLayerColor layerWithColor: ccc4(255, 255, 0, 255) width:self.boundingBox.size.width height:self.boundingBox.size.height];
        [self addChild:selectionBlock z:-1];
        selectionBlock.visible = NO;
        
        
	}
	return self;
}

#pragma mark - CCNode

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)draw
{
	[super draw];
//
//	CGFloat radius = 20.0;
//	CGFloat segments = 10;
//	ccDrawCircle((CGPoint){self.position.x + radius, self.position.y + radius}, radius, 0, segments, YES);
//    NSLog(@"draw");

    
    if(_color == GemColorBlack){
        _streak.position = self.position;
    }
    
    
//    if(_attributes == GemBoostExplosive){
//        self.color = spriteColor;
//    }
    
}

-(void) drawBoundingBox: (CGRect) rect

{
    
    CGPoint vertices[4]={
        
        ccp(rect.origin.x,rect.origin.y),
        
        ccp(rect.origin.x+rect.size.width,rect.origin.y),
        
        ccp(rect.origin.x+rect.size.width,rect.origin.y+rect.size.height),
        
        ccp(rect.origin.x,rect.origin.y+rect.size.height),
        
    };
    
//    ccDrawSolidPoly(vertices, 4, YES);
    ccDrawSolidPoly(vertices, 4, ccc4FFromccc3B(ccRED));
    
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)onEnter
{
//	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    
	[super onEnter];
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)onExit
{
    CCDirector *director = [CCDirector sharedDirector];
	[[director touchDispatcher] removeDelegate:self];
	[super onExit];
}

#pragma mark - CCTargetedTouchDelegate

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    
    if (!_active) {
        return NO;
    }
    _moved = NO;

    
//    [_gameboard.updateTimer invalidate];
//    _gameboard.updateTimer = nil;
    
    CGPoint touchLocation = [touch locationInView:touch.view];
    CGPoint touchLocationFlipped = {touchLocation.x, [[CCDirector sharedDirector] winSize].height - touchLocation.y};
    
    startLocation = touchLocation;
    
    
    if(CGRectContainsPoint(self.boundingBox, touchLocationFlipped)){
        _firstTouchLocation = touchLocationFlipped;
        return YES;
    }
        
    
	return NO;
	
//	CCLOG(@"Gameboard touch location = %@, sprite_frame = %@", NSStringFromCGPoint(touchLocation), NSStringFromCGRect((CGRect){self.position, rect_.size}));
//	CCLOG(@"Gameboard gem index = %@", NSStringFromCGPoint(CoordinatesForWindowLocation(p)));
//	return YES;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
	_moved = YES;
}

//////////////////////////////////////////////////////////////////////////////////
// Note: consider moving the gem touch handling logic to the gameboard itself
// besided the potential improvement in performance (individual gems don't have to handle touches)
// it's probably a much more flexible design
//////////////////////////////////////////////////////////////////////////////////
- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
	if(!_moved || self.kind == GemKindSpecialPower) {
        [self callUpdate];
	}
	else {
		CGPoint endTouchLocation = [touch locationInView:touch.window];
		CGPoint endTouchLocationFlipped = {endTouchLocation.x, [[CCDirector sharedDirector] winSize].height - endTouchLocation.y};
		
		// vertical or horizontal?
		CGFloat horizontalOffset = endTouchLocationFlipped.x - _firstTouchLocation.x;
		CGFloat verticalOffset = endTouchLocationFlipped.y - _firstTouchLocation.y;
		
		GameboardMovementDirection direction = GameboardMovementDirectionInvalid;
		if(fabs(horizontalOffset) >= fabs(verticalOffset)) { // moved horizontally
			if(horizontalOffset > 0) 		direction = GameboardMovementDirectionRight;
			else if(horizontalOffset < 0) 	direction = GameboardMovementDirectionLeft;
		}
		else {
			if(verticalOffset > 0)			direction = GameboardMovementDirectionUp;
			else if(verticalOffset < 0) 	direction = GameboardMovementDirectionDown;
		}
		
		if(![_gameboard moveGemAtPoint:self.point withDirection:direction]) {
//			CCLOGINFO(@"Invalid move %@ => %@", NSStringFromCGPoint(self.point), );
//            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Eheh];
		} else {
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
        }
	}
	
	_firstTouchLocation = CGPointZero;
}

//- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
//{
//    
////    CGPoint touchLocation = [touch locationInView: [touch view]];
////    touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
////    touchLocation = [self convertToNodeSpace:touchLocation];
////    
////    endLocation = touchLocation;
//    
//    
////    id swipeAction;
//    
//    // Compare difference in distance
////    if ((startLocation.x - endLocation.x) < 100 || (startLocation.x - endLocation.x) > 100 ) {
////        // Swipe
////        if ((startLocation.x - endLocation.x) < 100){
////            //left
////            NSLog(@"left");
////            swipeAction = [self changeColourLeft];
////        } else if ((startLocation.x - endLocation.x) > 100){
////            //RIGHT
////            NSLog(@"right");
////            swipeAction = [self changeColourRight];
////        }
////        
////    }
//    
//    
////    if ((startLocation.x - endLocation.x) < 225){
////        NSLog(@"right");
////        swipeAction = [self changeColourRight];
////    } else {
////        NSLog(@"left");
////        swipeAction = [self changeColourLeft];
////    }
////    
////    
////    NSLog(@"diff == %f",(startLocation.x - endLocation.x));
//    
////    swipeAction = [self changeColour];
//    
////    NSLog(@"1_color %d",_color);
//    
////    self.color = ccWHITE;
//    
//    switch(_color) {                        
//        case GemColorRed:      
//        { 
//            _color = GemColorMagenta;
////            self.color = ccMAGENTA;
//        } break;
//        case GemColorMagenta:
//        { 
//            _color = GemColorPurple;
//        } break;
//        case GemColorPurple:
//        {
//            _color = GemColorBlue;
//        } break;
//        case GemColorBlue:     
//        { 
//            _color = GemColorCyan;
////            self.color = ccc3(0, 255, 255);;
//        } break;
//        case GemColorCyan:
//        { 
//            _color = GemColorGreen;
//        } break;
//        case GemColorGreen:
//        { 
//            _color = GemColorYellow;
//        } break;            
//        case GemColorYellow:
//        {
//            _color = GemColorOrange;
////            self.color = ccORANGE;
//        } break;
//        case GemColorOrange:
//        {
//            _color = GemColorRed;
//        } break;
//        case GemColorGrey:
//        {
//            _color = GemColorGrey;
//        } break;
//        default: break;
//    }
////    NSLog(@"2kind == %d",_kind);
//
//    
//    if(_kind == GemKindNormal && _active){
//        float d = 0.25f;
//        id firstAction = [CCOrbitCamera actionWithDuration:d/2 radius:1 deltaRadius:0 angleZ:0 deltaAngleZ:90 angleX:0 deltaAngleX:0];
//        id secondAction = [CCOrbitCamera actionWithDuration:d/2 radius:1 deltaRadius:0 angleZ:270 deltaAngleZ:90 angleX:0 deltaAngleX:0];
//        [self runAction: [CCSequence actions:
//                           firstAction,
//                          [CCCallFunc actionWithTarget:self selector:@selector(changeColour)],
//                           secondAction,
//                          [CCCallFunc actionWithTarget:self selector:@selector(callUpdate)],
//                           nil]];
//    } else if(_kind == GemKindSpecialPower && _active){
//        [self callUpdate];
//    }
//    
////    [self changeColour];
//    
////    if(_color == GemColorBlack || _color == GemColorWhite)
////    {
////        _color = GemColorClear;
////    }
//    
//    
////    [_gameboard updateBoardAtPoint:self.point :_color];
////    
////    //[self performSelector:@selector(updateGrid:) withObject:[NSNumber numberWithInt:index] afterDelay:1.0];
////    
//////    [timer invalidate];
//////    timer = nil;
//////    timer = [NSTimer scheduledTimerWithTimeInterval: 1.00f target: self selector: @selector(updateGrid) userInfo:nil repeats: NO] ;
////    
//////    [self updateGrid];
////    
////    [_gameboard.updateTimer invalidate];
////    _gameboard.updateTimer = nil;
////    
////    if(_color == GemColorBlack || _color == GemColorWhite){
//////        _gameboard.updateTimer = [NSTimer scheduledTimerWithTimeInterval: 0.1f target: self selector: @selector(updateGrid) userInfo:nil repeats: NO] ;
////        if(_color == GemColorBlack){
////            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
//////            float d = 0.25f;
//////            id firstAction = [CCOrbitCamera actionWithDuration:d/2 radius:1 deltaRadius:0 angleZ:0 deltaAngleZ:90 angleX:0 deltaAngleX:0];
//////            id secondAction = [CCOrbitCamera actionWithDuration:d/2 radius:1 deltaRadius:0 angleZ:270 deltaAngleZ:90 angleX:0 deltaAngleX:0];
//////            [self runAction: [CCSequence actions:
//////                              firstAction,
//////                              secondAction,
//////                              nil]];
////            [self runAction: [CCSequence actions:
////                             [CCBlink actionWithDuration:0.4 blinks:10],
////                             [CCCallFunc actionWithTarget:self selector:@selector(removeFromParent)],
////                             nil]];
////            
////        }
////        
////        [self updateGrid];
////    } else {
////        _gameboard.updateTimer = [NSTimer scheduledTimerWithTimeInterval: 0.8f target: self selector: @selector(updateGrid) userInfo:nil repeats: NO] ;
////    }
//    
//    
//	
//	_firstTouchLocation = CGPointZero;
//}

-(void) callUpdate
{
//    [_gameboard updateBoardAtPoint:self.point :_color];
//    [_gameboard.updateTimer invalidate];
//    _gameboard.updateTimer = nil;
    
    if(_color == GemColorBlack || _color == GemColorWhite || _color == GemColorRedNinja || _color == GemColorGreenNinja){
        if(_color == GemColorBlack  && !_gameboard.isBusy){
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
            [self runAction: [CCSequence actions:
                              [CCBlink actionWithDuration:0.4 blinks:10],
                              nil]];
            [self updateGrid];
            
        }
        if(_color == GemColorRedNinja && !_gameboard.isBusy){
            self.zOrder = 103;
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Sword];
            [self runAction: [CCSequence actions:
                              [CCFadeTo actionWithDuration:0.8 opacity:0],
                              nil]];
            [self runAction: [CCSequence actions:
                              [CCScaleTo actionWithDuration:0.8 scale:8],
                              [CCCallFunc actionWithTarget:self selector:@selector(removeFromParent)],
                              nil]];
            [self updateGrid];
        }
        if(_color == GemColorGreenNinja && !_gameboard.isBusy){
            self.zOrder = 102;
//            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
//            [self runAction: [CCSequence actions:
//                              [CCBlink actionWithDuration:0.4 blinks:10],
//                              nil]];
            if(self.point.x < 4){ //clear right
                [self runAction: [CCSequence actions:
                                 [CCMoveTo actionWithDuration:0.4 position:CGPointMake(self.position.x+self.boundingBox.size.width, self.position.y)],
                                 [CCCallFunc actionWithTarget:self selector:@selector(removeFromParent)],
                                 nil]];
                
            } else { //clear left
                [self runAction: [CCSequence actions:
                                 [CCMoveTo actionWithDuration:0.4 position:CGPointMake(self.position.x-self.boundingBox.size.width, self.position.y)],
                                 [CCCallFunc actionWithTarget:self selector:@selector(removeFromParent)],
                                 nil]];
                
            }
            [self updateGrid];
        }
        if(_color == GemColorWhite){
            [self updateGrid];
        }
        
    } else {
//        _gameboard.updateTimer = [NSTimer scheduledTimerWithTimeInterval: 0.8f target: self selector: @selector(updateGrid) userInfo:nil repeats: NO] ;
        [self markSelected:!_selected];
        [_gameboard checkSelections:self];
    }
}

-(void) updateGrid
{
    if (_color == GemColorGrey) {
        return;
    }
    if(![_gameboard updateGemAtPoint:self.point:_color]) {
        CCLOGINFO(@"Invalid move %@ => %@", NSStringFromCGPoint(self.point), );
    }

}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
	_firstTouchLocation = CGPointZero;
}

#pragma mark - Public Methods

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)updatePosition:(CGPoint)point
{
	
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)markSelected:(BOOL)selected
{
	_selected = selected;
	if(_selected) {
		// draw selected mode
        selectionBlock.visible = YES;
	}
	else {
		// clear selected mode
        selectionBlock.visible = NO;
	}
	// notify gameboard
}

#pragma mark - Animations and Effects

//-(CCAction*) changeColourLeft
//{
//    
////	GemColorBlue, //
////	GemColorGreen, //
////	GemColorYellow, //
////	GemColorOrange, //
////	GemColorRed, //
//    
////    ccColor3B spriteColor;
//    switch(_color) {
//        case GemColorRed:      { spriteColor = ccYELLOW; _color = GemColorYellow;} break;
//        case GemColorGreen:    { spriteColor = ccRED; _color = GemColorRed;} break;
//        case GemColorBlue:     { spriteColor = ccGREEN; _color = GemColorGreen;} break;
//        case GemColorYellow:   { spriteColor = ccBLUE; _color = GemColorBlue;} break;
//        case GemColorBlack:     { spriteColor = ccBLACK; _color = GemColorBlack;} break;
//        case GemColorGrey:      { spriteColor = ccGRAY; _color = GemColorGrey;} break;
//        default: break;
//    }
//    //[self setColor:spriteColor];
////    float d = 0.25f;
////    [self runAction:[CCTintTo actionWithDuration:d/2 red:spriteColor.r green:spriteColor.g blue:spriteColor.b]];
//     
//    return [CCTintTo actionWithDuration:0 red:spriteColor.r green:spriteColor.g blue:spriteColor.b];
//}

//-(CCAction*) changeColourRight
//{
//    
//    
////    ccColor3B spriteColor;
//    switch(_color) {
//        case GemColorRed:      { spriteColor = ccGREEN; _color = GemColorGreen;} break;
//        case GemColorGreen:    { spriteColor = ccBLUE; _color = GemColorBlue;} break;
//        case GemColorBlue:     { spriteColor = ccYELLOW; _color = GemColorYellow;} break;
//        case GemColorYellow:   { spriteColor = ccRED; _color = GemColorRed;} break;
//        case GemColorBlack:      { spriteColor = ccBLACK; _color = GemColorBlack;} break;
//        case GemColorGrey:      { spriteColor = ccGRAY; _color = GemColorGrey;} break;
//        default: break;
//    }
//    
//    if(_color == GemColorGrey){
//        [[SimpleAudioEngine sharedEngine] playEffect:@"eheh.caf"];
//    } else {
//        [[SimpleAudioEngine sharedEngine] playEffect:@"fx-rotate-block.m4a"];
//    }
//    
//    return [CCTintTo actionWithDuration:0 red:spriteColor.r green:spriteColor.g blue:spriteColor.b];
//}


-(void) changeColour
{
    
    
    CCTextureCache* cache = [CCTextureCache sharedTextureCache];
    
    CCTexture2D* newTexture;    
    
    switch(_color) {                        
        case GemColorRed:      
        { 
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Red.png"]];
        } break;
        case GemColorGreen:    
        { 
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Green.png"]];
        } break;
        case GemColorBlue:     
        { 
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Blue.png"]];
        } break;
        case GemColorYellow:   
        { 
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Yellow.png"]];
        } break;
        case GemColorBlack:
        {
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Ninja.png"]];
        } break;
        case GemColorRedNinja:
        {
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"NinjaRed.png"]];
        } break;
        case GemColorGreenNinja:
        {
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"NinjaGreen.png"]];
        } break;
        case GemColorWhite:
        {
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Vampire.png"]];
        } break;
        case GemColorMagenta:
        {
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Magenta.png"]];
//            self.color = ccMAGENTA;
        } break;
        case GemColorCyan:
        {
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Cyan.png"]];
//            self.color = ccc3(0, 255, 255);
        } break;
        case GemColorOrange:
        {
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Orange.png"]];
//            self.color = ccORANGE;
        } break;
        case GemColorPurple:
        {
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Purple.png"]];
//            self.color = ccc3(128, 0, 128);
        } break;
        case GemColorGrey:
        { 
            newTexture = [cache textureForKey:[NSString stringWithFormat:@"Skull.png"]];
        } break;            
        default: break;
    }
    
    if(_color == GemColorGrey){
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Eheh];
    } else {
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
    }

    [self setTexture:newTexture];
    
    [_gameboard updateBoardAtPoint:self.point :_color];


}


- (void)wobble
{
//    [gem runAction: [CCSequence actions:
//                     [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:0],
//                     nil]];
    [self runAction: [CCSequence actions:
                     [CCRotateTo actionWithDuration:0.2 angle:22.5],
//                      [CCRotateTo actionWithDuration:0.4 angle:0],
                      [CCRotateTo actionWithDuration:0.4 angle:-22.5],
                      [CCRotateTo actionWithDuration:0.2 angle:0],
                     nil]];
}

- (void)needsExplode
{
    
//    ccGRAY
    id repeat = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                   [CCTintTo actionWithDuration:2.0 red:166 green:166 blue:166],
                                                   [CCTintTo actionWithDuration:2.0 red:255 green:255 blue:255],
                                                   nil]];
    
    [self runAction:repeat];
}


- (void)explode
{
    
//    [super onEnter];
//	self.emitter = [CCParticleExplosion node];
//	[self addChild:emitter_ z:10];
//    
//	emitter_.texture = [[CCTextureCache sharedTextureCache] addImage: @"stars-grayscale.png"];
//    
//	emitter_.autoRemoveOnFinish = YES;
    
    
//    self.emitter = [CCParticleFlower node];
//	[self addChild:emitter_ z:10];
//	emitter_.texture = [[CCTextureCache sharedTextureCache] addImage: @"stars-grayscale.png"];
//    emitter_.duration = 0.3;
//    emitter_.startColor = ccc4FFromccc3B(self.color);
//    emitter_.endColor = ccc4FFromccc3B(self.color);
//    emitter_.startSize = 40.0f;
//	emitter_.startSizeVar = 00.0f;
//	emitter_.endSize = kCCParticleStartSizeEqualToEndSize;
//    emitter_.autoRemoveOnFinish = YES;
//    
//    emitter_.position = ccp(20, 20);
    
    
	
}

- (void)shrink
{
	
}

-(void) makeActive
{
    _active = YES;
}


- (void) removeFromParent{
    CCNode *parent=self.parent;
    [parent removeChild:self cleanup:YES];
}

@end
