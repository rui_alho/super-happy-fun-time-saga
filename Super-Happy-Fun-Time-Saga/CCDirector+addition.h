//
//  CCDirector+addition.h
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 7/05/2014.
//
//

#import "CCDirector.h"

@interface CCDirector (addition)

- (void) popSceneWithTransition:(Class)transitionClass duration:(ccTime)t;


@end
