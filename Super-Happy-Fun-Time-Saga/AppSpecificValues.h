/*
 
 File: AppSpecificValues.h
 Abstract: Basic introduction to GameCenter
 
 Version: 1.0
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple Inc.
 ("Apple") in consideration of your agreement to the following terms, and your
 use, installation, modification or redistribution of this Apple software
 constitutes acceptance of these terms.  If you do not agree with these terms,
 please do not use, install, modify or redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and subject
 to these terms, Apple grants you a personal, non-exclusive license, under
 Apple's copyrights in this original Apple software (the "Apple Software"), to
 use, reproduce, modify and redistribute the Apple Software, with or without
 modifications, in source and/or binary forms; provided that if you redistribute
 the Apple Software in its entirety and without modifications, you must retain
 this notice and the following text and disclaimers in all such redistributions
 of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may be used
 to endorse or promote products derived from the Apple Software without specific
 prior written permission from Apple.  Except as expressly stated in this notice,
 no other rights or licenses, express or implied, are granted by Apple herein,
 including but not limited to any patent rights that may be infringed by your
 derivative works or by other works in which the Apple Software may be
 incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE MAKES NO
 WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
 COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR
 DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF
 CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF
 APPLE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
 */

//These constants are defined in iTunesConnect, and will function as long
//  as this sample is built/run with the existing bundle identifier
//  (com.appledts.GKTapper).  If you want to experiment with this sample and
//  iTunesConnect, you'll need to define you're own bundle ID and iTunes
//  Connect configurations.  This sample uses reverse DNS for Leaderboards
//  and Achievement IDs, but this is not a requirement.  Any string that
//  iTunes Connect will accept will work fine.

//Leaderboard Category IDs

#define LEAD_LEVELS_LEADERBOARD @"CgkIkZ3MmsUUEAIQAQ"
#define LEAD_TIMED_LEADERBOARD_100MIN @"CgkIkZ3MmsUUEAIQAg"
#define LEAD_TIMED_LEADERBOARD_200MIN @"CgkIkZ3MmsUUEAIQAw"
#define LEAD_TIMED_LEADERBOARD_300MIN @"CgkIkZ3MmsUUEAIQBA"


#ifdef ANDROID
#define kLeaderboardLevels_v1 LEAD_LEVELS_LEADERBOARD
#define kLeaderboard1_v1 LEAD_TIMED_LEADERBOARD_100MIN
#define kLeaderboard2_v1 LEAD_TIMED_LEADERBOARD_200MIN
#define kLeaderboard3_v1 LEAD_TIMED_LEADERBOARD_300MIN
#else
#define kLeaderboardLevels_v1 @"shfts_leaderboard_levels"
#define kLeaderboard1_v1 @"shfts_leaderboard_timed_1"
#define kLeaderboard2_v1 @"shfts_leaderboard_timed_2"
#define kLeaderboard3_v1 @"shfts_leaderboard_timed_3"
#endif


//#define kSFX_Play       @"play.m4a"
#define kSFX_Awesome    @"awesome.caf"
#define kSFX_Cool       @"cool.caf"
#define kSFX_Eheh       @"eheh.caf"
#define kSFX_Sweet      @"sweet.caf"
#define kSFX_Woohoo     @"woohoo.caf"
#define kSFX_Yeah       @"yeah.caf"
#define kSFX_Yipee      @"yipee.caf"
#define kSFX_Sword      @"Sword Draw 4.mp3"
#define kSFX_Swoosh     @"Swoosh.mp3"
#define kSFX_Choose     @"Drip.mp3"
#define kSFX_Back       @"Steak.mp3"

#define kSFX_Blah       @"blah.caf"
#define kSFX_Bleeh      @"bleeh.caf"
#define kSFX_Dude       @"dude.caf"
#define kSFX_Go         @"go.caf"
//#define kSFX_LetsGo     @"lets go.caf"
#define kSFX_Nice       @"nice.caf"
#define kSFX_Sup        @"sup.caf"
#define kSFX_Yes        @"yes.caf"
#define kSFX_Yo         @"yo.caf"
#define kSFX_Explosive  @"cartoon019.mp3"
#define kSFX_Explode    @"thunder_1.mp3"



#define ACH_3_CHAIN @"CgkIkZ3MmsUUEAIQBQ"
#define ACH_4_CHAIN @"CgkIkZ3MmsUUEAIQBg"
#define ACH_5_CHAIN @"CgkIkZ3MmsUUEAIQBw"
#define ACH_6_CHAIN @"CgkIkZ3MmsUUEAIQCA"
#define ACH_7_CHAIN @"CgkIkZ3MmsUUEAIQCQ"

#ifdef ANDROID
#define kAchievement3Chain ACH_3_CHAIN
#define kAchievement4Chain ACH_4_CHAIN
#define kAchievement5Chain ACH_5_CHAIN
#define kAchievement6Chain ACH_6_CHAIN
#define kAchievement7Chain ACH_7_CHAIN
#define kAchievement8Chain NULL
#define kAchievement9Chain NULL
#define kAchievement10Chain NULL
#define kAchievement11Chain NULL
#define kAchievement12Chain NULL
#define kAchievement13Chain NULL
#define kAchievement14Chain NULL
#define kAchievement15Chain NULL
#define kAchievement16Chain NULL
#define kAchievement17Chain NULL
#define kAchievement18Chain NULL
#define kAchievement19Chain NULL
#define kAchievement20Chain NULL
#define kAchievement21Chain NULL
#define kAchievement22Chain NULL
#define kAchievement23Chain NULL
#define kAchievement24Chain NULL
#define kAchievement25Chain NULL
#define kAchievement26Chain NULL
#define kAchievement27Chain NULL
#define kAchievement28Chain NULL
#define kAchievement29Chain NULL
#define kAchievement30Chain NULL
#define kAchievement31Chain NULL
#define kAchievement32Chain NULL
#define kAchievement33Chain NULL
#define kAchievement34Chain NULL
#define kAchievement35Chain NULL
#define kAchievement36Chain NULL
#define kAchievement37Chain NULL
#define kAchievement38Chain NULL
#define kAchievement39Chain NULL
#define kAchievement40Chain NULL

#else
//Achievement IDs

//Achievement IDs
#define kAchievement3Chain @"3_chain"
#define kAchievement4Chain @"4_chain"
#define kAchievement5Chain @"5_chain"
#define kAchievement6Chain @"6_chain"
#define kAchievement7Chain @"7_chain"
#define kAchievement8Chain @"8_chain"
#define kAchievement9Chain @"9_chain"
#define kAchievement10Chain @"10_chain"
#define kAchievement11Chain @"11_chain"
#define kAchievement12Chain @"12_chain"
#define kAchievement13Chain @"13_chain"
#define kAchievement14Chain @"14_chain"
#define kAchievement15Chain @"15_chain"
#define kAchievement16Chain @"16_chain"
#define kAchievement17Chain @"17_chain"
#define kAchievement18Chain @"18_chain"
#define kAchievement19Chain @"19_chain"
#define kAchievement20Chain @"20_chain"
#define kAchievement21Chain @"21_chain"
#define kAchievement22Chain @"22_chain"
#define kAchievement23Chain @"23_chain"
#define kAchievement24Chain @"24_chain"
#define kAchievement25Chain @"25_chain"
#define kAchievement26Chain @"26_chain"
#define kAchievement27Chain @"27_chain"
#define kAchievement28Chain @"28_chain"
#define kAchievement29Chain @"29_chain"
#define kAchievement30Chain @"30_chain"
#define kAchievement31Chain @"31_chain"
#define kAchievement32Chain @"32_chain"
#define kAchievement33Chain @"33_chain"
#define kAchievement34Chain @"34_chain"
#define kAchievement35Chain @"35_chain"
#define kAchievement36Chain @"36_chain"
#define kAchievement37Chain @"37_chain"
#define kAchievement38Chain @"38_chain"
#define kAchievement39Chain @"39_chain"
#define kAchievement40Chain @"40_chain"
#endif

#define kGameModeTimedEasy 0
#define kGameModeTimedHard 1
#define kGameModeLevels 2

/*
 Triple (3) (also: triplet, treble, thrice, threesome, troika, trio)
 Quadruple (4)
 Quintuple or Pentuple (5)
 Sextuple or Hextuple (6)
 Septuple (7)
 Octuple (8)
 Nonuple (9)
 Decuple (10)
 Hendecuple or Undecuple (11)
 Duodecuple (12)
 Centuple (100)
*/



