//
//  CCMenuItemFontWithStroke.m
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 10/04/2014.
//
//

#import "CCMenuItemFontWithStroke.h"

@implementation CCMenuItemFontWithStroke

#define kTagStroke 1029384756

-(id) initFromString: (NSString*) value target:(id) rec selector:(SEL) cb
{
    self = [super initFromString:value target:rec selector:cb];
    
    if ([label_ isKindOfClass: [CCLabelTTF class]]) {
        CCRenderTexture * stroke  = [Util createStroke:(CCLabelTTF*)label_ size:3 color:ccBLACK];
        [self addChild:stroke z:-1 tag:kTagStroke];
    }else{
        NSLog(@"Error adding stroke in menu, label_ is not a CCLabelTTF.  This has only been tested on cocos2d 99.5");
    }
    
    return self;
}

+(id) itemWithLabel:(CCNode<CCLabelProtocol,CCRGBAProtocol> *)label block:(void (^)(id))block
{

    id this = [[self alloc] initWithLabel:label block:block];
    
    if ([label isKindOfClass: [CCLabelTTF class]]) {
        CCRenderTexture * stroke  = [Util createStroke:(CCLabelTTF*)label size:3 color:ccBLACK];
        [this addChild:stroke z:-1 tag:kTagStroke];
    }else{
        NSLog(@"Error adding stroke in menu, label_ is not a CCLabelTTF.  This has only been tested on cocos2d 99.5");
    }
    
    return this;
}

+(id) itemWithLabel:(CCNode<CCLabelProtocol,CCRGBAProtocol> *)label color:(ccColor3B) color block:(void (^)(id))block
{
    
    id this = [[self alloc] initWithLabel:label block:block];
    
    if ([label isKindOfClass: [CCLabelTTF class]]) {
        CCRenderTexture * stroke  = [Util createStroke:(CCLabelTTF*)label size:3 color:color];
        [this addChild:stroke z:-1 tag:kTagStroke];
    }else{
        NSLog(@"Error adding stroke in menu, label_ is not a CCLabelTTF.  This has only been tested on cocos2d 99.5");
    }
    
    return this;
}


+(id) itemWithString: (NSString*) value color:(ccColor3B) color block:(void(^)(id sender))block
{
	return [[self alloc] initWithString:value color:color block:block];
}

+(id) itemWithString: (NSString*) value block:(void(^)(id sender))block
{
	return [[self alloc] initWithString:value color:ccBLACK block:block];
}

-(id) initWithString:(NSString *)value block:(void (^)(id))block
{
    return [self initWithString:value color:ccBLACK block:block];
}


-(id) initWithString:(NSString *)value color:(ccColor3B) color block:(void (^)(id))block
{
    self = [super initWithString:value block:block];
    
    if ([label_ isKindOfClass: [CCLabelTTF class]]) {
        CCRenderTexture * stroke  = [Util createStroke:(CCLabelTTF*)label_ size:3 color:color];
        [self addChild:stroke z:-1 tag:kTagStroke];
    }else{
        NSLog(@"Error adding stroke in menu, label_ is not a CCLabelTTF.  This has only been tested on cocos2d 99.5");
    }
    
    return self;
}

-(void) setString:(NSString *)string
{
    [super setString:string];
    
    if ([label_ isKindOfClass: [CCLabelTTF class]]) {
        [self removeChildByTag:kTagStroke cleanup:YES];
        CCRenderTexture * stroke  = [Util createStroke:(CCLabelTTF*)label_ size:3 color:ccBLACK];
        [self addChild:stroke z:-1 tag:kTagStroke];
        
    }else{
        NSLog(@"Error adding stroke in menu, label_ is not a CCLabelTTF.  This has only been tested on cocos2d 99.5");
    }
    
}

@end
