//
//  TimedGameLayer.m
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 12/04/2014.
//
//

#import "TimedGameLayer.h"
#import "ClassicGameScene.h"
#import "HelloWorldLayer.h"

@implementation TimedGameLayer


// Helper class method that creates a Scene with the AboutLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	TimedGameLayer *layer = [TimedGameLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
    // always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
        // ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];

#ifndef ANDROID
        AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
        
        [app.tracker set:kGAIScreenName
                   value:@"Timed Game"];
        
        [app.tracker send:[[GAIDictionaryBuilder createAppView] build]];
#else
//        [[DAGoogleAnalytics defaultTracker] sendAppView:@"Timed Game"];
#endif
        
        CCSprite * bg;
        if(IS_IPHONE_5){
            bg = [CCSprite spriteWithFile:@"startpage-iphone5.jpg"];
        } else {
            bg = [CCSprite spriteWithFile:@"startpage.jpg"];
        }
        [bg setPosition:ccp(size.width/2, size.height/2)];
        [self addChild:bg z:-100];
        
        // Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28*kScale];
        
//        CCMenuItemFontWithStroke *itemNewGame = [CCMenuItemFontWithStroke itemWithString:@"Play" block:^(id sender) {
//            
//            
//            [[SimpleAudioEngine sharedEngine] playEffect:@"fx-play.m4a"];
//            
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            int time = [defaults integerForKey:@"time"];
//            int modeType = [defaults integerForKey:@"mode"];
//            
//            NSString* mode = modeType==kGameModeTimedEasy ? @"Easy" : @"Hard";
//            
//            if(time==0){
//                app.currentLeaderBoard = modeType==kGameModeTimedEasy? kLeaderboard1ID:kLeaderboardHard1ID;
//            } else if (time==1){
//                app.currentLeaderBoard = modeType==kGameModeTimedEasy? kLeaderboard2ID:kLeaderboardHard2ID;
//            } else {
//                app.currentLeaderBoard = modeType==kGameModeTimedEasy? kLeaderboard3ID:kLeaderboardHard3ID;
//            }
//            
//            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
//                                                                      action:@"button_press"
//                                                                       label:[NSString stringWithFormat: @"Play %@ time:%d",mode,time]
//                                                                       value:nil] build]];
//            
//            //            NSLog(@"app.currentLeaderBoard==%@",app.currentLeaderBoard);
//            
//            [[CCDirector sharedDirector] replaceScene:[CCTransitionJumpZoom transitionWithDuration:1.0 scene:[ClassicGameScene scene]]];
//            
//        }];
        
        
        
        
        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        int time = [defaults integerForKey:@"time"];
//        int modeType = [defaults integerForKey:@"mode"];
//        if(modeType==2)modeType=0;
        int modeType = 0;
        
        CCMenuItemToggle *mode = [CCMenuItemToggle itemWithTarget:self selector:@selector(mode:) items:
                                  [CCMenuItemFontWithStroke itemWithString: @"Easy"], nil];
        
		NSArray *mode_items = [NSArray arrayWithObjects:
                               [CCMenuItemFontWithStroke itemWithString: @"Hard"],
                               nil];
		// TIP: you can manipulate the items like any other NSMutableArray
		[mode.subItems addObjectsFromArray: mode_items];
        
		// you can change the one of the items by doing this
		mode.selectedIndex = modeType;
        
//        CCMenuItemToggle *levelTime = [CCMenuItemToggle itemWithTarget:self selector:@selector(levelTime:) items:
//                                       [CCMenuItemFontWithStroke itemWithString: @"1:00 min"], nil];
//        
//		NSArray *more_items = [NSArray arrayWithObjects:
//                               [CCMenuItemFontWithStroke itemWithString: @"2:00 min"],
//                               [CCMenuItemFontWithStroke itemWithString: @"3:00 min"],
//                               nil];
//		// TIP: you can manipulate the items like any other NSMutableArray
//		[levelTime.subItems addObjectsFromArray: more_items];
//        
//		// you can change the one of the items by doing this
//		levelTime.selectedIndex = time;
        
        CCMenuItemFontWithStroke *itemGame1 = [CCMenuItemFontWithStroke itemWithString:@"1:00 min" block:^(id sender) {
            [self levelTime:0];
            [self startGame];
        }];
        CCMenuItemFontWithStroke *itemGame2 = [CCMenuItemFontWithStroke itemWithString:@"2:00 min" block:^(id sender) {
            [self levelTime:1];
            [self startGame];
        }];
        CCMenuItemFontWithStroke *itemGame3 = [CCMenuItemFontWithStroke itemWithString:@"3:00 min" block:^(id sender) {
            [self levelTime:2];
            [self startGame];
        }];
        CCMenuItemFontWithStroke *itemBack = [CCMenuItemFontWithStroke itemWithString:@"Back" color:ccWHITE  block:^(id sender) {
            
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
            [[CCDirector sharedDirector] popSceneWithTransition:[CCTransitionJumpZoom class] duration:1.0];

            
        }];
        itemBack.color = ccBLACK;
        
        itemGame1.color = ccRED;
        itemGame2.color = ccGREEN;
        itemGame3.color = ccBLUE;
        
        CCMenu *menu = [CCMenu menuWithItems:itemGame1,itemGame2,itemGame3,itemBack, nil];
		
		[menu alignItemsVerticallyWithPadding:20*kScale];
//        [menu alignItemsInColumns:
//         [NSNumber numberWithUnsignedInt:1],
//         [NSNumber numberWithUnsignedInt:1],
//         [NSNumber numberWithUnsignedInt:1],
//         [NSNumber numberWithUnsignedInt:1],
//         [NSNumber numberWithUnsignedInt:1],
//         [NSNumber numberWithUnsignedInt:1],
//         [NSNumber numberWithUnsignedInt:1],
//         nil];
		[menu setPosition:ccp( size.width/2, size.height/2 - 60*kScale)];
		
		// Add the menu to the layer
		[self addChild:menu];
        
        
    }
    return self;
}

-(void) startGame
{
    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
    
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int time = [defaults integerForKey:@"time"];
    int modeType = [defaults integerForKey:@"mode"];
    
    NSString* mode = modeType==kGameModeTimedEasy ? @"Easy" : @"Hard";
    
    if(time==0){
//        app.currentLeaderBoard = modeType==kGameModeTimedEasy? kLeaderboard1ID:kLeaderboardHard1ID;
        app.currentLeaderBoard = kLeaderboard1_v1;
    } else if (time==1){
//        app.currentLeaderBoard = modeType==kGameModeTimedEasy? kLeaderboard2ID:kLeaderboardHard2ID;
        app.currentLeaderBoard = kLeaderboard2_v1;
    } else {
//        app.currentLeaderBoard = modeType==kGameModeTimedEasy? kLeaderboard3ID:kLeaderboardHard3ID;
        app.currentLeaderBoard = kLeaderboard3_v1;
    }

#ifndef ANDROID
    [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                              action:@"button_press"
                                                               label:[NSString stringWithFormat: @"Play %@ time:%d",mode,time]
                                                               value:nil] build]];
#else
//    [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                     action:@"button_press"
//                                                      label:[NSString stringWithFormat: @"Play %@ time:%d",mode,time]];

#endif
    
    //            NSLog(@"app.currentLeaderBoard==%@",app.currentLeaderBoard);
    
    [[CCDirector sharedDirector] pushScene:[CCTransitionJumpZoom transitionWithDuration:1.0 scene:[ClassicGameScene scene]]];
}

-(void) levelTime:(int) time
{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:time forKey:@"time"];
    [defaults setInteger:0 forKey:@"mode"];
    [defaults synchronize];
    
}

-(void) mode:(id) sender
{
    
    
    CCMenuItemToggle* toggle = (CCMenuItemToggle*)sender;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:toggle.selectedIndex forKey:@"mode"];
    [defaults synchronize];
    
}


@end
