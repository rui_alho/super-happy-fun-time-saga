//
//  CCMenuItemFontWithStroke.h
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 10/04/2014.
//
//

#import "CCMenuItem.h"

@interface CCMenuItemFontWithStroke : CCMenuItemFont


-(id) initWithString:(NSString *)value block:(void (^)(id))block;
+(id) itemWithString: (NSString*) value color:(ccColor3B) color block:(void(^)(id sender))block;
-(id) initWithString:(NSString *)value color:(ccColor3B) color block:(void (^)(id))block;
+(id) itemWithLabel:(CCNode<CCLabelProtocol,CCRGBAProtocol> *)label color:(ccColor3B) color block:(void (^)(id))block;

@end
