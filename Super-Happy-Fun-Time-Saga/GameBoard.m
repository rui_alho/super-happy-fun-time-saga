//
//  GameBoard.m
//  Chainz
//
//  Created by Pedro Gomes on 12/13/11.
//  Copyright (c) 2011 Phluid Labs. All rights reserved.
//

#import "GameBoard.h"
#import "Gem.h"
#import "HelloWorldLayer.h"
#import "ClassicGameScene.h"
#import "AppSpecificValues.h"
#import "Toast.h"
#import "SimpleAudioEngine.h"
#import "Appirater.h"

////////////////////////////////////////////////////////////////////////////////
// Constants and definitions
////////////////////////////////////////////////////////////////////////////////
const NSUInteger 	kGameboardMinSequence 	= 3;

const NSInteger 	kGameboardNumberOfRows	= 8;
const NSInteger 	kGameboardNumberOfCols	= 8;
//const CGSize		kGameboardCellSize		= {40.0, 40.0};

#define kGameboardCellSize (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? CGSizeMake(40.0, 40.0) : CGSizeMake(80.0, 80.0))

#define kNinjaLeft  399999
#define kNinjaRight 388888
#define kNinjaUp    377777
#define kNinjaDown  366666

#define kNinjTut    355555
#define kVampTut    344444

const CGFloat       kSwapAnimationDuration              = 0.4;


const CGFloat       kfindAndClearAllComboChains         = 1.0;//1.0;//0.41;
const CGFloat       kUpdateAnimationDelay               = 0.0;//0.4;
const CGFloat       kClearChainAnimationDelay           = 0.4;

const CGFloat       kClearGemAnimationDuration          = 0.4;
const CGFloat       kDropNewGemAnimationDuration        = 0.41;

//const CGFloat       kDropDanglingGemAnimationDuration	= 0.3;


#define GET_COLOR(_point_) _board[(NSInteger)_point_.x][(NSInteger)_point_.y]
#define GET_COLORXY(_x_, _y_) _board[(NSInteger)_x_][(NSInteger)_y_]
#define SET_COLOR(_point_, _color_) do {_board[(NSInteger)_point_.x][(NSInteger)_point_.y] = _color_;} while(0)
#define SET_COLORXY(_x_, _y_, _color_) do {_board[(NSInteger)_x_][(NSInteger)_y_] = _color_;} while(0)

#define RAND_COLOR() (arc4random()%GemColorCount)

#define GEM_ACTION(_gem_, _action_) ([NSArray arrayWithObjects:_gem_, _action_, nil])

#define ITERATE_GAMEBOARD(x, y) \
for(x = 0; x < GAMEBOARD_NUM_COLS; x++)\
for(y = 0; y < GAMEBOARD_NUM_ROWS; y++)

#define ITERATE_GAMEBOARD_REVERSE(x, y) \
for(x = GAMEBOARD_NUM_COLS-1; x >= 0; x--)\
for(y = GAMEBOARD_NUM_ROWS; y >= 0; y--)

#define VALID_CELL(x,y) (x >= 0 && x < GAMEBOARD_NUM_COLS && y >= 0 && y < GAMEBOARD_NUM_ROWS)
#define LEFT_CELL(x,y) (VALID_CELL(x-1, y) ? _board[x-1][y] : GemColorInvalid)
#define RIGHT_CELL(x,y) (VALID_CELL(x+1, y) ? _board[x+1][y] : GemColorInvalid)
#define ABOVE_CELL(x,y) (VALID_CELL(x,y-1) ? _board[x][y-1] : GemColorInvalid)
#define BELOW_CELL(x,y) (VALID_CELL(x,y+1) ? _board[x][y+1] : GemColorInvalid)

#define ASSERT_VALID_CELL(point) do { NSAssert1(VALID_CELL(point.x, point.y), @"Invalid cell %@", NSStringFromCGPoint(node)); } while(0)


#define levelGoalIncrement 1000
#define levelGoalStart 4000
#define chainBonus 100
#define vampirePenalty 1000

////////////////////////////////////////////////////////////////////////////////
// Helper functions
/////////////////////////////////////////////// /////////////////////////////////
static NSInteger GemIndexForBoardPosition(CGPoint p) 
{
	return p.x*GAMEBOARD_NUM_COLS + p.y;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//#define GEM_SPACING 40.0

static CGPoint CoordinatesForGemAtPosition(CGPoint p)
{
	CGSize windowSize = [[CCDirector sharedDirector] winSize];
//	CGFloat yOrigin = (windowSize.height - windowSize.width)-GEM_SPACING;
    
    CGFloat xOrigin = (windowSize.width - GEM_SPACING*GAMEBOARD_NUM_ROWS)/2;
    CGFloat yOrigin = 50*kScale;
    
	CGFloat x = xOrigin + p.x*GEM_SPACING + GEM_SPACING/2; //+1;// + 12;
	CGFloat y = yOrigin +  GEM_SPACING*(GAMEBOARD_NUM_ROWS-1) - p.y*GEM_SPACING + GEM_SPACING/2;//+ 12;
	return CGPointMake(x, y);
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//static CGPoint CoordinatesForWindowLocation(CGPoint p)
//{
//	CGSize windowSize = [[CCDirector sharedDirector] winSize];
//	CGRect rect = CGRectMake(0, 0, windowSize.width, windowSize.width);
//	if(!CGRectContainsPoint(rect, p)) {
//		return (CGPoint){NSNotFound, NSNotFound};
//	}
//	
//	CGFloat x = GAMEBOARD_NUM_COLS - floor((rect.size.width - p.x)/GEM_SPACING) - 1;
//	CGFloat y = floor((rect.size.height - p.y)/GEM_SPACING);
//	
//	return (CGPoint){x, y};
//}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
@interface GameBoard()
{

    int _level;
//    BOOL showVamp;
    float goal;
    int runningTotal;
    int preRunningTotal;
    int moveCount;
//    int stars;
//    CCLabelTTFWithStroke* continueLabel;
    CCMenuItemFontWithStroke* continueItem;
    
    BOOL multiNinjaBooster;
    BOOL redNinjaBooster;
    BOOL greenNinjaBooster;
    BOOL vampAttack;
    BOOL vampLevel;
    BOOL showVampAttack;
    int ninjaCount;
}

- (void)_updateLegalMoves;
- (BOOL)_isLegalMove:(CGPoint)p1 p2:(CGPoint)p2;
- (NSMutableDictionary *)_findAllValidMoves;

- (NSArray *)_floodFill:(CGPoint)node color:(NSInteger)color;
- (NSArray *)_findAllChainsForSequence:(NSArray *)sequence;
- (NSArray *)_findAllChainsFromPoint:(CGPoint)point;
- (NSMutableDictionary *)_findAllChains;
- (void)_findAndClearAllComboChains;

- (void)_dropDanglingGems;
- (void)_generateAndDropDownGemsForClearedChains;
- (NSMutableArray *)_generateGemsForClearedCells;

// Used by simulateGameplay, but to be deprecated
- (NSDictionary *)_findAllSequences;

- (void)_drawGameboardGrid;
- (void)_animateAllPendingChanges;

@end

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
@implementation GameBoard

@synthesize emitter = emitter_;
//@synthesize updateTimer = _updateTimer;
@synthesize isBusy = _isbusy;

#pragma mark - Dealloc and Initialization

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//- (void)dealloc
//{
//	[_validMovesLookupTable release];
//	[_legalMovesLookupTable release];
//	[_gemDestructionQueue release];
//	[_gemDropdownQueue release];
//	[_gemGenerationQueue release];
//	[super dealloc];
//}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (id)init
{
	if((self = [super init])) {
		_gemDestructionQueue 	= [[NSMutableArray alloc] init];
		_gemDropdownQueue 		= [[NSMutableArray alloc] init];
		_gemGenerationQueue 	= [[NSMutableArray alloc] init];
		_validMovesLookupTable	= [[NSMutableDictionary alloc] init];
		_legalMovesLookupTable 	= [[NSMutableDictionary alloc] init];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([defaults valueForKey:@"virgin"] == nil){
            virgin = true;
        } else {
            virgin = [defaults boolForKey:@"virgin"];            
        }
//        virgin = NO;
        
        levelTime = [defaults integerForKey:@"time"];     
        mode = [defaults integerForKey:@"mode"];
        
        if(mode==kGameModeLevels){ //levels mode
            highScore = @"score_levels";
        } else {
            if(levelTime==0){
                highScore = mode==kGameModeTimedEasy? @"score1":@"score1hard";
            } else if (levelTime==1){
                highScore = mode==kGameModeTimedEasy? @"score2":@"score2hard";
            } else {
                highScore = mode==kGameModeTimedEasy? @"score3":@"score3hard";
            }
        }
        currentHighScore = [defaults integerForKey:highScore];
        if(mode==kGameModeLevels){
            _score = currentHighScore;
            _updatescore = _score;
            if([defaults valueForKey:@"level"] == nil){
                _level = 1;
            } else {
                _level = [defaults integerForKey:@"level"];
            }
        }
        
        if(fmod(_level, 10)  == 0){
            vampLevel = YES;
        }
//        vampLevel = YES;
        
//        NSLog(@"highscore === %d",currentHighScore);
        for (int i=0; i<_level; i++) {
            runningTotal += levelGoalStart+(levelGoalIncrement*(i+1));
            
        }
        
        //calc level score
        
        preRunningTotal = 0;
        for (int i=0; i<_level-1; i++) {
            preRunningTotal += (levelGoalStart+(levelGoalIncrement*(i+1)));
            
        }
        _levelscore = _score> 0 ? _score - preRunningTotal :0;
        
//        NSLog(@"_levelscore %d // %d",_levelscore, preRunningTotal);
        
//        redNinjaBooster = YES;
        

        
	}
	return self;
}


-(void) onEnter
{
    [super onEnter];
//    self.emitter = [CCParticleFlower node];
//	[self addChild:emitter_ z:10];
//	emitter_.texture = [[CCTextureCache sharedTextureCache] addImage: @"stars-grayscale.png"];
//    emitter_.duration = 0.3;
////    emitter_.startColor = ccc4FFromccc3B(self.color);
////    emitter_.endColor = ccc4FFromccc3B(self.color);
//    emitter_.startSize = 40.0f;
//	emitter_.startSizeVar = 00.0f;
//	emitter_.endSize = kCCParticleStartSizeEqualToEndSize;
//    emitter_.autoRemoveOnFinish = YES;
//    
//    emitter_.position = ccp(0, 0);
//    
//    [emitter_ stopSystem];
    
    //[self performSelector:@selector(updateGrid:) withObject:[NSNumber numberWithInt:index] afterDelay:1.0];
//    timer = [NSTimer scheduledTimerWithTimeInterval: 60.00f target: self selector: @selector(endgame) userInfo:nil repeats: NO] ;

    
    [Xplode hidePromotionDock];
    
    [self checkBoosters];
    
    elapsedTime = 5;
        
    [self schedule:@selector(gameLoop:) interval: 1/60.0f];
    

    
}

-(void) checkBoosters
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    int multiNinja = [defaults integerForKey:@"multiNinja"];
    int redNinja = [defaults integerForKey:@"redNinja"];
    int greenNinja = [defaults integerForKey:@"greenNinja"];
    
    if(multiNinja>0 || redNinja > 0 || greenNinja > 0){
        
        app.paused = YES;
        [self lockGems];
        pauseItem.visible = NO;
        CCLayerGradient* boosterLayer = [CCLayerGradient layerWithColor:ccc4(224, 166, 0, 250) fadingTo:ccc4(237, 235, 0, 250)];
        boosterLayer.position = CGPointMake(0, 0);
        [self addChild:boosterLayer z:106];
        
        CCLabelTTFWithStroke* bonusLabel = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:36*kScale];
        [bonusLabel setStringWithStroke:@"CHOOSE A BOOSTER"];
        bonusLabel.color = ccMAGENTA;
        bonusLabel.position = ccp(s.width/2,s.height-100*kScale);
        
//        CCSprite* ninja = [CCSprite spriteWithFile:@"Ninja.png"];
//        ninja.color = ccGRAY;
//        CCSprite* ninjaSelected = [CCSprite spriteWithFile:@"Ninja.png"];
//        ninjaSelected.color = ccWHITE;

        CCMenuItemToggle* multitoggle;
        if(multiNinja>0){
            NSString* boosterString = [NSString stringWithFormat:@"Multi Ninja booster\nx%d",multiNinja];
            
            CCLabelTTFWithStroke* label1 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:26*kScale];
            [label1 setStringWithStroke:boosterString];
            CCLabelTTFWithStroke* label2 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:26*kScale];
            [label2 setStringWithStroke:boosterString];
            
            CCMenuItemFontWithStroke *item1 = [CCMenuItemFontWithStroke itemWithLabel:label1];
            item1.color = ccWHITE;
            CCMenuItemFontWithStroke *item2 = [CCMenuItemFontWithStroke itemWithLabel:label2];
            item2.color = ccRED;
            
            
            multitoggle = [CCMenuItemToggle itemWithItems:[NSArray arrayWithObjects:item1,item2,nil] block:^(id sender) {
                CCMenuItemToggle* t = (CCMenuItemToggle*)sender;
                if(t.selectedIndex==0){
                    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
                } else {
                    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Sword];
                }
            }];

        }
        
        CCMenuItemToggle* redtoggle;
        
        if(redNinja>0){
            NSString* boosterString = [NSString stringWithFormat:@"Red Ninja booster\nx%d",redNinja];
            
            CCLabelTTFWithStroke* label1 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:26*kScale];
            [label1 setStringWithStroke:boosterString];
            CCLabelTTFWithStroke* label2 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:26*kScale];
            [label2 setStringWithStroke:boosterString];
            
            CCMenuItemFontWithStroke *item1 = [CCMenuItemFontWithStroke itemWithLabel:label1];
            item1.color = ccWHITE;
            CCMenuItemFontWithStroke *item2 = [CCMenuItemFontWithStroke itemWithLabel:label2];
            item2.color = ccRED;
            
            
            redtoggle = [CCMenuItemToggle itemWithItems:[NSArray arrayWithObjects:item1,item2,nil] block:^(id sender) {
                CCMenuItemToggle* t = (CCMenuItemToggle*)sender;
                if(t.selectedIndex==0){
                    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
                } else {
                    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Sword];
                }
            }];
        }
        
        CCMenuItemToggle* greentoggle;
        
        if(greenNinja>0){
            NSString* boosterString = [NSString stringWithFormat:@"Green Ninja booster\nx%d",greenNinja];
            
            CCLabelTTFWithStroke* label1 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:26*kScale];
            [label1 setStringWithStroke:boosterString];
            CCLabelTTFWithStroke* label2 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:26*kScale];
            [label2 setStringWithStroke:boosterString];
            
            CCMenuItemFontWithStroke *item1 = [CCMenuItemFontWithStroke itemWithLabel:label1];
            item1.color = ccWHITE;
            CCMenuItemFontWithStroke *item2 = [CCMenuItemFontWithStroke itemWithLabel:label2];
            item2.color = ccRED;
            
            
            greentoggle = [CCMenuItemToggle itemWithItems:[NSArray arrayWithObjects:item1,item2,nil] block:^(id sender) {
                CCMenuItemToggle* t = (CCMenuItemToggle*)sender;
                if(t.selectedIndex==0){
                    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
                } else {
                    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Sword];
                }
            }];
        }
        
        
        CCLabelTTFWithStroke* continueLabel = [CCLabelTTFWithStroke labelWithString:@"CONTINUE" fontName:@"Marker Felt" fontSize:40*kScale];
        continueLabel.color = ccBLUE;
        
        CCMenuItemFontWithStroke* continueBoosterItem = [CCMenuItemFontWithStroke itemWithLabel:continueLabel color:ccWHITE block:^(id sender) {
            
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            
            int multiNinja = [defaults integerForKey:@"multiNinja"];
            if(multiNinja>0)
            {
                if (multitoggle.selectedIndex==1) {
                    multiNinja --;
                    
                    [defaults setInteger:multiNinja forKey:@"multiNinja"];
                    [defaults synchronize];
                    
                    multiNinjaBooster = YES;
                    
                }

            }
            int redNinja = [defaults integerForKey:@"redNinja"];
            if(redNinja>0)
            {
                if (redtoggle.selectedIndex==1) {
                    redNinja --;
                    
                    [defaults setInteger:redNinja forKey:@"redNinja"];
                    [defaults synchronize];
                    
                    redNinjaBooster = YES;
                    
                }
                
            }
            int greenNinja = [defaults integerForKey:@"greenNinja"];
            if(greenNinja>0)
            {
                if (greentoggle.selectedIndex==1) {
                    greenNinja --;
                    
                    [defaults setInteger:greenNinja forKey:@"greenNinja"];
                    [defaults synchronize];
                    
                    greenNinjaBooster = YES;
                    
                }
                
            }
            
            app.paused = NO;
            [self unlockGems];
            pauseItem.visible = YES;
            
            
            [boosterLayer runAction:[CCSequence actions:
                                [CCMoveTo actionWithDuration:0.5 position:CGPointMake(0, -boosterLayer.boundingBox.size.height)],
                                     [CCCallFunc actionWithTarget:boosterLayer selector:@selector(removeFromParentAndCleanup:)],
                                     nil]];
            
//            [boosterLayer removeFromParentAndCleanup:YES];
            
        }];
        
        continueBoosterItem.position = CGPointMake(s.width/2, 100*kScale);
        
        
        CCMenu* themenu = [CCMenu menuWithItems:continueBoosterItem, nil];
        [themenu alignItemsVertically];
        [themenu setPosition:ccp(s.width/2, 100*kScale)];
        
        CCMenu* themenu2;
        
        NSMutableArray *mArray = [NSMutableArray new];
        if(multitoggle) [mArray addObject:multitoggle];
        if(redtoggle) [mArray addObject:redtoggle];
        if(greentoggle) [mArray addObject:greentoggle];
        
        themenu2 = [CCMenu menuWithArray:mArray];
        
        
        [themenu2 alignItemsVertically];
        [themenu2 setPosition:ccp(s.width/2, s.height/2)];
        
        [boosterLayer addChild:bonusLabel];
        [boosterLayer addChild:themenu];
        [boosterLayer addChild:themenu2];
        
        id repeat = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                       [CCScaleTo actionWithDuration:1.0 scale:1.05],
                                                       [CCScaleTo actionWithDuration:1.0 scale:0.95],
                                                       nil]];
        
        [continueBoosterItem runAction:repeat];
        
        
        
        boosterLayer.position = CGPointMake(0, -boosterLayer.boundingBox.size.height);
        
        [boosterLayer runAction:[CCSequence actions:
                                 [CCMoveTo actionWithDuration:0.5 position:CGPointMake(0, 0)],nil]];
        
    }

}

//-(void) removeFromParent:(id)selector
//{
//    CCNode *parent=((CCNode*)selector).parent;
//    [parent removeChild:self cleanup:YES];
//}

-(void)endgame
{
    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
    [[CCDirector sharedDirector] popSceneWithTransition:[CCTransitionJumpZoom class] duration:1.0];

}

- (void) gameLoop: (ccTime) dT
{
    // Do game logic here
    
    if(app.paused)return;
    
    elapsedTime -= dT;
    vampireTime += dT;
    vampireAttackTime += dT;
    
    seconds = elapsedTime;
    minutes = seconds / 60;
    hours = minutes / 60;
    
    seconds -= minutes * 60;
    minutes -= hours * 60;
    
    if(virgin){
        return;
    }
    
    
    if(!endGame){
    if(startGame){
    
        
        if(elapsedTime<0){
            elapsedTime = 0;
        }
    
        if(elapsedTime<=59){
            [startLabel setString:@""];
        }
    
//        [timerLabel setString:[NSString stringWithFormat:@"%@%02uh:%02um:%02us", (elapsedTime<0?@"-":@""), hours, minutes, seconds]];
        goal = levelGoalStart+(levelGoalIncrement*_level);
        float start = _levelscore;
        
        if(mode==kGameModeLevels){
            [timerLabel setString:[NSString stringWithFormat:@"Level %d",_level]];
        } else {
            [timerLabel setString:[NSString stringWithFormat:@"%2u:%02u",minutes, seconds]];
        }
        
        
//        if(_score>app.currentScore){
//            [self displayToast:@"New High Score!" :CGPointMake(s.width/2,400*kScale)];
//        }

        
        if(mode==kGameModeLevels){
            if(_score>0){
                
                
                [timeBar setPercentage:((start)/goal)*100];
//                NSLog(@"%d %d / %f == %f",currentHighScore,_score,goal,((_score-currentHighScore)/goal)*100);
                
                
            }
            if(vampLevel){
                //10 second delay before start
                if(vampireAttackTime > 10){
                    vampAttack = YES;
                    if(!showVampAttack){
                        showVampAttack = YES;
//                        [self displayToast:@"Vamp\nAttack!" :CGPointMake(s.width/2, s.height/2) andColor:ccRED];
                        Toast* label = [Toast labelWithString:@"" fontName:@"Marker Felt" fontSize:30*kScale];
                        [label setStringWithStroke:@"Vampire\nAttack!"];
                        label.color = ccRED;
                        label.position = CGPointMake(s.width/2, s.height/2);
                        label.opacity = 0;
                        [self addChild:label z:102];
                        [label runAction: [CCSequence actions:
                                           [CCFadeIn actionWithDuration:0.4],
                                           [CCDelayTime actionWithDuration:1.0],
                                           [CCFadeOut actionWithDuration:0.4],
                                           [CCCallFunc actionWithTarget:label selector:@selector(removeFromParent)],
                                           nil]];
                    }
                }
            }
            //attack for 30 seconds
            if(vampireAttackTime > 40){
                vampAttack = NO;
                showVampAttack = NO;
                vampireAttackTime = -110; //restart after 110 seconds + 10 delay
            }
        } else {
            [timeBar setPercentage:(elapsedTime*100)/((levelTime+1)*60)];
        }
        
//        NSLog(@"perc == %f", (elapsedTime*100)/60);
        
        if (_updatescore<_score) {
            _updatescore+=(_score-_updatescore)/4 + 4;
            if(_updatescore>+_score)_updatescore=_score;
        } else {
            _updatescore=_score;
        }
        
        
        [scoreLabel setStringWithStroke:[formatter stringFromNumber:[NSNumber numberWithInt:_updatescore]]];
        
        if(mode==kGameModeLevels){
            if(((start)/goal)*100>=100){
                endGame = YES;
                pauseItem.visible = NO;
                currentHighScore = 0;
            }
        } else {
            if(elapsedTime<=0)
            {
                endGame = YES;
                //            menuPause.visible = YES;
                //            pauseLayer.visible = YES;
                pauseItem.visible = NO;
            }
        }
        
        
    } else {
        
        if(mode==kGameModeLevels){
            startGame = YES;
            if(![startLabel.string isEqualToString:@"GO!"]){
                [startLabel setString:@"GO!"];
                AGSprite* sStart = [Util spriteForLabel:startLabel strokeSize:3 stokeColor:ccBLACK];
                sStart.opacity = 0;
                [sStart setPosition:CGPointMake(s.width/2, s.height/2)];
                [self addChild:sStart z:101];
                [sStart runAction:[CCSequence actions:
                                   [CCDelayTime actionWithDuration:1.0], //wait for animation to finish
                                   [CCFadeIn actionWithDuration:0.3],
                                   [CCCallFunc actionWithTarget:self selector:@selector(playGo)],
                                   [CCDelayTime actionWithDuration:1.0],
                                   [CCFadeOut actionWithDuration:0.3],
                                   [CCCallFunc actionWithTarget:sStart selector:@selector(removeFromParent)],
                                   nil]];
            }

        } else {
            if(seconds<=0)
            {
                startGame = YES;
                elapsedTime = 60 * (levelTime+1);
                if(![startLabel.string isEqualToString:@"GO!"]){
                    [startLabel setString:@"GO!"];
                    AGSprite* sStart = [Util spriteForLabel:startLabel strokeSize:3 stokeColor:ccBLACK];
                    sStart.opacity = 0;
                    [sStart setPosition:CGPointMake(s.width/2, s.height/2)];
                    [self addChild:sStart z:101];
                    [sStart runAction:[CCSequence actions:
                                       [CCFadeIn actionWithDuration:0.3],
                                       [CCCallFunc actionWithTarget:self selector:@selector(playGo)],
                                       [CCDelayTime actionWithDuration:0.4],
                                       [CCFadeOut actionWithDuration:0.3],
                                       [CCCallFunc actionWithTarget:sStart selector:@selector(removeFromParent)],
                                       nil]];
                }
            } else {
                if(![startLabel.string isEqualToString:[NSString stringWithFormat:@"%d",seconds]]){
                    [startLabel setString:[NSString stringWithFormat:@"%d",seconds]];
                    AGSprite* sStart = [Util spriteForLabel:startLabel strokeSize:3 stokeColor:ccBLACK];
                    sStart.opacity = 0;
                    [sStart setPosition:CGPointMake(s.width/2, s.height/2)];
                    [self addChild:sStart z:101];
                    [sStart runAction:[CCSequence actions:
                                       [CCFadeIn actionWithDuration:0.2],
                                       [CCDelayTime actionWithDuration:0.4],
                                       [CCFadeOut actionWithDuration:0.2],
                                       [CCCallFunc actionWithTarget:sStart selector:@selector(removeFromParent)],
                                       nil]];
                }
            }

        }
        
        
    }
    } else {
        
        //end game/level
        
        [self lockGems];
        
        if(mode == kGameModeLevels){
            
            if(!_showLevelEnd){
                [scoreLabel setStringWithStroke:[formatter stringFromNumber:[NSNumber numberWithInt:_score]]];
                
                
                if(!_isbusy){
                    _showLevelEnd = YES;
                    
                    for (Gem* gem in _gems){
                        if(gem != (id)[NSNull null]) [gem wobble];
                    }
                    
                    [self runAction:[CCSequence actions:
                                 [CCDelayTime actionWithDuration:1.0],
                                 [CCCallFunc actionWithTarget:self selector:@selector(showLevelEnd)],
                                 nil]];
                }
                
            }
            
//            [self showLevelEnd];
            
            
//            if(![startLabel.string isEqualToString:@"Level Completed!"]){
            
//                startLabel.fontSize = 32*kScale;
//                [startLabel setString:@"Level Completed!"];
//                AGSprite* sStart = [Util spriteForLabel:startLabel strokeSize:3 stokeColor:ccBLACK];
//                sStart.opacity = 0;
//                [sStart setPosition:CGPointMake(s.width/2, s.height/2)];
//                [self addChild:sStart z:103];
//                [sStart runAction: [CCSequence actions:
//                                    [CCFadeIn actionWithDuration:0.2],
//                                    [CCDelayTime actionWithDuration:0.4],
//                                    [CCFadeOut actionWithDuration:0.2],
//                                    [CCCallFunc actionWithTarget:self selector:@selector(showLevelEnd)],
//                                    [CCCallFunc actionWithTarget:sStart selector:@selector(removeFromParent)],
//                                    nil]];
//            }
            
        } else {
            if(![startLabel.string isEqualToString:@"Time Up!"]){
                startLabel.fontSize = 64*kScale;
                [startLabel setString:@"Time Up!"];
                AGSprite* sStart = [Util spriteForLabel:startLabel strokeSize:3 stokeColor:ccBLACK];
                sStart.opacity = 0;
                [sStart setPosition:CGPointMake(s.width/2, s.height/2)];
                [self addChild:sStart z:103];
                [sStart runAction: [CCSequence actions:
                                    [CCFadeIn actionWithDuration:2.0],
                                    [CCCallFunc actionWithTarget:self selector:@selector(showMenu)],
                                    //                            [CCCallFunc actionWithTarget:sStart selector:@selector(removeFromParent)],
                                    nil]];
                [self sendScore];
                
                
            }
            [scoreLabel setStringWithStroke:[formatter stringFromNumber:[NSNumber numberWithInt:_score]]];
            //        if(_score>app.currentScore)
            if(_score>currentHighScore)
            {
                if(![highscoreLabel.string isEqualToString:@"New High Score!"]){
                    [highscoreLabel setStringWithStroke:@"New High Score!"];
                }
            }
            
        }
        
        

        
    }
//    NSLog(@"vampireTime == %f  / %d",vampireTime, (((int)vampireTime)%5));
    
//    NSLog(@"seconds %d" , elapsedTime -= dT;);
    
    //Vampire Logic every second
    int penalty = 0;
    for(Gem* gem in _gems){
        if(gem != (id)[NSNull null]){
            if(gem.gemColor == GemColorWhite && gem.active && !_isbusy && !app.paused){
                //every second
                
                
                if(((int)vampireTime)%5 == 1){
                    
                    if(gem.showVamp){
                        gem.showVamp = NO;
                        gem.zOrder = 101;
                        
                        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Blah];
                        
//                        CGPoint point = scoreLabel.position;
//                        point.x += (scoreLabel.boundingBox.size.width + 10*kScale);
                        //                    point.y -= 50%kScale;
                        
                        penalty+=vampirePenalty;
//                        [self displayToast:[NSString stringWithFormat:@"-%d",penalty]:point andColor:ccRED];
                        [gem runAction: [CCSequence actions:
                                         [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:2.0f],
                                         [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:1.0f],
                                         nil]];
                        _levelscore -= vampirePenalty;
                        if(_levelscore<0){
                            _levelscore=0;
                            _updatescore = preRunningTotal;
                            _score = preRunningTotal;
                        } else {
                            _updatescore -= vampirePenalty;
                            _score -= vampirePenalty;
                        }
//                        NSLog(@"go");
                        
                    }
                    
                } else {
                    gem.showVamp = YES;
                }
//                NSLog(@"seconds %d" ,(seconds % minutes));
            }
        }
        if(penalty>0){
            CGPoint point = scoreLabel.position;
            point.x += (scoreLabel.boundingBox.size.width + 10*kScale);
            [self displayToast:[NSString stringWithFormat:@"-%d",penalty]:point andColor:ccRED];
        }
    }
    
    
    
}

#pragma mark - CCNode

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//- (void)draw
//{
//	[super draw];
//	[self _drawGameboardGrid];
//}

////////////////////////////////////////////////////////////////////////////////
// Just a helper grid
////////////////////////////////////////////////////////////////////////////////
- (void)_drawGameboardGrid
{
	CGSize windowSize = [[CCDirector sharedDirector] winSize];
	
	CGPoint borderVertices[4] = {{1,1}, {windowSize.width-1, 1}, {windowSize.width-1, windowSize.height-1}, {1, windowSize.height-1}};
	ccDrawPoly(borderVertices, 4, YES);
	
	CGFloat gridHeight = windowSize.height - (windowSize.width/2.0);
	CGFloat gridSpacing = 40.0;
	NSInteger x, y;
	for(x = 1; x < GAMEBOARD_NUM_COLS; x++) {
		ccDrawLine((CGPoint){x*gridSpacing, 0}, (CGPoint){x*gridSpacing, gridHeight});
	}
	for(y = 1; y <= GAMEBOARD_NUM_ROWS; y++) {
		ccDrawLine((CGPoint){0, (y*gridSpacing)}, (CGPoint){windowSize.width, (y*gridSpacing)});
	}
}

#pragma mark - Public Methods


-(void) showMenu
{
    menuPause.visible = YES;
    menuPause.opacity = 0;
    pauseLayer.visible = YES;            
    
    [menuPause runAction:[CCFadeIn actionWithDuration:1.0]];
    
    [app showInterstitial];
}


-(void) showLevelEnd
{

    
    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Yes];

    [levelLabel1 setStringWithStroke:[NSString stringWithFormat:@"LEVEL %d\nCOMPLETED",_level]];
//    [levelLayer runAction: [CCFadeTo actionWithDuration:1.5f opacity:100]];
    
    
//    levelLabel2.position = CGPointMake(-levelLabel2.boundingBox.size.width, levelLabel2.position.y);
    levelLabel2.visible = YES;
    
    levelLayer.position = CGPointMake(s.width, 0);
    levelLayer.visible = YES;
    [levelLayer runAction:[CCSequence actions:
                           [CCMoveTo actionWithDuration:0.4 position:CGPointMake(-40, 0)],
                           [CCMoveTo actionWithDuration:0.2 position:CGPointMake(20, 0)],
                           [CCCallFunc actionWithTarget:self selector:@selector(bounceLabel)],
                           [CCMoveTo actionWithDuration:0.1 position:CGPointMake(0, 0)],
                           [CCDelayTime actionWithDuration:0.6],
                           [CCCallFunc actionWithTarget:self selector:@selector(showAd)],
                           nil]];
    
    levelLabel3.visible = YES;
    
    [levelLabel3 runAction:[CCSequence actions:
                            [CCDelayTime actionWithDuration:0.4],
                            [CCCallFunc actionWithTarget:self selector:@selector(setStarsLabel)],
                            [CCDelayTime actionWithDuration:0.4],
                            [CCCallFunc actionWithTarget:self selector:@selector(setStarsLabel)],
                            [CCDelayTime actionWithDuration:0.4],
                            [CCCallFunc actionWithTarget:self selector:@selector(setStarsLabel)],
                           nil]];
    id repeat = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                   [CCScaleTo actionWithDuration:1.0 scale:1.05],
                                                   [CCScaleTo actionWithDuration:1.0 scale:0.95],
                                                   nil]];
    
    [continueItem runAction:repeat];
    
    int stars = 1;
    if(moveCount<=4+_level){
        stars = 3;
    } else if(moveCount<=4+_level+2){
        stars = 2;
    } else {
        stars = 1;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int oldscore = [defaults integerForKey:[NSString stringWithFormat:@"levelscore%d",_level]];
    
    if(_levelscore>oldscore){
        [defaults setInteger:_levelscore forKey:[NSString stringWithFormat:@"levelscore%d",_level]];
        [defaults setInteger:stars forKey:[NSString stringWithFormat:@"starsscore%d",_level]];
        [defaults synchronize];
    }
    
    [self sendScore];
    
}

-(void) setStarsLabel
{
    int stars = 1;
    if(moveCount<=4+_level){
        stars = 3;
    } else if(moveCount<=4+_level+2){
        stars = 2;
    } else {
        stars = 1;
    }
    
    NSString* starsString = [NSString new];
    starsString = levelLabel3.string;
    
    if([levelLabel3.string isEqual:@"☆☆☆"]){
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
        starsString = @"★☆☆";
    }
    
    if([levelLabel3.string isEqual:@"★☆☆"] && stars>1){
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
        starsString = @"★★☆";
    }
    
    if([levelLabel3.string isEqual:@"★★☆"] && stars>2){
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
        starsString = @"★★★";
    }
    

    [levelLabel3 setString:[NSString stringWithFormat:@"%@",starsString]];
}


-(void) bounceLabel
{
    [levelLabel2 runAction:[CCSequence actions:
//                            [CCDelayTime actionWithDuration:1.5],
                            [CCMoveTo actionWithDuration:0.1 position:CGPointMake(s.width/2-levelLabel2.boundingBox.size.width/2, levelLabel2.position.y)],
                            [CCMoveTo actionWithDuration:0.1 position:CGPointMake(s.width/2+levelLabel2.boundingBox.size.width/4, levelLabel2.position.y)],
                            [CCMoveTo actionWithDuration:0.1 position:CGPointMake(s.width/2-levelLabel2.boundingBox.size.width/8, levelLabel2.position.y)],
                            [CCMoveTo actionWithDuration:0.1 position:CGPointMake(s.width/2, levelLabel2.position.y)],
                            nil]];
}


-(void) playGo
{
    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Go];
}
//-(void) playLetsGo
//{
//    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_LetsGo];
//}
-(void) startNextLevel
{

//    [levelLayer runAction: [CCFadeTo actionWithDuration:1.5f opacity:0]];
//    levelLayer.position = CGPointMake(s.width, 0);
    [levelLayer runAction:[CCSequence actions:
                           [CCMoveTo actionWithDuration:0.5 position:CGPointMake(-s.width, 0)],
                           [CCCallFunc actionWithTarget:self selector:@selector(hideLevelLayer)],
                           nil]];

    _showLevelEnd = NO;
    
    [levelLabel3 setString:@"☆☆☆"];
    
    
    multiNinjaBooster = NO;
    redNinjaBooster = NO;
    greenNinjaBooster = NO;
    moveCount = 0;
    _level++;
    _levelscore = (_levelscore-goal); //overlap
    runningTotal += levelGoalStart+(levelGoalIncrement*_level);
    
    if(fmod(_level, 10)  == 0){
        vampLevel = YES;
        vampireAttackTime = 0;
    } else {
        vampLevel = NO;
    }
    vampAttack = NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:_level forKey:@"level"];
    [defaults synchronize];
    [self sendScore];
    currentHighScore = _score;
    _updatescore = _score;
    
    [startLabel setString:@""];
    endGame = NO;
    startGame = YES;
    pauseItem.visible = YES;
//    levelLayer.visible = NO;
    [self unlockGems];
    
    
    
}

-(void) hideLevelLayer
{
    levelLayer.visible = NO;
    levelLabel2.visible = NO;
    [self checkBoosters];
}

-(void) showAd
{
    [app showInterstitial];

}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)resetGameBoard
{
	unsigned int x, y;
	
	// We need to find out if the generated board has valid chains
	// if it has, clear them, generate new gems and start over 
	// We also need to figure out if the current board is solvable
	// If not, generate a new one(?)
	BOOL boardReady = NO;
	while(!boardReady) {
//		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
        @autoreleasepool {
            
		for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
			for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
				_board[x][y] = RAND_COLOR();
			}
		}
		
//		ITERATE_GAMEBOARD(x, y) {
//			_board[x][y] = RAND_COLOR();
//		}
		
		NSMutableDictionary *chains = [self _findAllChains];
		while([chains count] > 0) {
			for(NSString *pointStr in [chains allKeys]) {
				CGPoint p = CGPointFromString(pointStr);
				NSArray *chain = [chains objectForKey:pointStr];
				[self clearChain:p sequence:chain];
			}
			[self generateGemsForClearedCells];
			
			[chains removeAllObjects];
			[chains setDictionary:[self _findAllChains]];
		}
		
		if([[self _findAllValidMoves] count] > 0) {
			boardReady = YES;
		}
		
//		[pool drain];
        }
	}
//	[self printBoard];
	
	if(!_gems) {
		_gems = [[NSMutableArray alloc] initWithCapacity:GAMEBOARD_NUM_ROWS*GAMEBOARD_NUM_COLS];
	}
	
	for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
		for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
			Gem *gem = [[Gem alloc] initWithGameboard:self position:(CGPoint){x,y} kind:GemKindNormal color:_board[x][y]];
			//			gem.anchorPoint = CGPointZero;
//			gem.position = CoordinatesForGemAtPosition((CGPoint){x,y});
			
            CGPoint dest = CoordinatesForGemAtPosition(gem.point);
            CGSize windowSize = [[CCDirector sharedDirector] winSize];
            CGPoint srcSpritePosition = {dest.x, windowSize.height-kGameboardCellSize.height/2};
            gem.position = srcSpritePosition;
            
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                CCMoveTo *dropAction = [CCMoveTo actionWithDuration:kDropNewGemAnimationDuration position:dest];
                [gem runAction:dropAction];
            });
            
			//			CCLOG(@"%@ = %@, c = %d", NSStringFromCGPoint(gem.point), NSStringFromCGPoint(gem.position), gem.gemColor);
			[self addChild:gem];
			[_gems addObject:gem];
//			[gem release];
            gem = nil;
		}
	}
	
//	ITERATE_GAMEBOARD(x, y) {
//		Gem *gem = [[Gem alloc] initWithGameboard:self position:(CGPoint){x,y} kind:GemKindNormal color:_board[x][y]];
//		gem.position = CoordinatesForGemAtPosition((CGPoint){x,y});
//		[self addChild:gem];
//		[_gems addObject:gem];
//		[gem release];
//	}
    
    app = (AppController*) [[UIApplication sharedApplication] delegate];

    s = [[CCDirector sharedDirector] winSize];    
    
//    [app addMultiNinja:10];
//    [app addRedNinja:10];
//    [app addGreenNinja:10];
    
    CCSprite * bg;
    
    if(IS_IPHONE_5){
        bg = [CCSprite spriteWithFile:@"Gameboard-iphone5.jpg"];
    } else {
        bg = [CCSprite spriteWithFile:@"Gameboard.jpg"];
    }
    
	[bg setPosition:ccp(s.width/2, s.height/2)];
	[self addChild:bg z:-100];
    
    
    formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    scoreLabel = [CCLabelTTFWithStroke labelWithString:@"0" dimensions:CGSizeMake(0, 0) hAlignment:kCCTextAlignmentLeft fontName:@"Marker Felt" fontSize:30*kScale];
    [scoreLabel setStringWithStroke:@"0"];
    scoreLabel.position = CGPointMake(60*kScale,396*kScale);
    [self addChild:scoreLabel z:103];
    
    timerLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%2u:%02u",(levelTime+1),0] fontName:@"Marker Felt" fontSize:20*kScale];
    if(mode == kGameModeLevels){
        [timerLabel setString:[NSString stringWithFormat:@"Level %d",_level]];
    }
    timerLabel.position = CGPointMake(s.width/2,10*kScale);
    timerLabel.color = ccBLUE;
    [self addChild:timerLabel z:100];
    
    
    startLabel = [CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:96*kScale];
    startLabel.position = CGPointMake(s.width/2,s.height/2);
//    [self addChild:startLabel z:103];
    
    
    CCLabelTTF* quitLabel = [CCLabelTTF labelWithString:@"Quit" fontName:@"Marker Felt" fontSize:26*kScale];
    quitItem = [CCMenuItemFontWithStroke itemWithLabel:quitLabel block:^(id sender) {
        
        [[CCDirector sharedDirector] resume];
        
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
        
        if(mode==kGameModeLevels){
            _score = currentHighScore;
            _updatescore = _score;
        }
        
        [self sendScore];
        
        [self endgame];
        
    }];
    quitItem.color = ccRED;
    
    CCLabelTTF* restartLabel = [CCLabelTTF labelWithString:@"Play Again" fontName:@"Marker Felt" fontSize:26*kScale];
    restartItem = [CCMenuItemFontWithStroke itemWithLabel:restartLabel block:^(id sender) {
        
        [[CCDirector sharedDirector] resume];
        
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
        
        
        if(mode==kGameModeLevels){
            _score = currentHighScore;
            _updatescore = _score;
        }
        
        [self sendScore];
        
        
            [[CCDirector sharedDirector] replaceScene:[CCTransitionJumpZoom transitionWithDuration:1.0 scene:[ClassicGameScene scene]]];
    }];
    restartItem.color = ccGREEN;
    
    if(mode==kGameModeLevels){
        [restartItem setString:@"Restart Level"];
    }
    CCLabelTTF* labelLeaderboard = [CCLabelTTF labelWithString:@"Leaderboard" fontName:@"Marker Felt" fontSize:26*kScale];
    CCMenuItemFontWithStroke* itemLeaderboard = [CCMenuItemFontWithStroke itemWithLabel:labelLeaderboard block:^(id sender) {
        
        
#ifndef ANDROID
        [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                  action:@"button_press"
                                                                   label:@"Leaderboard"
                                                                   value:nil] build]];
#else
        //            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
        //                                                             action:@"button_press"
        //                                                              label:@"Leaderboard"];
        
#endif
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
        
        [app leaderboard];
        
    }];
    itemLeaderboard.color = ccYELLOW;
    
//    CCLabelTTF* continueLabel = [CCLabelTTF labelWithString:@"Continue" fontName:@"Marker Felt" fontSize:26*kScale];
//    continueItem = [CCMenuItemLabel itemWithLabel:continueLabel block:^(id sender) {
//        [[CCDirector sharedDirector] resume];
//    }];
    
    
    highscoreLabel = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:26*kScale];
    highscoreLabel.position = (CGPoint){s.width/2,360*kScale};
    highscoreLabel.color = ccYELLOW;
//    [self addChild:highscoreLabel];

    
    pauseLayer = [CCLayerColor layerWithColor: ccc4(0, 0, 0, 100) width: s.width height: s.height];
    pauseLayer.visible = NO;
//    CCLayerColor* menuLayer = [CCLayerColor layerWithColor: ccc4(0, 0, 255, 255) width: s.width-40*kScale height: 100*kScale];
    CCSprite * menuLayer = [CCSprite spriteWithFile:@"play_again_box.jpg"];
    menuLayer.position = ccp(s.width/2, 100*kScale);
    [menuLayer setScaleY:1.5];
    [pauseLayer addChild:menuLayer];
    [pauseLayer addChild:highscoreLabel];
    [self addChild:pauseLayer z:102];
    
    
    helpLayer = [CCLayerColor layerWithColor: ccc4(0, 0, 0, 180) width: s.width height: s.height];
    helpLayer.visible = NO;
    [self addChild:helpLayer z:104];
    
//    levelLayer = [CCLayerGradient layerWithColor: ccc4(255, 127, 0, 220) width: s.width height: s.height];
    levelLayer = [CCLayerGradient layerWithColor:ccc4(224, 166, 0, 250) fadingTo:ccc4(237, 235, 0, 250)];
    levelLayer.visible = NO;
    levelLayer.position = CGPointMake(s.width, 0);
    [self addChild:levelLayer z:104];
    
    menuPause = [CCMenu menuWithItems:itemLeaderboard, restartItem, quitItem, nil];
    [menuPause alignItemsVertically];
    [menuPause setPosition:ccp(s.width/2, 100*kScale)];
    menuPause.visible = NO;
    [self addChild: menuPause z:102];
    
//    CCProgressFromTo *to1 = [CCProgressFromTo actionWithDuration:60 from:100 to:0];
    timeBar = [CCProgressTimer progressWithSprite:[CCSprite spriteWithFile:@"timer_bar.jpg"]];
    timeBar.type = kCCProgressTimerTypeBar;
    timeBar.barChangeRate = ccp(1,0);
    timeBar.midpoint = ccp(0, 0);
    if(mode==kGameModeLevels){
        timeBar.percentage = 0;
    } else {
        timeBar.percentage = 100;
    }
    [timeBar setPosition:ccp(s.width/2,10*kScale)];
    [self addChild:timeBar];
//    [timeBar runAction:to1];

    
    CCMenuItemImage *itemPause = [CCMenuItemImage itemWithNormalImage:@"pause_button.jpg" selectedImage:nil];
    CCMenuItemImage *itemPlay = [CCMenuItemImage itemWithNormalImage:@"play_button.jpg" selectedImage:nil];       
    
    app.paused = NO;
    
    pauseItem = [CCMenuItemToggle itemWithItems:[NSArray arrayWithObjects:itemPause,itemPlay,nil] block:^(id sender) {
        if (app.paused) {
            [pauseItem setSelectedIndex:0];
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            app.paused = NO;
            [[CCDirector sharedDirector] resume];
            menuPause.visible = NO;
            pauseLayer.visible = NO;
            
            [self unlockGems];
            
        } else {
            [pauseItem setSelectedIndex:1];
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
            app.paused = YES;            
            [[CCDirector sharedDirector] pause];
            menuPause.visible = YES;
            pauseLayer.visible = YES;  
            
            [self lockGems];
        }
        
    }];

	pauseItem.position = ccp(140*kScale,160*kScale);
    [pauseItem setSelectedIndex:0];
	[self addChild:[CCMenu menuWithItems:pauseItem,nil] z:104];
    
    
    if(virgin){
        
        CCLabelTTFWithStroke* okLabel = [CCLabelTTFWithStroke labelWithString:@"OK" fontName:@"Marker Felt" fontSize:20*kScale];
        okLabel.color = ccBLACK;
        
        CCMenuItemFontWithStroke* okItem = [CCMenuItemFontWithStroke itemWithLabel:okLabel color:ccWHITE block:^(id sender) {
            

            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            helpCounter++;
            helpLayer.visible = NO;
            helpLabel.visible = NO;
            helpMenu.visible = NO;
            
            [self cleanUp];
            [self unlockGems];
            
            if(helpCounter==3){
                [self showHelp:@"Ninja's will clear out a\nline of faces"];
            } else if(helpCounter==4){
                [self showHelp:@"Watch out for Vampires\nthey will drain your score"];
            } else if(helpCounter==5){
            
                _score = currentHighScore;
                _updatescore = _score;
                virgin = NO;
                pauseItem.visible = YES;
                elapsedTime = 5;
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setBool:virgin forKey:@"virgin"];
                [defaults synchronize];  
                
            } else {
                
            }
            
        }];

        
//        helpLayer = [CCSprite spriteWithFile:@"play_again_box.jpg"];
//        helpLayer.position = ccp(s.width/2,s.height/2);
        

        
        
        helpLabel = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:20*kScale];
        [helpLabel setStringWithStroke:@"Drag a happy face\nto swap it with another"];
        helpLabel.position = ccp(s.width/2,s.height/2);
        
        
        helpMenu = [CCMenu menuWithItems:okItem, nil];
        helpMenu.position = ccp(s.width/2,s.height/2 - 75*kScale);
        
//        [helpLayer addChild:helpLabel];
//        [helpLayer addChild:helpMenu];
        
//        [self addChild:helpLayer z:103];
        
        [self addChild:helpMenu z:105];
        [self addChild:helpLabel z:105];
        
        helpLayer.visible = YES;
        
        [self lockGems];
        
        pauseItem.visible = NO;
    }
    
    CCLabelTTFWithStroke* continueLabel = [CCLabelTTFWithStroke labelWithString:@"CONTINUE" fontName:@"Marker Felt" fontSize:40*kScale];
    continueLabel.color = ccBLUE;
    
    continueItem = [CCMenuItemFontWithStroke itemWithLabel:continueLabel color:ccWHITE block:^(id sender) {
        
        [self startNextLevel];
        
    }];
    

    
    
    CCLabelTTF* levelRestartLabel = [CCLabelTTF labelWithString:@"Try Again" fontName:@"Marker Felt" fontSize:26*kScale];
    CCMenuItemFontWithStroke* levelRestartItem = [CCMenuItemFontWithStroke itemWithLabel:levelRestartLabel block:^(id sender) {
        
        [[CCDirector sharedDirector] resume];
        
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
        
        
        _score = currentHighScore;
        _updatescore = _score;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setInteger:preRunningTotal forKey:highScore];
        [defaults synchronize];
        
        [[CCDirector sharedDirector] replaceScene:[CCTransitionJumpZoom transitionWithDuration:1.0 scene:[ClassicGameScene scene]]];
    }];
    levelRestartLabel.color = ccRED;

    labelLeaderboard = [CCLabelTTF labelWithString:@"Leaderboard" fontName:@"Marker Felt" fontSize:26*kScale];
    itemLeaderboard = [CCMenuItemFontWithStroke itemWithLabel:labelLeaderboard block:^(id sender) {
        
        
#ifndef ANDROID
        [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                  action:@"button_press"
                                                                   label:@"Leaderboard"
                                                                   value:nil] build]];
#else
        //            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
        //                                                             action:@"button_press"
        //                                                              label:@"Leaderboard"];
        
#endif
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
        
        [app leaderboard];
        
    }];
    itemLeaderboard.color = ccYELLOW;
    
    levelLabel1 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:36*kScale];
    levelLabel1.color = ccGREEN;
    levelLabel1.position = ccp(s.width/2,s.height-150*kScale);
    
    levelLabel2 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:36*kScale];
    [levelLabel2 setStringWithStroke:@"WELL DONE!!!"];
    levelLabel2.color = ccMAGENTA;
    levelLabel2.position = ccp(s.width/2,s.height/3);
    levelLabel2.visible = NO;
    
    levelLabel3 = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:70*kScale];
    [levelLabel3 setStringWithStroke:@"☆☆☆"];
    levelLabel3.color = ccRED;
    levelLabel3.position = ccp(s.width/2,s.height/2);
    levelLabel3.visible = NO;
    
    levelMenu = [CCMenu menuWithItems:itemLeaderboard, levelRestartItem, continueItem, nil];
    [levelMenu alignItemsVertically];
    levelMenu.position = CGPointMake(s.width/2, 80*kScale);
    
    [levelLayer addChild:levelMenu];
    [levelLayer addChild:levelLabel1];
    [levelLayer addChild:levelLabel2];
    [levelLayer addChild:levelLabel3];
    
}


-(void) sendScore
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(_score>currentHighScore){
                
        [defaults setInteger:_score forKey:highScore];
        [defaults synchronize];            
    }
    
    app.currentScore =_score;
    [app submitScore];
}

-(void) showHelp:(NSString*) message
{
    helpLayer.visible = YES;
    helpLabel.visible = YES;
    helpMenu.visible = YES;

    [helpLabel setStringWithStroke:message];
    
    if(helpCounter==3){
        Gem* ninja = [[Gem alloc] initWithGameboard:self position:CGPointMake(s.width/2, s.height/2) kind:GemKindSpecialPower color:GemColorBlack];
        
        ninja.position = CGPointMake(60*kScale, 100*kScale + (IS_IPHONE_5?40:0));
        
        ninja.active = NO;
        [self addChild:ninja.streak z:1 tag:kNinjaRight];
        
        id repeat = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                       [CCDelayTime actionWithDuration:1.0],
                                                       [CCMoveTo actionWithDuration:kClearGemAnimationDuration position:CGPointMake(280*kScale, ninja.position.y)],
                                                       [CCMoveTo actionWithDuration:kClearGemAnimationDuration position:CGPointMake(60*kScale, ninja.position.y)],
                                                       nil]];
        
        
        [self addChild:ninja z:105 tag:kNinjTut];
        
        [ninja runAction:repeat];
        
    } else if (helpCounter==4){
        
        Gem* vampire = [[Gem alloc] initWithGameboard:self position:CGPointMake(s.width/2, s.height/2) kind:GemKindSpecialPower color:GemColorWhite];
        
        vampire.position = CGPointMake(s.width/2, 100*kScale + (IS_IPHONE_5?40:0));
        
        vampire.active = NO;
        id repeat = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                       [CCDelayTime actionWithDuration:1.0],
                         [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:2.0f],
                        [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:1.0f],
                         nil]];
        [self addChild:vampire z:105 tag:kVampTut];
        
        [vampire runAction:repeat];

    }

    
    [self lockGems];
}


////////////////////////////////////////////////////////////////////////////////
// This is the actual game move, swaps a gem with another gem
// Checks if it's a valid move (did the swap result in a chain?)
// Is so, clears the resulting chain, and readjusts the board (drops gems above cleared ones)
// and also generates gems for the resulting empty cells
////////////////////////////////////////////////////////////////////////////////
- (void)swapGemAtPoint:(CGPoint)point1 withGemAtPoint:(CGPoint)point2
{
	// TODO: lookup _validMovesLookupTable, if it's not a valid movement 
	// we simply schedule two animations (swap node1 <-> node2, swap node2 <-> node1)
	// else, compute the actual chains, clear them, etc.
	
	CC_SWAP(_board[(NSInteger)point1.x][(NSInteger)point1.y], _board[(NSInteger)point2.x][(NSInteger)point2.y]);
	
	NSInteger indexGem1 = GemIndexForBoardPosition(point1);
	NSInteger indexGem2 = GemIndexForBoardPosition(point2);
	
	Gem *gem1 = [_gems objectAtIndex:indexGem1];
	Gem *gem2 = [_gems objectAtIndex:indexGem2];
	[_gems exchangeObjectAtIndex:indexGem1 withObjectAtIndex:indexGem2];
    CC_SWAP(gem1.point, gem2.point);
	
	NSArray *point1Chain = [self _findAllChainsFromPoint:point1];
	NSArray *point2Chain = [self _findAllChainsFromPoint:point2];
	
	id swapGem1Action = [CCMoveTo actionWithDuration:0.4 position:gem2.position];
	id swapGem2Action = [CCMoveTo actionWithDuration:0.4 position:gem1.position];
	
	BOOL canSwapGems = ([point1Chain count] + [point2Chain count] > 0);
	if(!canSwapGems) {
		id swapGem1ReverseAction = [CCMoveTo actionWithDuration:0.4 position:gem1.position];
		id swapGem2ReverseAction = [CCMoveTo actionWithDuration:0.4 position:gem2.position];
		
		CC_SWAP(_board[(NSInteger)point1.x][(NSInteger)point1.y], _board[(NSInteger)point2.x][(NSInteger)point2.y]);
        CC_SWAP(gem1.point, gem2.point);
		[_gems exchangeObjectAtIndex:indexGem1 withObjectAtIndex:indexGem2];
		
        id unlockSeq = [CCCallFunc actionWithTarget:self selector:@selector(unlockGems)];
		id gem1Sequence = [CCSequence actions:swapGem1Action, swapGem1ReverseAction, unlockSeq, nil];
		id gem2Sequence = [CCSequence actions:swapGem2Action, swapGem2ReverseAction, unlockSeq, nil];
        
		
		[gem1 runAction:gem1Sequence];
		[gem2 runAction:gem2Sequence];
	}
	else {
		[gem1 runAction:swapGem1Action];
		[gem2 runAction:swapGem2Action];
        moveCount++;
		
		// replace this with the appropriate CCScheduler invocations
		double delayInSeconds = 0.41;
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            NSArray *newPoint1Chain = [self checkExplosives:point1Chain addSpecial:point1Chain.count>3?YES:NO];
            NSArray *newPoint2Chain = [self checkExplosives:point2Chain addSpecial:point2Chain.count>3?YES:NO];
            _chains = point1Chain.count + point2Chain.count;
            [self checkAchievements];
            
            [self updateSpecial];
			[self clearChain:point1 sequence:newPoint1Chain];
			[self clearChain:point2 sequence:newPoint2Chain];
			[self _dropDanglingGems];
			[self _generateAndDropDownGemsForClearedChains];
			[self _findAndClearAllComboChains];
		});
	}
    
    if(virgin && helpCounter==1){
        double delayInSeconds = 2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        
            [self showHelp:@"Match three or more of\na kind to clear them."];

        });
    }
    
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (BOOL)moveGemAtPoint:(CGPoint)point withDirection:(GameboardMovementDirection)direction
{
    
    for (Gem* gem in _gems){
        if(gem.selected) {
            gem.selected = NO;
            [gem markSelected:NO];
        }
    }
    
	CGPoint destPoint = point;
	switch(direction)
	{
		case GameboardMovementDirectionUp:
			if(point.y - 1 >= 0) destPoint = CGPointMake(point.x, point.y-1);
			break;
			
		case GameboardMovementDirectionDown:
			if(point.y + 1 < GAMEBOARD_NUM_ROWS) destPoint = CGPointMake(point.x, point.y+1);
			break;
			
		case GameboardMovementDirectionLeft:
			if(point.x - 1 >= 0) destPoint = CGPointMake(point.x - 1, point.y);
			break;
			
		case GameboardMovementDirectionRight:
			if(point.x + 1 < GAMEBOARD_NUM_COLS) destPoint = CGPointMake(point.x + 1, point.y);
			break;
			
		default: break;
	}
	
	if(CGPointEqualToPoint(point, destPoint)) {
		return NO;
	}
	
    [self lockGems];

	[self swapGemAtPoint:point withGemAtPoint:destPoint];
    
	return YES;
}


-(void) checkSelections:(Gem*) gemIn
{
    int selected = 0;
    Gem* firstGem;
    Gem* secondGem;
    for (Gem* gem in _gems){
        if(gem.selected) {
            selected++;
            if(selected == 1){
                firstGem = gem;
            }
            if(selected == 2){
                secondGem = gem;
            }
        }
    }
    if(selected>=2){
        
        
        Gem* checkGem;
        if([gemIn isEqual:firstGem]){
            checkGem = secondGem;
        } else {
            checkGem = firstGem;
        }
        
		// vertical or horizontal?
		CGFloat horizontalOffset = gemIn.point.x - checkGem.point.x;
		CGFloat verticalOffset = gemIn.point.y - checkGem.point.y;

        
		GameboardMovementDirection direction = GameboardMovementDirectionInvalid;
		if(fabs(horizontalOffset) >= fabs(verticalOffset)) { // moved horizontally
			if(horizontalOffset == 1 && verticalOffset == 0)
                direction = GameboardMovementDirectionRight;
			else if(horizontalOffset == -1 && verticalOffset == 0)
                direction = GameboardMovementDirectionLeft;
		}
		else {
			if(verticalOffset == 1 && horizontalOffset == 0)
                direction = GameboardMovementDirectionDown;
			else if(verticalOffset == -1 && horizontalOffset == 0)
                direction = GameboardMovementDirectionUp;
		}
        
        [self moveGemAtPoint:checkGem.point withDirection:direction];
        [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Swoosh];
        

    }
    
}

-(void)updateBoardAtPoint:(CGPoint)point :(NSInteger)color
{
    int x = point.x;
    int y = point.y;
    _board[x][y] = color;
    
}

-(BOOL)updateGemAtPoint:(CGPoint)point :(NSInteger)color
{
    
//    if(_isbusy) return NO;
    
//    NSArray *point1Chain = [self _findAllChainsFromPoint:point];
    
//    NSArray *point1Chain = [self _floodFill:point color:_board[(NSInteger)point.x][(NSInteger)point.y]];
    

    if(virgin && helpCounter==1){
        [self showHelp:@"Match three or more of\na kind to clear them."];
        return NO;
    }
    
    
//    NSArray *point1Chain;
//    
//    if(mode == 0){
//        point1Chain = [self _floodFill:point color:_board[(NSInteger)point.x][(NSInteger)point.y]];  
//    } else if(mode == 1){
//        point1Chain = [self _findAllChainsFromPoint:point];    
//    } else if(mode == 2){
//        point1Chain = [self _floodFill:point color:_board[(NSInteger)point.x][(NSInteger)point.y]];
//    }
//    
//    
//    if(point1Chain.count>0)
//    {
//        moveCount++;
//        _chains = point1Chain.count;
//        [self checkAchievements];
//    }

    // replace this with the appropriate CCScheduler invocations
    double delayInSeconds = kUpdateAnimationDelay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_current_queue(), ^(void) {
        
        _isbusy = YES;
        [self updateSpecial:point];
//        [self clearChain:point sequence:point1Chain];
        [self _dropDanglingGems];
        [self _generateAndDropDownGemsForClearedChains];
        [self _findAndClearAllComboChains];
        [self _animateAllPendingChanges];
    });
    

    
    return YES;
}

////////////////////////////////////////////////////////////////////////////////
// Iterates the whole board and generates new gems for all cells marked as cleared (-1)
// Schedules the drop down animation of all generated gems
////////////////////////////////////////////////////////////////////////////////
- (void)_generateAndDropDownGemsForClearedChains
{
	NSMutableArray *generatedGems = [self _generateGemsForClearedCells];
	for(NSString *pointStr in generatedGems) {
		// Create the actual gem sprite, add it outside the gameboard and schedule
		// a drop animation action to the destination point
		CGPoint boardPos = CGPointFromString(pointStr);
		Gem *gem = [[Gem alloc] initWithGameboard:self position:boardPos kind:GemKindNormal color:_board[(NSInteger)boardPos.x][(NSInteger)boardPos.y]];
        gem.active = NO;
		CGPoint dstSpritePosition = CoordinatesForGemAtPosition(gem.point);
//		CGPoint srcSpritePosition = {dstSpritePosition.x, kGameboardNumberOfRows*kGameboardCellSize.height+kGameboardCellSize.height}; //RA
        CGSize windowSize = [[CCDirector sharedDirector] winSize];
        CGPoint srcSpritePosition = {dstSpritePosition.x, windowSize.height-kGameboardCellSize.height/2};
		gem.position = srcSpritePosition;
		
		[self addChild:gem];
//		[gem runAction:[CCMoveTo actionWithDuration:0.4 position:dstSpritePosition]];
        
        [self _enqueueDropGemAnimation:gem point:boardPos];
		
		[_gems replaceObjectAtIndex:GemIndexForBoardPosition(boardPos) withObject:gem];
//		[gem release];
        gem = nil;
	}
//	[self printBoard];
}

////////////////////////////////////////////////////////////////////////////////
// Iterates the whole board and generates new gems for all cells marked as cleared (-1)
// Returns the list of all positions where new gems have been generated
////////////////////////////////////////////////////////////////////////////////
- (NSMutableArray *)_generateGemsForClearedCells
{
	NSMutableArray *generatedGems = [[NSMutableArray alloc] init];
	
    int count = 0;
    int redNinjaCount = 0;
    int greenNinjaCount = 0;
    
	NSInteger x, y;
    int oldColor = -1;
	for(x = GAMEBOARD_NUM_COLS-1; x >= 0; x--) {
		for(y = GAMEBOARD_NUM_ROWS; y >= 0; y--) {
			if(_board[x][y] == -1) {
                int col = RAND_COLOR();
                count++;

                //1 in 40 chnace of getting a ghost
                int ghost = arc4random()%(mode==kGameModeLevels?40:20);
//                NSLog(@"ghost==%d",ghost);
                if(ghost == 1){
                    col = GemColorWhite;
                    vampireTime = 0;
                }
                
                if(vampAttack && count%2 == 1){
                    col = GemColorWhite;
                    vampireTime = 0;
                }
                
                //every 3rd is a ninja
                if(multiNinjaBooster && count%3 == 1){
                    col = GemColorBlack;
                    if (ninjaCount>10) {
                        multiNinjaBooster = NO;
                        ninjaCount = 0;
                    }
                    ninjaCount++;
                } else {
                    //1 in 20 chance of getting a ninja, trump ghosts
                    int ninja = arc4random()%(mode==kGameModeLevels?20:10);
                    if(ninja == 1){
                        col = GemColorBlack;
                    }
                }
                
                if(redNinjaBooster){
                    if(redNinjaCount==0){
                        col = GemColorRedNinja;
                        redNinjaBooster = NO;
                        redNinjaCount++;
                    }
                } else if(greenNinjaBooster){
                    if(greenNinjaCount==0){
                        col = GemColorGreenNinja;
                        greenNinjaBooster = NO;
                        greenNinjaCount++;
                    }
                }
                
                _board[x][y] = col;
//				_board[x][y] = RAND_COLOR();
                while(_board[x][y] == oldColor) {
                    _board[x][y] = RAND_COLOR();
                }
                oldColor = _board[x][y];
				[generatedGems addObject:NSStringFromCGPoint((CGPoint){x,y})];
			}
		}
	}
	
//	ITERATE_GAMEBOARD_REVERSE(x, y) {
//		if(_board[x][y] == -1) {
//			_board[x][y] = RAND_COLOR();
//			[generatedGems addObject:NSStringFromCGPoint((CGPoint){x,y})];
//		}
//	}
//	return [generatedGems autorelease];
    return generatedGems;
}

//-(void) checkDirection:(CGPoint) point forGem:(Gem*) gem
//{
//    NSInteger gemIndex = GemIndexForBoardPosition(point);
//    Gem* check = [_gems objectAtIndex:gemIndex];
//    if(gem.gemColor == GemColorBlack){
//        if(![gem isEqual:check]){
//            check.directionH = !check.directionH;
//            [self updateSpecial:check.point];
//        }
//    }
//}


-(NSArray*) checkExplosives:(NSArray *)sequence addSpecial:(BOOL) addSpecial
{
    
    
//    //check for a 4 chain that was made with a swap gem
//    GemColor oldColor = GemColorInvalid;
//    addSpecial = NO;
//    
//    if(sequence.count > 3){
//        for(NSString *pointStr in sequence) {
//            CGPoint p = CGPointFromString(pointStr);
//            NSInteger gemIndex = GemIndexForBoardPosition(p);
//            Gem* gem = [_gems objectAtIndex:gemIndex];
//            if([gem isKindOfClass:[Gem class]]){
//                if(oldColor==GemColorInvalid){
//                    oldColor = gem.gemColor;
//                    addSpecial = YES;
//                }
//                if(gem.gemColor != oldColor){
//                    addSpecial = NO;
//                    break;
//                }
//            }
//        }
//    }
//    
//    NSLog(@"2addSpecial==%d",addSpecial);
    
    NSMutableArray* newArray = [NSMutableArray new];
    int count = 0;
	for(NSString *pointStr in sequence) {
		CGPoint p = CGPointFromString(pointStr);
		NSInteger gemIndex = GemIndexForBoardPosition(p);
        
		if([[_gems objectAtIndex:gemIndex] isKindOfClass:[Gem class]]) {

            Gem* gem = [_gems objectAtIndex:gemIndex];
            if(addSpecial ){
                if(count == 0){
                    gem.attributes = GemBoostExplosive;
                    [gem needsExplode];
                    [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Explosive];
                    gem.zOrder = 102;
                    [gem runAction: [CCSequence actions:
                                 [CCScaleTo actionWithDuration:0.4 scale:2],
                                 [CCScaleTo actionWithDuration:0.4 scale:1],
                                 nil]];
                } else {
                    [newArray addObject:pointStr];
                }
//                if(count==4){
//                    [app addRandomBooster];
//                }
            } else {
                if(gem.attributes == GemBoostExplosive){
                    gem.needsUpdate = YES;
                }
                [newArray addObject:pointStr];
            }
        }
        count++;
    }
    
    return newArray;
    
}

-(BOOL) updateSpecial
{
    //needsupdate
    for(Gem* gem in _gems){
        if(gem != (id)[NSNull null]){
            if(gem.needsUpdate){
                [self updateSpecial:gem.point];
                return YES;
            }
        }
    }
    return NO;
}

-(void) updateSpecial:(CGPoint) pointIn
{
    int gemIndex = GemIndexForBoardPosition(pointIn);
    Gem* gem = [_gems objectAtIndex:gemIndex];
    
    if([gem isKindOfClass:[Gem class]]){
        
        if(gem.kind == GemKindSpecialPower){
        NSMutableArray *chain = [[NSMutableArray alloc] init];
        switch (gem.gemColor) {
            case GemColorBlack:{
                if(gem.directionH){
                    //clear left or right?
                    if(pointIn.x < 4){ //clear right
                        for(int x=pointIn.x; x<GAMEBOARD_NUM_COLS; x++){
                            CGPoint point = CGPointMake(x, pointIn.y);
                            [chain addObject:NSStringFromCGPoint(point)];
//                            [self checkDirection:point forGem:gem];
                        }
                    } else { //clear left
                        for(int x=pointIn.x; x>=0; x--){
                            CGPoint point = CGPointMake(x, pointIn.y);
                            [chain addObject:NSStringFromCGPoint(point)];
//                            [self checkDirection:point forGem:gem];
                        }
                    }
                } else {
                    //clear down or up?
                    if(pointIn.y < 4){ //clear down
                        for(int y=pointIn.y; y<GAMEBOARD_NUM_ROWS; y++){
                            CGPoint point = CGPointMake(pointIn.x, y);
                            [chain addObject:NSStringFromCGPoint(point)];
//                            [self checkDirection:point forGem:gem];
                        }
                    } else { //clear up
                        for(int y=pointIn.y; y>=0; y--){
                            CGPoint point = CGPointMake(pointIn.x, y);
                            [chain addObject:NSStringFromCGPoint(point)];
//                            [self checkDirection:point forGem:gem];
                        }
                    }
                }
                
                break;
            }
            case GemColorWhite:
                [chain addObject:NSStringFromCGPoint(pointIn)];
                break;
            case GemColorRedNinja:
                //3 up/down left/right
                for(int y=pointIn.y-3; y < pointIn.y+3; y++){
                    if(y>=0 && y<GAMEBOARD_NUM_ROWS)
                    {
                        for(int x=pointIn.x-3; x < pointIn.x+3; x++){
                            if(x>=0 && x<GAMEBOARD_NUM_COLS)
                            {
                                CGPoint point = CGPointMake(x, y);
                                [chain addObject:NSStringFromCGPoint(point)];
                            }
                        }
                    }
                }

                
                break;
            case GemColorGreenNinja:

                if(pointIn.x < 4){ //clear right
                    [chain addObject:NSStringFromCGPoint(pointIn)];
                    CGPoint point = CGPointMake(pointIn.x+1, pointIn.y);
                    NSInteger gemIndex = GemIndexForBoardPosition(point);
                    Gem* check = [_gems objectAtIndex:gemIndex];
                    for (Gem* g in _gems){
                        if(check != (id)[NSNull null] && g != (id)[NSNull null]){
                            if(check.gemColor == g.gemColor){
                                [chain addObject:NSStringFromCGPoint(g.point)];
                            }
                        }
                    }
                    
                } else {
                    [chain addObject:NSStringFromCGPoint(pointIn)];
                    CGPoint point = CGPointMake(pointIn.x-1, pointIn.y);
                    NSInteger gemIndex = GemIndexForBoardPosition(point);
                    Gem* check = [_gems objectAtIndex:gemIndex];
                    for (Gem* g in _gems){
                        if(check != (id)[NSNull null] && g != (id)[NSNull null]){
                            if(check.gemColor == g.gemColor){
                                [chain addObject:NSStringFromCGPoint(g.point)];
                            }
                        }
                    }
                    
                }
                
                break;
            default:
                break;
        }

            //change direction for hits set needsupdate
            NSMutableArray *newchain = [[NSMutableArray alloc] init];
            for(NSString *pointStr in chain) {
                CGPoint hitp = CGPointFromString(pointStr);
                NSInteger gemIndex = GemIndexForBoardPosition(hitp);
                Gem* check = [_gems objectAtIndex:gemIndex];
                if(check != (id)[NSNull null]){
                    if(check.gemColor == GemColorBlack || check.gemColor == GemColorRedNinja || check.gemColor == GemColorGreenNinja || check.attributes == GemBoostExplosive){
                        if([gem isEqual:check]){
                            [newchain addObject:pointStr];
                        } else {
                            check.directionH = !gem.directionH;
                            check.needsUpdate = YES;
                        }
                    } else {
                        [newchain addObject:pointStr];
                    }
                }
            }
            chain = newchain;
        
            [self clearChain:pointIn sequence:chain];
            
        } else {
            NSMutableArray *chain = [[NSMutableArray alloc] init];
            if(gem.attributes == GemBoostExplosive){
//                NSLog(@"KABOOM");
                //1 up/down left/right
                for(int y=pointIn.y-2; y < pointIn.y+2; y++){
                    if(y>=0 && y<GAMEBOARD_NUM_ROWS)
                    {
                        for(int x=pointIn.x-2; x < pointIn.x+2; x++){
                            if(x>=0 && x<GAMEBOARD_NUM_COLS)
                            {
                                CGPoint point = CGPointMake(x, y);
                                [chain addObject:NSStringFromCGPoint(point)];
                            }
                        }
                    }
                }
            }
            //change direction for hits set needsupdate
            NSMutableArray *newchain = [[NSMutableArray alloc] init];
            for(NSString *pointStr in chain) {
                CGPoint hitp = CGPointFromString(pointStr);
                NSInteger gemIndex = GemIndexForBoardPosition(hitp);
                Gem* check = [_gems objectAtIndex:gemIndex];
                if(check != (id)[NSNull null]){
                    if(check.gemColor == GemColorBlack || check.gemColor == GemColorRedNinja || check.gemColor == GemColorGreenNinja || check.attributes == GemBoostExplosive){
                        if([gem isEqual:check]){
                            [newchain addObject:pointStr];
                        } else {
                            check.directionH = !gem.directionH;
                            check.needsUpdate = YES;
                        }
                    } else {
                        [newchain addObject:pointStr];
                    }
                }
            }
            chain = newchain;
            [self clearChain:pointIn sequence:chain];
        }
    }

    //needsupdate
    for(Gem* gem in _gems){
        if(gem != (id)[NSNull null]){
            if(gem.needsUpdate){
                [self updateSpecial:gem.point];
                break;
            }
        }
    }
    
}

////////////////////////////////////////////////////////////////////////////////
// Clears (marks) all gems in a given chain
////////////////////////////////////////////////////////////////////////////////
- (void)clearChain:(CGPoint)point sequence:(NSArray *)sequence
{
//	CCLOG(@"CLEARING CHAIN <%@: %@>", NSStringFromCGPoint(point), sequence);
	   
    
	for(NSString *pointStr in sequence) {
		CGPoint p = CGPointFromString(pointStr);
        _board[(NSInteger)p.x][(NSInteger)p.y] = -1;

		NSInteger gemIndex = GemIndexForBoardPosition(p);

		if([[_gems objectAtIndex:gemIndex] isKindOfClass:[Gem class]]) { // when clearing multiple intersecting chains, intersecting gems may have been cleared already
            
//            Gem* gem = [_gems objectAtIndex:gemIndex];
            
		 // instead of simply removing the sprite, add it to a "destroy" list
            [_gemDestructionQueue addObject:[_gems objectAtIndex:gemIndex]]; // gem is now queued for destruction
            [_gems replaceObjectAtIndex:gemIndex withObject:[NSNull null]];
        }
    }
    
	[self visit];
	// schedule the clear animation
    [self _animateAllPendingChanges];
}

////////////////////////////////////////////////////////////////////////////////
// Iterates over the whole board and generates a random gem for every position
// TODO: figure out a way to be able to tweak the generation process (like a bias factor or something)
// This way we could generate boards with higher/lower number of possible matches
////////////////////////////////////////////////////////////////////////////////
- (void)generateGemsForClearedCells
{
	NSInteger x, y;
    int oldColor = -1;
	for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
		for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
			if(_board[x][y] == -1) {
                _board[x][y] = RAND_COLOR();
                
                while(_board[x][y] == oldColor) {
                    _board[x][y] = RAND_COLOR();
                }
                
//                if (_board[x][y] == oldColor) {
//                    _board[x][y] = RAND_COLOR();
//                }
                oldColor = _board[x][y];
                
//                NSLog(@"color == %d", oldColor);
                
			}
		}
	}
	
//	ITERATE_GAMEBOARD(x, y) {
//		if(_board[x][y] == -1) {
//			_board[x][y] = RAND_COLOR();
//		}
//	}
}

////////////////////////////////////////////////////////////////////////////////
// Shifts down all gems that are above cleared chains
// Also schedules the drop down animation
// NOTE: should we just returns the list of position updates so we can deliver them to an animator later?
////////////////////////////////////////////////////////////////////////////////
- (void)_dropDanglingGems
{
	NSInteger x, y;
//	[self printBoard];
	for(x = GAMEBOARD_NUM_COLS-1; x >= 0; x--) {
		for(y = GAMEBOARD_NUM_ROWS-1; y >= 0; y--) {
			if(_board[x][y] != -1) continue;
			NSInteger py = y-1;
			while(py >= 0 && _board[x][py] == -1) {
				py--;
			}
			if(py >= 0) {
				_board[x][y] = _board[x][py];
				_board[x][py] = -1;
				// schedule the drop down animation here
				// or
				// save the position update (src => destination) and return the list
				CGPoint oldPos = {x, py};
				CGPoint newPos = {x, y};
				
				NSInteger oldIndex = GemIndexForBoardPosition(oldPos);
				NSInteger newIndex = GemIndexForBoardPosition(newPos);
				
				[_gems exchangeObjectAtIndex:oldIndex withObjectAtIndex:newIndex];
				id gem = [_gems objectAtIndex:newIndex];
				if([gem isKindOfClass:[Gem class]]) {
					[(Gem *)gem setPoint:newPos];
//					CCMoveTo *action = [CCMoveTo actionWithDuration:0.4 position:CoordinatesForGemAtPosition(newPos)];
//					[(Gem *)gem runAction:action]; //RA
                    
                    [self _enqueueDropGemAnimation:gem point:newPos];
                    
				}
			}
		}
	}
	// schedule the drop animations for every gem we actually moved down
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)simulateGameplay
{
	[self resetGameBoard];
	
	//	printMatrix(NUM_ROWS, NUM_COLS, matrix);
	//	swapGems(NSMakePoint(0, 0), NSMakePoint(1, 0));
	//	printMatrix(NUM_ROWS, NUM_COLS, matrix);
	
	NSDictionary *matches = [self _findAllSequences];
	int move = 1;
	while([matches count] > 0) {
		NSArray *positions = [matches allKeys];
		NSInteger pos = arc4random()%[positions count];
		NSString *key = [positions objectAtIndex:pos];
		NSLog(@"pos = %@", key);
		NSLog(@"move %d", move++);
		
		NSArray *chains = [self _findAllChainsForSequence:[matches objectForKey:key]];
		[self clearChain:CGPointFromString(key) sequence:chains];
		[self _dropDanglingGems];
		[self printBoard];
		[self generateGemsForClearedCells];
		[self printBoard];
		matches = [self _findAllSequences];
	}
	
//	NSLog(@"No more moves left!");	
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)printBoard
{
	unsigned int x, y;
	
	NSMutableString	*matrixStr = [[NSMutableString alloc] initWithString:@"\n"];
	for(y = 0; y < GAMEBOARD_NUM_COLS; y++) {
		for(x = 0; x < GAMEBOARD_NUM_ROWS; x++) {
			[matrixStr appendFormat:@"[%2d] ", _board[x][y]];
		}
		[matrixStr appendString:@"\n"];
	}
	
	NSLog(@"%@", matrixStr);
//	[matrixStr release];
    matrixStr = nil;
}

#pragma mark - Private Methods

////////////////////////////////////////////////////////////////////////////////
// At the moment, at least in my head, it makes sense to separate the flood fill
// from the actual code that determines if there are valid chains
// Since the flood fill is all about finding sequences of matching colors given
// an origin point, we can then work from there and find which sequences are 
// actually triggering some game event
// What this actually does:
//	given a starting point, finds all adjacent points with the same color
//	we perform the same operation for every matching point
//	if we matched at least 3 points (including the source) we return them
////////////////////////////////////////////////////////////////////////////////
- (NSArray *)_floodFill:(CGPoint)node color:(NSInteger)color
{
	ASSERT_VALID_CELL(node);
	
	NSMutableArray *matches = [[NSMutableArray alloc] init];
    
    //hack to prevent ninja/vampire chains
    if(color==GemColorBlack || color == GemColorWhite || color == GemColorRedNinja || color == GemColorGreenNinja){
        return matches;
    }
    
	NSMutableArray *queue = [[NSMutableArray alloc] init];
	[queue addObject:NSStringFromCGPoint(node)];
	
	NSMutableDictionary *visitedNodes = [[NSMutableDictionary alloc] init];
	
	while([queue count] > 0) {
		NSString *nodeKey = [queue objectAtIndex:0];
		[queue removeObjectAtIndex:0];
		
		CGPoint p = CGPointFromString(nodeKey);
		NSInteger x = (NSUInteger)p.x;
		NSInteger y = (NSUInteger)p.y;
		
		if([visitedNodes objectForKey:nodeKey]) {
			continue;
		}
		else {
			[visitedNodes setObject:[NSNull null] forKey:nodeKey];
		}
		
		if(_board[x][y] == color) {
			[matches addObject:NSStringFromCGPoint(p)];
		}
		
		NSInteger cursor = x-1;
		if(cursor >= 0 && _board[cursor][y] == color) {
			[queue addObject:NSStringFromCGPoint((CGPoint){cursor, y})];
			cursor--;
		}
		
		cursor = x+1;
		if(cursor < GAMEBOARD_NUM_ROWS && _board[cursor][y] == color) {
			[queue addObject:NSStringFromCGPoint((CGPoint){cursor, y})];
			cursor++;
		}
		
		cursor = y-1;
		if(cursor >= 0 && _board[x][cursor] == color) {
			[queue addObject:NSStringFromCGPoint((CGPoint){x, cursor})];
			cursor--;
		}
		
		cursor = y+1;
		if(cursor < GAMEBOARD_NUM_COLS && _board[x][cursor] == color) {
			[queue addObject:NSStringFromCGPoint((CGPoint){x, cursor})];
			cursor++;
		}
	}
    [queue removeAllObjects];
	queue = nil;
    [visitedNodes removeAllObjects];
	visitedNodes = nil;
	
	// This stage we don't really  care if the matching adjacent cells actually translate to a valid move
	// It's unlikely that we'll every need to match less than 3 cells, but in the future we may want to match diagonals, L shapes, etc.
	// We we'll implement the validation algorithm elsewhere
	// e.g.
	// [ 1] [ 2] [ 2]
	// [ 1] [ 1] [ 0]
	// [ 3] [ 3] [ 3]
	// This would match (0,0), (0,1), (1,1) (color = 1) and (0,2) (1,2) (2,2) (color = 3)
	// typical bejeweled rules would only translate to one valid chain (color 3)
	// again, we'll implement the game logic validation elsewhere
	if([matches count] < kGameboardMinSequence) {
		[matches removeAllObjects];
	}
	return matches;
}

////////////////////////////////////////////////////////////////////////////////
// Given the result of applying the FloodFill algorithm to a given point
// find all valid chains (given our game rules, every sequence of 3 or more columns or rows)
//
// TODO: modify to allow a reference point to be passed. The resulting chain must contain
// the point
////////////////////////////////////////////////////////////////////////////////
- (NSArray *)_findAllChainsForSequence:(NSArray *)sequence
{
	//	NSLog(@"***** FINDING ALL VALID CHAINS *****");
	//	NSLog(@"sequence = %@", sequence);
	
	// First we'll sort all points by X and by Y
	
	// Sort sequence by X
	NSArray *sortedByColumns = [sequence sortedArrayWithOptions:NSSortStable usingComparator:(NSComparator)^(id obj1, id obj2) {
		CGPoint p1 = CGPointFromString(obj1);
		CGPoint p2 = CGPointFromString(obj2);
		
		NSComparisonResult result;
		if(p1.x == p2.x) {
			if(p1.y < p2.y)			result = NSOrderedAscending;
			else if(p1.y > p2.y)	result = NSOrderedDescending;
			else 					result = NSOrderedSame;
		}
		else if(p1.x < p2.x) 		result = NSOrderedAscending;
		else 						result = NSOrderedDescending;
		return result;
	}];
	//	NSLog(@"sortedByColumns = %@", sortedByColumns);
	
	// Sort sequence by Y
	NSArray *sortedByRows = [sequence sortedArrayWithOptions:NSSortStable usingComparator:(NSComparator)^(id obj1, id obj2) {
		CGPoint p1 = CGPointFromString(obj1);
		CGPoint p2 = CGPointFromString(obj2);
		
		NSComparisonResult result;
		if(p1.y == p2.y) {
			if(p1.x < p2.x) 		result = NSOrderedAscending;
			else if(p1.x > p2.x) 	result = NSOrderedDescending;
			else 					result = NSOrderedSame;
		}
		if(p1.y < p2.y) 			result = NSOrderedAscending;
		else 						result = NSOrderedDescending;
		
		return result;
	}];
	//	NSLog(@"sortedByRows = %@", sortedByRows);
	
	NSMutableArray *chain = [[NSMutableArray alloc] init];
	NSMutableArray *tmpStack = [[NSMutableArray alloc] init];
	
	// ensure we have at least kGameboardMinSequence contiguous points on the same column
	CGFloat prevX = -1;
	for(NSString *pv in sortedByColumns) {
		CGPoint p = CGPointFromString(pv);
		if(p.x != prevX) {
			if([tmpStack count] >= kGameboardMinSequence) {
				[chain addObjectsFromArray:tmpStack];
			}
			[tmpStack removeAllObjects];
		}
		[tmpStack addObject:pv];
		prevX = p.x;
	}
	if([tmpStack count] >= kGameboardMinSequence) {
		[chain addObjectsFromArray:tmpStack];	
	}
	[tmpStack removeAllObjects];

	// ensure we have at least kGameboardMinSequence contiguous points on the same row
	CGFloat prevY = -1;
	for(NSString *pv in sortedByRows) {
		CGPoint p = CGPointFromString(pv);
		if(p.y != prevY) {
			if([tmpStack count] >= kGameboardMinSequence) {
				[chain addObjectsFromArray:tmpStack];
			}
			[tmpStack removeAllObjects];
		}
		[tmpStack addObject:pv];
		prevY = p.y;
	}
	if([tmpStack count] >= kGameboardMinSequence) {
		[chain addObjectsFromArray:tmpStack];	
	}
//	[tmpStack release];
    [tmpStack removeAllObjects];
    tmpStack = nil;
	
	//	NSLog(@"chain = %@", chain);
	
//	return [chain autorelease];
    return chain;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (NSArray *)_findAllChainsFromPoint:(CGPoint)point
{
	NSArray *sequences = [self _floodFill:point color:_board[(NSInteger)point.x][(NSInteger)point.y]];
	return [self _findAllChainsForSequence:sequences];
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (NSDictionary *)_findAllSequences
{
//	[self printBoard];
	
	NSUInteger x, y;
	BOOL visited[GAMEBOARD_NUM_ROWS][GAMEBOARD_NUM_COLS];
	for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
		for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
			visited[x][y] = NO;
		}
	}
	
	NSMutableDictionary *matchesByPos = [[NSMutableDictionary alloc] init];
	for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
		for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
			NSArray *matches = [self _floodFill:(CGPoint){x, y} color:_board[x][y]];
			if([matches count] > 0) {
				[matchesByPos setObject:matches forKey:NSStringFromCGPoint((CGPoint){x,y})];
			}
		}
	}
//	return [matchesByPos autorelease];
    return matchesByPos;
}

////////////////////////////////////////////////////////////////////////////////
// Inspects the whole board and returns a list of all current chains 
// [source_point] => [list of points that comprise the chain]
////////////////////////////////////////////////////////////////////////////////
- (NSMutableDictionary *)_findAllChains
{
	NSUInteger x, y;
	NSMutableDictionary *matchesByPos = [[NSMutableDictionary alloc] init];
	for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
		for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
			NSArray *sequence = [self _floodFill:(CGPoint){x, y} color:_board[x][y]];
			if([sequence count] == 0) continue;
			
			NSArray *chains = [self _findAllChainsForSequence:sequence];
			if([chains count] == 0) continue;
			
			[matchesByPos setObject:chains forKey:NSStringFromCGPoint((CGPoint){x,y})];
		}
	}
	return matchesByPos;// autorelease];
}

- (NSMutableDictionary *)_findAllChainsEasy
{
	NSUInteger x, y;
	NSMutableDictionary *matchesByPos = [[NSMutableDictionary alloc] init];
	for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
		for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
			NSArray *sequence = [self _floodFill:(CGPoint){x, y} color:_board[x][y]];
			if([sequence count] == 0) continue;
			
//			NSArray *chains = [self _findAllChainsForSequence:sequence];
//			if([chains count] == 0) continue;
			
			[matchesByPos setObject:sequence forKey:NSStringFromCGPoint((CGPoint){x,y})];
		}
	}
	return matchesByPos;// autorelease];
}

////////////////////////////////////////////////////////////////////////////////
// Inspects the whole board and returns a collection of all valid moves:
// [point] => [list of all valid adjacent swaps]
// NOTE: consider turning this in an updateAllValidMoves method and cache the results
// any subsequent movement attempt would only require a dictionary lookup
// after all chains are processed, we would invoke this method agan to recompute
// all valid moves - this would also make the job of handing tips to the user a lot easier
////////////////////////////////////////////////////////////////////////////////
- (NSDictionary *)_findAllValidMoves
{
	[self _updateLegalMoves];
	return _validMovesLookupTable;
}

////////////////////////////////////////////////////////////////////////////////
// todo: add proper delays between relevant calls
// we need animations to flow smoothly (clearing chains, dropping dangling gems, dropping replacement gems, etc.)
// after swapping gems and clearing the produced chains and dropping dangling gems
// we also need to check whether new chains were formed as a result and clear them too0
////////////////////////////////////////////////////////////////////////////////
- (void)_findAndClearAllComboChains
{
    
	double delayInSeconds = kfindAndClearAllComboChains;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
	dispatch_after(popTime, dispatch_get_current_queue(), ^(void){
//		CCLOG(@"Finding combo chains...");
        
        if(!virgin){
            [self lockGems];
        }
        _isbusy = YES;

        NSDictionary *comboChains;
            comboChains = [self _findAllChains];
		if([comboChains count] == 0) {
//			CCLOG(@"No combo chains found.");
            if(!virgin){
                [self unlockGems];
            }
            _isbusy = NO;
			return;
		}
        
		
//		CCLOG(@"Combo chains found: %@", comboChains);
		
		for(NSString *pointStr in [comboChains allKeys]) {
            [self checkExplosives:[comboChains objectForKey:pointStr] addSpecial:NO];
            if(![self updateSpecial]){
                [self clearChain:CGPointFromString(pointStr) sequence:[comboChains objectForKey:pointStr]];
            }
		}
		[self _dropDanglingGems];
		[self _generateAndDropDownGemsForClearedChains];
        [self _animateAllPendingChanges];
        
		[self _findAndClearAllComboChains]; // recursively invoke the function
	});
}

////////////////////////////////////////////////////////////////////////////////
// Note: this needs optimizing, we're currently doing a lot of redundant checks
////////////////////////////////////////////////////////////////////////////////


- (NSDictionary *)_findOneMove
{
    NSMutableDictionary * dict = [NSMutableDictionary new];
    
	NSUInteger x, y;
	for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
		for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
			NSMutableArray *movesForPoint = [[NSMutableArray alloc] initWithCapacity:4];
			CGPoint p = (CGPoint){x, y};
			if([self _isLegalMove:p p2:(CGPoint){x+1, y}]) { // swap with right gem
				[movesForPoint addObject:NSStringFromCGPoint((CGPoint){x+1, y})];
			}
			if([self _isLegalMove:p p2:(CGPoint){x-1, y}]) { // swap with left gem
				[movesForPoint addObject:NSStringFromCGPoint((CGPoint){x-1, y})];
			}
			if([self _isLegalMove:p p2:(CGPoint){x, y+1}]) { // swap with gem below
				[movesForPoint addObject:NSStringFromCGPoint((CGPoint){x, y+1})];
			}
			if([self _isLegalMove:p p2:(CGPoint){x, y-1}]) { // swap with gem above
				[movesForPoint addObject:NSStringFromCGPoint((CGPoint){x, y-1})];
			}
			if([movesForPoint count] > 0) {
				[dict setObject:movesForPoint forKey:NSStringFromCGPoint(p)];
			}
            movesForPoint = nil;

		}
	}
    return dict;
}

- (void)_updateLegalMoves
{
	[_validMovesLookupTable removeAllObjects];
	[_legalMovesLookupTable removeAllObjects];
	
	NSUInteger x, y;
	for(x = 0; x < GAMEBOARD_NUM_COLS; x++) {
		for(y = 0; y < GAMEBOARD_NUM_ROWS; y++) {
			NSMutableArray *movesForPoint = [[NSMutableArray alloc] initWithCapacity:4];
			CGPoint p = (CGPoint){x, y};
			if([self _isLegalMove:p p2:(CGPoint){x+1, y}]) { // swap with right gem
				[movesForPoint addObject:NSStringFromCGPoint((CGPoint){x+1, y})];
			}
			if([self _isLegalMove:p p2:(CGPoint){x-1, y}]) { // swap with left gem
				[movesForPoint addObject:NSStringFromCGPoint((CGPoint){x-1, y})];
			}
			if([self _isLegalMove:p p2:(CGPoint){x, y+1}]) { // swap with gem below
				[movesForPoint addObject:NSStringFromCGPoint((CGPoint){x, y+1})];
			}
			if([self _isLegalMove:p p2:(CGPoint){x, y-1}]) { // swap with gem above
				[movesForPoint addObject:NSStringFromCGPoint((CGPoint){x, y-1})];
			}
			if([movesForPoint count] > 0) {
				[_validMovesLookupTable setObject:movesForPoint forKey:NSStringFromCGPoint(p)];
			}
//			[movesForPoint release];
            movesForPoint = nil;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////////////
- (void)_markMove:(CGPoint)point1 to:(CGPoint)point2 legal:(BOOL)legal
{
	NSString *point1Str = NSStringFromCGPoint(point1);
	NSString *point2Str = NSStringFromCGPoint(point2);
	
	void (^updateLookupTableBlock)(NSString *fromPoint, NSString *toPoint, BOOL legal);
	updateLookupTableBlock = ^(NSString *fromPointStr, NSString *toPointStr, BOOL legal) {
		NSString *key = [NSString stringWithFormat:@"%@-%@", fromPointStr, toPointStr];
		[_legalMovesLookupTable setObject:[NSNumber numberWithBool:legal] forKey:key];
	};
	
	updateLookupTableBlock(point1Str, point2Str, legal);
	updateLookupTableBlock(point2Str, point1Str, legal);
}

////////////////////////////////////////////////////////////////////////////////
// Checks whether a given move was already computed (marked)
////////////////////////////////////////////////////////////////////////////////
- (BOOL)_lookupMove:(CGPoint)point1 toPoint:(CGPoint)point2
{
	NSString *point1Str = NSStringFromCGPoint(point1);
	NSString *point2Str = NSStringFromCGPoint(point2);
	
	NSString *key = [NSString stringWithFormat:@"%@-%@", point1Str, point2Str];
	return [_legalMovesLookupTable objectForKey:key] != nil;
}

////////////////////////////////////////////////////////////////////////////////
// Checks whether performing a p1 <-> p2 swap would result in a valid move
////////////////////////////////////////////////////////////////////////////////
- (BOOL)_isLegalMove:(CGPoint)p1 p2:(CGPoint)p2
{
	if([self _lookupMove:p1 toPoint:p2]) {
		NSString *lookupKey = [NSString stringWithFormat:@"%@-%@", NSStringFromCGPoint(p1), NSStringFromCGPoint(p2)];
		return [[_legalMovesLookupTable objectForKey:lookupKey] boolValue];
	}
	
	CGRect bounds = CGRectMake(0, 0, GAMEBOARD_NUM_COLS, GAMEBOARD_NUM_ROWS);
	BOOL gameboardContainsPoints = CGRectContainsPoint(bounds, p1) && CGRectContainsPoint(bounds, p2);
	if(!gameboardContainsPoints) {
		return NO;
	}

	CC_SWAP(_board[(NSInteger)p1.x][(NSInteger)p1.y], _board[(NSInteger)p2.x][(NSInteger)p2.y]);
	
	NSArray *p1Sequences = [self _floodFill:p1 color:_board[(NSInteger)p1.x][(NSInteger)p1.y]];
	NSArray *p2Sequences = [self _floodFill:p2 color:_board[(NSInteger)p2.x][(NSInteger)p2.y]];	
	
	NSArray *p1Chain = [self _findAllChainsForSequence:p1Sequences];
	NSArray *p2Chain = [self _findAllChainsForSequence:p2Sequences];
	
	CC_SWAP(_board[(NSInteger)p2.x][(NSInteger)p2.y], _board[(NSInteger)p1.x][(NSInteger)p1.y]);
	
	BOOL isLegalMove = ([p1Chain count] + [p2Chain count] > 0);
	[self _markMove:p1 to:p2 legal:isLegalMove];	
	return isLegalMove;
	
//	return ([p1Chain count] + [p2Chain count] > 0);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)_animateAllPendingChanges
{
    
    
    double delayInSeconds = kClearChainAnimationDelay;
    
    if(_gemDestructionQueue.count==1){
        Gem* gem = [_gemDestructionQueue objectAtIndex:0];
        if(gem.gemColor == GemColorWhite){
            delayInSeconds = 0;
        }
    }
    
    if(_gemDestructionQueue.count>0){
        BOOL redExplode = NO;
        BOOL greenExplode = NO;
        BOOL normalExplode = NO;
        ccColor3B explodecolor = ccWHITE;
        
        for(Gem* gem in _gemDestructionQueue){
            if(gem.gemColor == GemColorRedNinja || gem.gemColor == GemColorGreenNinja){
                delayInSeconds = kClearChainAnimationDelay*3;
                if(gem.gemColor == GemColorRedNinja){
                    redExplode = YES;
                } else {
                    greenExplode = YES;
                }
                break;
            }
        }
        if(redExplode){
            for(Gem* gem in _gemDestructionQueue){
                gem.color = ccRED;
            }
        } else if(greenExplode){
            for(Gem* gem in _gemDestructionQueue){
                gem.color = ccGREEN;
            }
        } else {
            for(Gem* gem in _gemDestructionQueue){
                if(gem.attributes == GemBoostExplosive){
                    normalExplode = YES;
                    explodecolor = gem.spriteColor;
                    break;
                }
            }
            if(normalExplode){
                delayInSeconds = kClearChainAnimationDelay*2;
                for(Gem* gem in _gemDestructionQueue){
                    gem.color = explodecolor;
                }
//                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Awesome];

            }

            
        }
        
    }
    
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        

        [self doAnimations:delayInSeconds];
        
        
        [timer invalidate];
        timer = nil;
        timer = [NSTimer scheduledTimerWithTimeInterval: 60.00f target: self selector: @selector(showHelper) userInfo:nil repeats: YES] ;
        

        
    });

    

}

-(void) showHelper
{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_current_queue(), ^(void){
        
        
        NSDictionary* moves = [self _findAllValidMoves];;
        
        BOOL anySpecials = NO;
        
        for(Gem* gem in _gems){
            if(gem.kind == GemKindSpecialPower){
                anySpecials = YES;
            }
        }
        
//        for(Gem* gem in _gems){
//            gem.color = ccWHITE;
//        }
        
        for(NSString *pointStr in [moves allKeys]) {
            CGPoint p = CGPointFromString(pointStr);
            NSInteger gemIndex = GemIndexForBoardPosition(p);
            Gem* gem = [_gems objectAtIndex:gemIndex];
//            gem.color = ccRED;
            [gem wobble];
            
            NSArray *chain = [moves objectForKey:pointStr];
            for(NSString *pointStr2 in chain){
                CGPoint p2 = CGPointFromString(pointStr2);
                NSInteger gemIndex2 = GemIndexForBoardPosition(p2);
                Gem* gem2 = [_gems objectAtIndex:gemIndex2];
//                gem2.color = ccYELLOW;
                [gem2 wobble];
            }
            break;
        }
        
        
        if([moves count] == 0 && !anySpecials)
        {
            [self displayToast:@"No moves left\nReshuffling" : CGPointMake(s.width/2, s.height/2) andColor:ccWHITE andDelay:3];
            
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_current_queue(), ^(void){
                [self lockGems];
                [self reShuffle];
                [self unlockGems];
            });
            
        }
    });

}

-(void) reShuffle
{

    
    
    NSMutableArray* used = [NSMutableArray new];
    
    NSUInteger count = [_gems count];
    for (int i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        int nElements = count - i;
        int n = (arc4random() % nElements) + i;
        
        if([used containsObject:[NSNumber numberWithInt:n]]){
            continue;
        }
        if([used containsObject:[NSNumber numberWithInt:i]]){
            continue;
        }
        
        [used addObject:[NSNumber numberWithInt:n]];
        [used addObject:[NSNumber numberWithInt:i]];
        
        
        Gem* gem1 = [_gems objectAtIndex:i];
        Gem* gem2 = [_gems objectAtIndex:n];
        
        CC_SWAP(_board[(NSInteger)gem1.point.x][(NSInteger)gem1.point.y], _board[(NSInteger)gem2.point.x][(NSInteger)gem2.point.y]);
        
        CC_SWAP(gem1.point, gem2.point);
        
        [_gems exchangeObjectAtIndex:i withObjectAtIndex:n];
        
        
        id swapGem1Action = [CCMoveTo actionWithDuration:0.4 position:gem2.position];
        id swapGem2Action = [CCMoveTo actionWithDuration:0.4 position:gem1.position];
        
        [gem1 runAction:swapGem1Action];
        [gem2 runAction:swapGem2Action];
        
    }
}

-(void) doAnimations:(double) delayInSeconds
{
    int chainscore = 0;
    CGPoint toastPos = CGPointMake(0, 0);
    int count = 0;
    for(Gem *gem in _gemDestructionQueue) {
        if(!virgin){
            _score+=gem.points;
            _levelscore+=gem.points;
        }
        //            if(count == round(_gemDestructionQueue.count/2))
        //                toastPos = gem.position;
        toastPos.x += gem.position.x;
        toastPos.y += gem.position.y;
        chainscore+=gem.points;
        //            gem.color = ccWHITE; //gem.spriteColor;
        
        switch (gem.gemColor) {
            case GemColorRed:
            case GemColorMagenta:
                [gem runAction: [CCSequence actions:
                                 [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:0.0f],
                                 [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                 nil]];
                break;
            case GemColorGreen:
            case GemColorCyan:
                [gem runAction: [CCSequence actions:
                                 [CCFadeOut actionWithDuration:kClearGemAnimationDuration],
                                 nil]];
                [gem runAction: [CCSequence actions:
                                 [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:1.5f],
                                 [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                 nil]];
                break;
            case GemColorBlue:
            case GemColorPurple:
                [gem runAction: [CCSequence actions:
                                 [CCFadeOut actionWithDuration:kClearGemAnimationDuration],
                                 [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                 nil]];
                break;
            case GemColorYellow:
            case GemColorOrange:
                [gem runAction: [CCSequence actions:
                                 [CCBlink actionWithDuration:kClearGemAnimationDuration blinks:100],
                                 [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                 nil]];
                break;
            case GemColorGrey:
                [gem runAction: [CCSequence actions:
                                 [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:0.0f],
                                 [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                 nil]];
                break;
            case GemColorBlack:
            {
                
                if (gem.directionH) {
                    //clear left or right?
                    if(gem.point.x < 4){ //clear right
                        gem.zOrder = 101;
                        [self addChild:gem.streak z:1 tag:kNinjaRight];
                        [gem runAction: [CCSequence actions:
                                         [CCMoveTo actionWithDuration:kClearGemAnimationDuration position:CGPointMake(s.width, gem.position.y)],
                                         [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                         [CCCallFunc actionWithTarget:self selector:@selector(cleanUp)],
                                         nil]];
                        
                    } else { //clear left
                        gem.zOrder = 101;
                        [self addChild:gem.streak z:1 tag:kNinjaLeft];
                        [gem runAction: [CCSequence actions:
                                         [CCMoveTo actionWithDuration:kClearGemAnimationDuration position:CGPointMake(-gem.boundingBox.size.width, gem.position.y)],
                                         [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                         [CCCallFunc actionWithTarget:self selector:@selector(cleanUp)],
                                         nil]];
                        
                    }
                    
                } else {
                    //clear up or down?
                    if(gem.point.y < 4){ //clear up
                        gem.zOrder = 101;
                        [self addChild:gem.streak z:1 tag:kNinjaUp];
                        [gem runAction: [CCSequence actions:
                                         [CCMoveTo actionWithDuration:kClearGemAnimationDuration position:CGPointMake(gem.position.x,-gem.boundingBox.size.height)],
                                         [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                         [CCCallFunc actionWithTarget:self selector:@selector(cleanUp)],
                                         nil]];
                        
                    } else { //clear down
                        gem.zOrder = 101;
                        [self addChild:gem.streak z:1 tag:kNinjaDown];
                        
                        [gem runAction: [CCSequence actions:
                                         [CCMoveTo actionWithDuration:kClearGemAnimationDuration position:CGPointMake(gem.position.x,s.height)],
                                         [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                         [CCCallFunc actionWithTarget:self selector:@selector(cleanUp)],
                                         nil]];
                    }
                    
                }
                
                break;
            }
            case GemColorWhite:
                //Vampire
                gem.zOrder = 101;
                [gem runAction: [CCSequence actions:
                                 [CCScaleTo actionWithDuration:kClearGemAnimationDuration scale:0],
                                 nil]];
                [gem runAction: [CCSequence actions:
                                 [CCRotateTo actionWithDuration:kClearGemAnimationDuration angle:180],
                                 [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                 nil]];
                break;
            case GemColorRedNinja:
                //Red Ninja
                gem.zOrder = 102;
                [gem runAction: [CCSequence actions:
                                  [CCFadeTo actionWithDuration:0.8 opacity:0],
                                  nil]];
                [gem runAction: [CCSequence actions:
                                  [CCScaleTo actionWithDuration:0.8 scale:8],
                                  [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                  nil]];
                break;
            case GemColorGreenNinja:
                gem.zOrder = 102;
                if(gem.point.x < 4){ //clear right
                    [gem runAction: [CCSequence actions:
                                     [CCMoveTo actionWithDuration:kClearGemAnimationDuration position:CGPointMake(gem.position.x+kGameboardCellSize.width*2, gem.position.y)],
                                     [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                     nil]];
                    
                } else { //clear left
                    [gem runAction: [CCSequence actions:
                                     [CCMoveTo actionWithDuration:kClearGemAnimationDuration position:CGPointMake(gem.position.x-kGameboardCellSize.width*2, gem.position.y)],
                                     [CCCallFunc actionWithTarget:gem selector:@selector(removeFromParent)],
                                     nil]];
                    
                }

                break;
            default:
                break;
        }
//        if(gem.attributes == GemBoostExplosive){
//            [self displayToast:@"KABOOM" : gem.position andColor:gem.spriteColor];
//        }
        
        count++;
        
        //            _board[(NSInteger)gem.point.x][(NSInteger)gem.point.y] = -1;
        //            NSInteger gemIndex = GemIndexForBoardPosition(gem.point);
        //            [_gems replaceObjectAtIndex:gemIndex withObject:[NSNull null]];
        
    }
    toastPos.x = toastPos.x / _gemDestructionQueue.count;
    toastPos.y = toastPos.y / _gemDestructionQueue.count;
    
    if(_gemDestructionQueue.count > 0){
        
        Gem* g = [_gemDestructionQueue objectAtIndex:0];
        if(g.attributes == GemBoostExplosive){
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Awesome];
        } else {
        switch (g.gemColor) {
            case GemColorRed:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Yeah];
                break;
            case GemColorMagenta:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Sweet];
                break;
            case GemColorGreen:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Yipee];
                break;
            case GemColorCyan:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Nice];
                break;
            case GemColorBlue:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Woohoo];
                break;
            case GemColorPurple:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Dude];
                break;
            case GemColorYellow:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Cool];
                break;
            case GemColorOrange:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Yo];
                break;
            case GemColorGrey:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Awesome];
                break;
            case GemColorBlack:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Sword];
                break;
            case GemColorWhite:
                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Bleeh];
                break;
            case GemColorRedNinja:
//                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Sword];
                 [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Awesome];
                break;
            case GemColorGreenNinja:
//                [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Sword];
                 [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Awesome];
                break;
            default:
                break;
        }
        }
        

        
    }
    
    
    if (_gemDestructionQueue.count>0) {
        
        if(virgin && helpCounter==2){
            [self showHelp:@"Match more than 3 faces\nto get a booster!"];
            //                [self unlockGems];
            //                return;
        }
        if(_gemDestructionQueue.count>3){
            [self displayToast:[NSString stringWithFormat:@"%d chain",_gemDestructionQueue.count]:CGPointMake(220*kScale,396*kScale) andColor:ccWHITE];
            chainscore += (_gemDestructionQueue.count*chainBonus);
            _score += (_gemDestructionQueue.count*chainBonus);
            _levelscore += (_gemDestructionQueue.count*chainBonus);
        }
    }
    
    [_gemDestructionQueue removeAllObjects];
    
    [self displayToast:[NSString stringWithFormat:@"%d",chainscore] :toastPos andColor:ccWHITE];
    
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        
        for(NSArray *dropAction in _gemDropdownQueue) {
            Gem *gem = [dropAction objectAtIndex:0];
            gem.active = NO;
            CCAction *action = [dropAction objectAtIndex:1];
            [gem runAction:action];
            [gem runAction:[CCSequence actions:
                            [CCDelayTime actionWithDuration:kDropNewGemAnimationDuration],
                            [CCCallFunc actionWithTarget:gem selector:@selector(makeActive)],
                            nil]];
        }
        
        [_gemDropdownQueue removeAllObjects];
        
        
    });
}

-(void) displayToast:(NSString*)message :(CGPoint) labelPosition andColor:(ccColor3B)color
{
    [self displayToast:message :labelPosition andColor:color andDelay:0.2];
}

-(void) displayToast:(NSString*)message :(CGPoint) labelPosition andColor:(ccColor3B)color andDelay:(float)delay
{

    
    Toast* label = [Toast labelWithString:message fontName:@"Marker Felt" fontSize:24*kScale];
    [label setStringWithStroke:message];
    label.color = color;
    label.position = labelPosition;
    label.opacity = 0;
    [self addChild:label z:102];
    [label runAction: [CCSequence actions:
                    [CCFadeIn actionWithDuration:0.4],
                    [CCDelayTime actionWithDuration:delay],
                    [CCFadeOut actionWithDuration:0.4],
                    [CCCallFunc actionWithTarget:label selector:@selector(removeFromParent)],
                     nil]];
        
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)_enqueueGemDestructionAnimation:(Gem *)gem
{
	[_gemDestructionQueue addObject:gem];
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)_enqueueDropGemAnimation:(Gem *)gem point:(CGPoint)dropDestination
{
	CCMoveTo *dropAction = [CCMoveTo actionWithDuration:kDropNewGemAnimationDuration position:CoordinatesForGemAtPosition(dropDestination)];
	[_gemDropdownQueue addObject:[NSArray arrayWithObjects:gem, dropAction, nil]];
}


-(void) lockGems
{
    for (Gem* gem in _gems){
        if(gem != (id)[NSNull null] && gem.gemColor != GemColorWhite) gem.active = NO;
    }
}

-(void) unlockGems
{
    for (Gem* gem in _gems){
        if(gem != (id)[NSNull null]) gem.active = YES;
    }
}


//- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
//    return YES;
//}
//
//
//-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
//{
//    if (endGame) {
//        [self endgame];
//    }
//}


- (void) checkAchievements
{
    
    if(virgin) return;
	NSString* identifier = NULL;
	double percentComplete = 0;
	switch(_chains)
	{
		case 3:
		{
			identifier= kAchievement3Chain;
			percentComplete= 100.0;
			break;
		}
		case 4:
		{
			identifier= kAchievement4Chain;
			percentComplete= 100.0;
			break;
		}
        case 5:
		{
			identifier= kAchievement5Chain;
			percentComplete= 100.0;
//            [Appirater userDidSignificantEvent:YES];
			break;
		}
        case 6:
		{
			identifier= kAchievement6Chain;
			percentComplete= 100.0;
			break;
		}
        case 7:
		{
			identifier= kAchievement7Chain;
			percentComplete= 100.0;
			break;
		}
        case 8:
		{
			identifier= kAchievement8Chain;
			percentComplete= 100.0;
			break;
		}
        case 9:
		{
			identifier= kAchievement9Chain;
			percentComplete= 100.0;
			break;
		}
        case 10:
		{
			identifier= kAchievement10Chain;
			percentComplete= 100.0;
			break;
		}
        case 11:
		{
			identifier= kAchievement11Chain;
			percentComplete= 100.0;
			break;
		}
        case 12:
		{
			identifier= kAchievement12Chain;
			percentComplete= 100.0;
			break;
		}
        case 13:
		{
			identifier= kAchievement13Chain;
			percentComplete= 100.0;
			break;
		}
        case 14:
		{
			identifier= kAchievement14Chain;
			percentComplete= 100.0;
			break;
		}
        case 15:
		{
			identifier= kAchievement15Chain;
			percentComplete= 100.0;
			break;
		}
        case 16:
		{
			identifier= kAchievement16Chain;
			percentComplete= 100.0;
			break;
		}
        case 17:
		{
			identifier= kAchievement17Chain;
			percentComplete= 100.0;
			break;
		}
        case 18:
		{
			identifier= kAchievement18Chain;
			percentComplete= 100.0;
			break;
		}
        case 19:
		{
			identifier= kAchievement19Chain;
			percentComplete= 100.0;
			break;
		}
        case 20:
		{
			identifier= kAchievement20Chain;
			percentComplete= 100.0;
			break;
		}
        case 21:
		{
			identifier= kAchievement21Chain;
			percentComplete= 100.0;
			break;
		}
        case 22:
		{
			identifier= kAchievement22Chain;
			percentComplete= 100.0;
			break;
		}
        case 23:
		{
			identifier= kAchievement23Chain;
			percentComplete= 100.0;
			break;
		}
        case 24:
		{
			identifier= kAchievement24Chain;
			percentComplete= 100.0;
			break;
		}
        case 25:
		{
			identifier= kAchievement25Chain;
			percentComplete= 100.0;
			break;
		}
        case 26:
		{
			identifier= kAchievement26Chain;
			percentComplete= 100.0;
			break;
		}
        case 27:
		{
			identifier= kAchievement27Chain;
			percentComplete= 100.0;
			break;
		}
        case 28:
		{
			identifier= kAchievement28Chain;
			percentComplete= 100.0;
			break;
		}
        case 29:
		{
			identifier= kAchievement29Chain;
			percentComplete= 100.0;
			break;
		}
        case 30:
		{
			identifier= kAchievement30Chain;
			percentComplete= 100.0;
			break;
		}
        case 31:
		{
			identifier= kAchievement31Chain;
			percentComplete= 100.0;
			break;
		}
        case 32:
		{
			identifier= kAchievement32Chain;
			percentComplete= 100.0;
			break;
		}
        case 33:
		{
			identifier= kAchievement33Chain;
			percentComplete= 100.0;
			break;
		}
        case 34:
		{
			identifier= kAchievement35Chain;
			percentComplete= 100.0;
			break;
		}
        case 36:
		{
			identifier= kAchievement36Chain;
			percentComplete= 100.0;
			break;
		}
        case 37:
		{
			identifier= kAchievement37Chain;
			percentComplete= 100.0;
			break;
		}
        case 38:
		{
			identifier= kAchievement38Chain;
			percentComplete= 100.0;
			break;
		}
        case 39:
		{
			identifier= kAchievement39Chain;
			percentComplete= 100.0;
			break;
		}
        case 40:
		{
			identifier= kAchievement40Chain;
			percentComplete= 100.0;
			break;
		}
	}
	if(identifier!= NULL)
	{
		[app.gameCenterManager submitAchievement: identifier percentComplete: percentComplete];
	}
    
    
    if(_chains>=5){
        [app addRandomBooster];
    }
    
    
}


-(void) cleanUp
{
    if([self getChildByTag:kNinjaRight]){
        [self removeChildByTag:kNinjaRight cleanup:YES];
    }
    if([self getChildByTag:kNinjaLeft]){
        [self removeChildByTag:kNinjaLeft cleanup:YES];
    }
    if([self getChildByTag:kNinjaDown]){
        [self removeChildByTag:kNinjaDown cleanup:YES];
    }
    if([self getChildByTag:kNinjaUp]){
        [self removeChildByTag:kNinjaUp cleanup:YES];
    }
    if([self getChildByTag:kNinjTut]){
        [self removeChildByTag:kNinjTut cleanup:YES];
    }
    if([self getChildByTag:kVampTut]){
        [self removeChildByTag:kVampTut cleanup:YES];
    }
}
//- (void) achievementSubmitted: (GKAchievement*) ach error:(NSError*) error;
//{
//    
//	if((error == NULL) && (ach != NULL))
//	{
//		if (ach.percentComplete == 100.0) {
////			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Achievement Earned!" 
////                                                            message:(@"%@",ach.identifier)
////                                                           delegate:nil 
////                                                  cancelButtonTitle:@"OK" 
////                                                  otherButtonTitles:nil];
////			[alert show];
//            
//            
////            [chainLabel setString:[NSString stringWithFormat:@"%@",ach.identifier]];
//            
//            [self displayToast:@"Achievement Earned!" :CGPointMake(s.width/2, s.height/2)];
//            
//		}
//		
//	}
//	else
//	{
//		// Achievement Submission Failed.
//        
//	}
//    
//}

@end
