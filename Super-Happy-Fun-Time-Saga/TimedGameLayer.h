//
//  TimedGameLayer.h
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 12/04/2014.
//
//

#import "CCLayer.h"

@interface TimedGameLayer : CCLayer

+(CCScene *) scene;

@end
