//
//  HelloWorldLayer.h
//  matchmania
//
//  Created by Rui Alho on 19/07/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "AppDelegate.h"


// HelloWorldLayer
@interface HelloWorldLayer : CCLayer
{
    
    enum GameStatePP _state;
}

@property(nonatomic) enum GameStatePP state;

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
