//
//  HelpLayer.m
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 10/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HelpLayer.h"
#import "SimpleAudioEngine.h"
#import "AboutLayer.h"

@implementation HelpLayer

// Helper class method that creates a Scene with the AboutLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelpLayer *layer = [HelpLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
    // always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {

        // ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];

#ifndef ANDROID
        AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
        
        [app.tracker set:kGAIScreenName
                   value:@"Help"];
        
        [app.tracker send:[[GAIDictionaryBuilder createAppView] build]];
#else
//        [[DAGoogleAnalytics defaultTracker] sendAppView:@"Help"];
#endif
        
        CCSprite * bg;
        if(IS_IPHONE_5){
            bg = [CCSprite spriteWithFile:@"startpage-iphone5.jpg"];
        } else {
            bg = [CCSprite spriteWithFile:@"startpage.jpg"];
        }
        [bg setPosition:ccp(size.width/2, size.height/2)];
        [self addChild:bg z:-100];
        
        
        // create and initialize a Label
		CCLabelTTFWithStroke *label = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:20*kScale];
        [label setStringWithStroke:@"Tap a Super Happy Face\nto change its color.\n\nMatch three or more of\na kind to clear them.\n\nNinja's will clear out\na line of faces\n\nWatch out for Vampires\nthey will drain your score"];
		// position the label on the center of the screen
		label.position =  ccp( size.width /2 , 190*kScale + (IS_IPHONE_5?40:0) );
		
        
		// add the label as a child to this Layer
		[self addChild: label];
        
        
        
        CCMenuItemFontWithStroke *itemBack = [CCMenuItemFontWithStroke itemWithString:@"Back" color:ccWHITE block:^(id sender) {
            
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
            [[CCDirector sharedDirector] popSceneWithTransition:[CCTransitionJumpZoom class] duration:1.0];

            
        }];
        itemBack.color = ccBLACK;
        
        CCMenu *menu = [CCMenu menuWithItems:itemBack, nil];
		
		[menu alignItemsVerticallyWithPadding:10*kScale];
		[menu setPosition:ccp( size.width/2, 40*kScale + (IS_IPHONE_5?30:0))];
		
		// Add the menu to the layer
		[self addChild:menu];
        
    }
    return self;
}

@end
