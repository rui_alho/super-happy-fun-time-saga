//
//  Util.h
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 10/04/2014.
//
//

#import <Foundation/Foundation.h>
#import "CCRenderTexture.h"
#import "cocos2d.h"
#import "AGSprite.h"

@interface Util : NSObject

+(CCRenderTexture*) createStroke: (CCLabelTTF*) label   size:(float)size   color:(ccColor3B)cor;
+(CCSprite*)labelWithString:(NSString *)string fontName:(NSString *)fontName fontSize:(CGFloat)fontSize color:(ccColor3B)color strokeSize:(CGFloat)strokeSize stokeColor:(ccColor3B)strokeColor;
//+(CCSprite*)spriteForLabel:(CCLabelTTF*) label strokeSize:(CGFloat)strokeSize stokeColor:(ccColor3B)strokeColor ;
+(AGSprite*)spriteForLabel:(CCLabelTTF*) label strokeSize:(CGFloat)strokeSize stokeColor:(ccColor3B)strokeColor;

@end
