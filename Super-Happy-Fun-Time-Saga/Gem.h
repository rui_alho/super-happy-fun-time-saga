//
//  Gem.h
//  Chainz
//
//  Created by Pedro Gomes on 12/13/11.
//  Copyright (c) 2011 Phluid Labs. All rights reserved.
//

#import "cocos2d.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
typedef enum {
	GemKindNormal,
	GemKindEmpty,
	GemKindSpecialPower,
	GemKindCount // must be last
} GemKind; // not yet being used

typedef enum {
	GemColorRed, //
	GemColorGreen, //
	GemColorBlue, //
	GemColorMagenta,
	GemColorYellow, //
	GemColorOrange, //
    
    
    GemColorPurple, //
    GemColorCount,
    
    
    GemColorGrey,
	GemColorCyan, //
    
	GemColorBlack, //
	GemColorWhite, //
    
    GemColorRedNinja, //
    GemColorGreenNinja, //
    
	GemColorClear = -1, // ensure it's always defined after GemColorCount
	GemColorInvalid = -2
} GemColor;

typedef enum {
	GemBoostNone,
	GemBoostExplosive,
} GemAttribute;


////////////////////////////////////////////////////////////////////////////////
// Utility functions
////////////////////////////////////////////////////////////////////////////////
extern NSString *GemKindString(GemKind kind);
extern NSString *GemColorString(GemColor color);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
@class GameBoard;
@interface Gem : CCSprite <CCTargetedTouchDelegate>
{
	GemKind				_kind;
	GemColor			_color;
	GemAttribute		_attributes;
	
	GameBoard	*_gameboard;
	CGPoint				_point;
	
	BOOL				_selected;
	BOOL				_moved;
	CGPoint				_firstTouchLocation;
    
    CCLabelTTF *label;
//     NSTimer* timer;
    CCParticleSystem	*emitter_;
    BOOL _active;
//    BOOL _remove;
    
    CGPoint startLocation;
    CGPoint endLocation;
    
    NSInteger _points;
    
    ccColor3B spriteColor;
    CCMotionStreak* _streak;
    
    BOOL				_directionH;
    BOOL				_needsUpdate;
    BOOL                _showVamp;
    
    CCLayerColor* selectionBlock;
    
    
}

@property (readwrite,retain) CCParticleSystem *emitter;
@property (nonatomic, readonly) GemKind kind;
@property (nonatomic, readonly) GemColor gemColor;
@property (nonatomic, assign) GemAttribute attributes;
@property (nonatomic, assign) CGPoint point;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, assign) BOOL directionH;
@property (nonatomic, assign) BOOL needsUpdate;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, assign) BOOL showVamp;
//@property (nonatomic, assign) BOOL remove;
@property (nonatomic, readonly) NSInteger points;
@property (nonatomic, readonly) ccColor3B spriteColor;
@property (nonatomic, strong) CCMotionStreak* streak;

- (id)initWithGameboard:(GameBoard *)gameboard position:(CGPoint)point kind:(GemKind)kind;
- (id)initWithGameboard:(GameBoard *)gameboard position:(CGPoint)point kind:(GemKind)kind color:(GemColor)color;
- (id)initWithGameboard:(GameBoard *)gameboard position:(CGPoint)point kind:(GemKind)kind color:(GemColor)color attributes:(GemAttribute)attribute;

- (void)updatePosition:(CGPoint)point;
- (void)markSelected:(BOOL)selected;
-(void) makeActive;

// Animations and Effects

//- (void)moveTo:(CGPoint)position;
//- (void)dropTo:(CGPoint)position;
- (void)wobble;
- (void)explode;
- (void)needsExplode;
//- (void)shrink;

@end
