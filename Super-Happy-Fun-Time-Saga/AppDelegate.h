//
//  AppDelegate.h
//  matchmania
//
//  Created by Rui Alho on 19/07/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GameCenterManager.h"
#import <GameKit/GameKit.h>
#import "GAITracker.h"
#import "GADBannerView.h"
#import "GADInterstitial.h"

#ifdef ANDROID
//#import "DAGoogleAnalytics.h"
//#import "DAMmedia.h"
#endif

#define kScale (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 1 : 2)
#define IS_IPHONE_5 ( [ [ UIScreen mainScreen ] bounds ].size.height == 568 )
#define kBannerAdUnitID (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? @"d098a464abf94b15" : @"d098a464abf94b15")
#define kInterAdUnitID @"1ffc68ea64de4900"


#define kAndroidBanner @"06bdc04211be4d4d"
#define kAndroidInter @"67e08fca9832452c"

enum GameStatePP {
    kGameStatePlaying,
    kGameStatePaused
};

@interface AppController : UIResponder <UIApplicationDelegate, CCDirectorDelegate,GameCenterManagerDelegate, GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate, GADBannerViewDelegate, GADInterstitialDelegate, UIAlertViewDelegate>
{
	UIWindow *window_;
	UINavigationController *navController_;

	CCDirectorIOS	*__unsafe_unretained director_;							// weak ref
    
    GameCenterManager *gameCenterManager;
	
	int64_t  currentScore;
	
	NSString* currentLeaderBoard;
    
    BOOL _paused;
    
    GADInterstitial *interstitial_;
    NSTimer* timer;
    NSTimer* timerUnlock;
    NSTimer* tellTimer;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (readonly) UINavigationController *navController;
@property (unsafe_unretained, readonly) CCDirectorIOS *director;

@property (nonatomic, retain) GameCenterManager *gameCenterManager;
@property (nonatomic, assign) int64_t currentScore;
@property (nonatomic, retain) NSString* currentLeaderBoard;
@property (nonatomic) BOOL paused;
//@property (nonatomic) int multiNinja;

@property(nonatomic, retain) id<GAITracker> tracker;
@property(nonatomic, strong) GADBannerView *adBanner;

- (void) checkAchievements;
- (void) submitScore;
-(void) leaderboard;
-(void) achievements;
- (void)showInterstitial;
-(void) tellAFriend;
-(void) addMultiNinja:(int)number;
-(void) addRedNinja:(int)number;
-(void) addGreenNinja:(int)number;
-(void) addRandomBooster;

@end
