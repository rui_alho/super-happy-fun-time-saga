//
//  Util.m
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 10/04/2014.
//
//

#import "Util.h"

@implementation Util


//+(CCRenderTexture*) createStroke: (CCLabelTTF*) label   size:(float)size   color:(ccColor3B)cor
//{
//    CCRenderTexture* rt = [CCRenderTexture renderTextureWithWidth:label.texture.contentSize.width+size*2  height:label.texture.contentSize.height+size*2];
//    CGPoint originalPos = [label position];
//    ccColor3B originalColor = [label color];
//    BOOL originalVisibility = [label visible];
//    [label setColor:cor];
//    [label setVisible:YES];
//    ccBlendFunc originalBlend = [label blendFunc];
//    [label setBlendFunc:(ccBlendFunc) { GL_SRC_ALPHA, GL_ONE }];
//    CGPoint bottomLeft = ccp(label.texture.contentSize.width * label.anchorPoint.x + size, label.texture.contentSize.height * label.anchorPoint.y + size);
//    CGPoint positionOffset = ccp(label.texture.contentSize.width * label.anchorPoint.x - label.texture.contentSize.width/2,label.texture.contentSize.height * label.anchorPoint.y - label.texture.contentSize.height/2);
//    CGPoint position = ccpSub(originalPos, positionOffset);
//    
//    [rt begin];
//    for (int i=0; i<360; i+=30) // you should optimize that for your needs
//    {
//        [label setPosition:ccp(bottomLeft.x + sin(CC_DEGREES_TO_RADIANS(i))*size, bottomLeft.y + cos(CC_DEGREES_TO_RADIANS(i))*size)];
//        [label visit];
//    }
//    [rt end];
//    [label setPosition:originalPos];
//    [label setColor:originalColor];
//    [label setBlendFunc:originalBlend];
//    [label setVisible:originalVisibility];
//    [rt setPosition:position];
//    return rt;
//}

+(CCRenderTexture*) createStroke: (CCLabelTTF*) label size:(float)size color:(ccColor3B)cor
{
    CCRenderTexture* rt = [CCRenderTexture renderTextureWithWidth:label.texture.contentSize.width+size*2 height:label.texture.contentSize.height+size*2];
    CGPoint originalPos = [label position];
    ccColor3B originalColor = [label color];
    float originalScaleX = [label scaleX];
    float originalScaleY = [label scaleY];
    BOOL originalVisibility = [label visible];
    [label setColor:cor];
    [label setScale:1];
    [label setVisible:YES];
    ccBlendFunc originalBlend = [label blendFunc];
    [label setBlendFunc:(ccBlendFunc) { GL_SRC_ALPHA, GL_ONE }];
    CGPoint bottomLeft = ccp(label.texture.contentSize.width * label.anchorPoint.x + size, label.texture.contentSize.height * label.anchorPoint.y + size);
    CGPoint positionOffset= ccp(label.contentSize.width/2, label.contentSize.height/2);
    [rt begin];
    for (int i=0; i<360; i+=45) // you should optimize that for your needs
    {
        [label setPosition:ccp(bottomLeft.x + sin(CC_DEGREES_TO_RADIANS(i))*size, bottomLeft.y + cos(CC_DEGREES_TO_RADIANS(i))*size)];
        [label visit];
    }
    [rt end];
    [label setPosition:originalPos];
    [label setColor:originalColor];
    [label setBlendFunc:originalBlend];
    [label setVisible:originalVisibility];
    [label setScaleX: originalScaleX];
    [label setScaleY: originalScaleY];
    rt.position = positionOffset;
    return rt;
}

+(CCSprite*)labelWithString:(NSString *)string fontName:(NSString *)fontName fontSize:(CGFloat)fontSize color:(ccColor3B)color strokeSize:(CGFloat)strokeSize stokeColor:(ccColor3B)strokeColor {
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:fontName fontSize:fontSize];
    
    CCRenderTexture* rt = [CCRenderTexture renderTextureWithWidth:label.texture.contentSize.width + strokeSize*2  height:label.texture.contentSize.height+strokeSize*2];
    
    [label setFlipY:YES];
    [label setColor:strokeColor];
    ccBlendFunc originalBlendFunc = [label blendFunc];
    [label setBlendFunc:(ccBlendFunc) { GL_SRC_ALPHA, GL_ONE }];
    
    CGPoint bottomLeft = ccp(label.texture.contentSize.width * label.anchorPoint.x + strokeSize, label.texture.contentSize.height * label.anchorPoint.y + strokeSize);
    CGPoint position = ccpSub([label position], ccp(-label.contentSize.width / 2.0f, -label.contentSize.height / 2.0f));
    
    [rt begin];
    
    for (int i=0; i<360; i++) // you should optimize that for your needs
    {
        [label setPosition:ccp(bottomLeft.x + sin(CC_DEGREES_TO_RADIANS(i))*strokeSize, bottomLeft.y + cos(CC_DEGREES_TO_RADIANS(i))*strokeSize)];
        [label visit];
    }
    
    [label setPosition:bottomLeft];
    [label setBlendFunc:originalBlendFunc];
    [label setColor:color];
    [label visit];
    
    [rt end];
    
    [rt setPosition:position];
    
    return [CCSprite spriteWithTexture:rt.sprite.texture];
    
}

+(AGSprite*)spriteForLabel:(CCLabelTTF*) label strokeSize:(CGFloat)strokeSize stokeColor:(ccColor3B)strokeColor {
    
    CCRenderTexture* rt = [CCRenderTexture renderTextureWithWidth:label.texture.contentSize.width + strokeSize*2  height:label.texture.contentSize.height+strokeSize*2];
    
    [label setFlipY:YES];
    [label setColor:strokeColor];
    ccBlendFunc originalBlendFunc = [label blendFunc];
    [label setBlendFunc:(ccBlendFunc) { GL_SRC_ALPHA, GL_ONE }];
    
    CGPoint bottomLeft = ccp(label.texture.contentSize.width * label.anchorPoint.x + strokeSize, label.texture.contentSize.height * label.anchorPoint.y + strokeSize);
    CGPoint position = ccpSub([label position], ccp(-label.contentSize.width / 2.0f, -label.contentSize.height / 2.0f));
    
    [rt begin];
    
    for (int i=0; i<360; i++) // you should optimize that for your needs
    {
        [label setPosition:ccp(bottomLeft.x + sin(CC_DEGREES_TO_RADIANS(i))*strokeSize, bottomLeft.y + cos(CC_DEGREES_TO_RADIANS(i))*strokeSize)];
        [label visit];
    }
    
    [label setPosition:bottomLeft];
    [label setBlendFunc:originalBlendFunc];
    [label setColor:ccWHITE];
    [label visit];
    
    [rt end];
    
    [rt setPosition:position];
    
    return [AGSprite spriteWithTexture:rt.sprite.texture];
    
}

@end
