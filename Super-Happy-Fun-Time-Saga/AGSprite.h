//
//  AGSprite.h
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 11/04/2014.
//
//

#import "CCSprite.h"

@interface AGSprite : CCSprite

- (void) removeFromParent;
    
@end
