//
//  main.m
//  Super-Happy-Fun-Time-Saga
//
//  Created by Rui Alho on 30/04/2014.
//  Copyright (c) 2014 AlhoGames. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppController class]));
    }
}
