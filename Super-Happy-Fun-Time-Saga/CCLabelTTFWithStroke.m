//
//  CCLabelTTFWithStroke.m
//  Super Happy Fun Time Game
//
//  Created by Rui Alho on 10/04/2014.
//
//

#import "CCLabelTTFWithStroke.h"

#define kTagStroke 1039384756

@implementation CCLabelTTFWithStroke


-(void) setStringWithStroke:(NSString *)str
{
    [super setString:str];
    
//    if ([label_ isKindOfClass: [CCLabelTTF class]]) {
    if([self getChildByTag:kTagStroke]){
        [self removeChildByTag:kTagStroke cleanup:YES];
    }
        CCRenderTexture * stroke  = [Util createStroke:(CCLabelTTF*)self size:3 color:ccBLACK];
        [self addChild:stroke z:-1 tag:kTagStroke];
    
//    }else{
//        NSLog(@"Error adding stroke in menu, label_ is not a CCLabelTTF.  This has only been tested on cocos2d 99.5");
//    }
}


-(void) setStringWithStroke:(NSString *)str color:(ccColor3B) color
{
    [super setString:str];
    if([self getChildByTag:kTagStroke]){
        [self removeChildByTag:kTagStroke cleanup:YES];
    }
    CCRenderTexture * stroke  = [Util createStroke:(CCLabelTTF*)self size:3 color:color];
    [self addChild:stroke z:-1 tag:kTagStroke];
}


@end
