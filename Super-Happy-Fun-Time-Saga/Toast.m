//
//  Toast.m
//  matchmania
//
//  Created by Rui Alho on 23/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Toast.h"

@implementation Toast


- (void) removeFromParent{
    CCNode *parent=self.parent;
    [parent removeChild:self cleanup:YES];
}

@end
