//
//  AboutLayer.m
//  matchmania
//
//  Created by Rui Alho on 31/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AboutLayer.h"
#import "HelloWorldLayer.h"
#import "SimpleAudioEngine.h"
#import "HelpLayer.h"
#import "GAIDictionaryBuilder.h"
#import "ClassicGameScene.h"

@implementation AboutLayer


// Helper class method that creates a Scene with the AboutLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	AboutLayer *layer = [AboutLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
        
        
        
        // ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        

        AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
#ifndef ANDROID
        [app.tracker set:kGAIScreenName
                   value:@"About"];
        
        [app.tracker send:[[GAIDictionaryBuilder createAppView] build]];
#else
//        [[DAGoogleAnalytics defaultTracker] sendAppView:@"About"];
#endif
        
        CCSprite * bg;
        if(IS_IPHONE_5){
            bg = [CCSprite spriteWithFile:@"startpage-iphone5.jpg"];
        } else {
            bg = [CCSprite spriteWithFile:@"startpage.jpg"];
        }
        
        [bg setPosition:ccp(size.width/2, size.height/2)];
        [self addChild:bg z:-100];
        
        // create and initialize a Label
		CCLabelTTFWithStroke *label = [CCLabelTTFWithStroke labelWithString:@"" fontName:@"Marker Felt" fontSize:36*kScale];
        [label setStringWithStroke:[NSString stringWithFormat:@"v%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]]];
        
		// position the label on the center of the screen
		label.position =  ccp( size.width /2 , size.height-200*kScale + (IS_IPHONE_5?-40:0) );
		
        
		// add the label as a child to this Layer
		[self addChild: label];
        
        CCMenuItemFontWithStroke *itemReview = [CCMenuItemFontWithStroke itemWithString:@"Review" block:^(id sender) {
#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"Review App"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                               action:@"button_press"
//                                                                label:@"Review App"];
#endif
            
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
//            NSString* url = [NSString stringWithFormat: @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d", 560822890];
//            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
            [self review];
            
        }];
        
        CCMenuItemFontWithStroke *itemTell = [CCMenuItemFontWithStroke itemWithString:@"Tell a friend" block:^(id sender) {

#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"Tell a friend"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"Tell a friend"];

#endif
            
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];

            [app tellAFriend];
        }];
        
        CCMenuItemFontWithStroke *itemEmail = [CCMenuItemFontWithStroke itemWithString:@"Send Feedback" block:^(id sender) {
#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"Send Feedback"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"Send Feedback"];

#endif

            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            NSString *mailurl=[NSString stringWithFormat:
                               @"mailto:%@?subject=%@%@&body=%@%@",@"contact@alhogames.com",
                               [NSString stringWithFormat:@"Super Happy Fun Time Saga v%@",                                
                                [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]],
                               @"",@"",@""];
            [[UIApplication sharedApplication] openURL:
             [NSURL URLWithString:[mailurl stringByAddingPercentEscapesUsingEncoding:
                                   NSUTF8StringEncoding]]];
            
        }];
        
        
        CCMenuItemFontWithStroke *itemHelp = [CCMenuItemFontWithStroke itemWithString:@"Tutorial" block:^(id sender) {
#ifndef ANDROID
            [app.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                      action:@"button_press"
                                                                       label:@"Tutorial"
                                                                       value:nil] build]];
#else
//            [[DAGoogleAnalytics defaultTracker] sendEventByCategory:@"ui_action"
//                                                             action:@"button_press"
//                                                              label:@"Tutorial"];

#endif
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:@"virgin"];
            [defaults setInteger:kGameModeLevels forKey:@"mode"];
            [defaults synchronize];
            
            app.currentLeaderBoard = kLeaderboardLevels_v1;
            
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Choose];
            
            [[CCDirector sharedDirector] pushScene:[CCTransitionJumpZoom transitionWithDuration:1.0 scene:[ClassicGameScene scene]]];
            
        }];
        
        CCMenuItemFontWithStroke *itemBack = [CCMenuItemFontWithStroke itemWithString:@"Back" color:ccWHITE  block:^(id sender) {
            
            [[SimpleAudioEngine sharedEngine] playEffect:kSFX_Back];
            [[CCDirector sharedDirector] popSceneWithTransition:[CCTransitionJumpZoom class] duration:1.0];

            
        }];
        itemBack.color = ccBLACK;
        
        itemTell.color = ccRED;
        itemHelp.color = ccGREEN;
        itemReview.color = ccBLUE;
        itemEmail.color = ccORANGE;
        
        CCMenu *menu;
#ifdef ANDROID
        menu = [CCMenu menuWithItems:itemHelp, itemReview, itemEmail, itemBack, nil];
#else
        menu = [CCMenu menuWithItems:itemTell, itemHelp, itemReview, itemEmail, itemBack, nil];
#endif
        
		[menu alignItemsVerticallyWithPadding:10*kScale];
		[menu setPosition:ccp( size.width/2, size.height/2 - 100*kScale)];
		
		// Add the menu to the layer
		[self addChild:menu];

    }
    
    return self;
}


-(void)review
{

    
    
    
    NSString *templateReviewURL = @"http://itunes.apple.com/app/appName/idAPP_ID?mt=8&ls=1";
    NSString* appid = @"874705889";
    
    
    NSString *reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%@", appid]];
    
#ifdef ANDROID
    reviewURL = [NSString stringWithFormat:@"market://details?id=%@",@"com.alhogames.shfts"];
    
#endif
    
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: reviewURL]];
}

@end
