//
//  ClassicGameScene.m
//  Chainz
//
//  Created by Pedro Gomes on 12/13/11.
//  Copyright (c) 2011 Phluid Labs. All rights reserved.
//

#import "ClassicGameScene.h"
#import "GameBoard.h"
#import "SimpleAudioEngine.h"



//#define kBannerAdUnitID (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? @"6b31940cd92b493f" : @"aaa39480dd4c4180")

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
@implementation ClassicGameScene

@synthesize state = _state;

#pragma mark - Class Methods

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
+ (CCScene *)scene
{
	CCScene *scene = [CCScene node];
	ClassicGameScene *layer = [ClassicGameScene node];
	[scene addChild:layer];
	
	return scene;
}

#pragma mark - Dealloc and Initialization

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//- (void) dealloc
//{
//	[_gameboard release];
//	[super dealloc];
//}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (id)init
{
	if((self = [super init])) {
		_gameboard = [[GameBoard alloc] init];
		[_gameboard setContentSize:[[CCDirector sharedDirector] winSize]];
		[_gameboard resetGameBoard];
		[self addChild:_gameboard];
		[_gameboard setIsTouchEnabled:YES];

#ifndef ANDROID
        AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
        [app.tracker set:kGAIScreenName
                   value:@"Game"];
        
        [app.tracker send:[[GAIDictionaryBuilder createAppView] build]];
#else
//        [[DAGoogleAnalytics defaultTracker] sendAppView:@"Game"];
#endif
	}
	return self;
}

//- (void)adWhirlWillPresentFullScreenModal {
//    
//    if (self.state == kGameStatePlaying) {
//        
//        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
//        
//        AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//        if (!app.paused) {
//            [[CCDirector sharedDirector] pause];            
//        }
//    }
//}
//
//- (void)adWhirlDidDismissFullScreenModal {
//    
//    if (self.state == kGameStatePaused)
//        return;
//    
//    else {
//        self.state = kGameStatePlaying;
//        [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
//        AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//        if (!app.paused) {
//            [[CCDirector sharedDirector] resume];
//        }
//        
//    }
//}

//- (NSString *)adWhirlApplicationKey {
//    return @"77088e4dbc1448909e03b3661f3dbe42";//@"079d13ff447248438d5e01d50a870e03";
//}

//- (UIViewController *)viewControllerForPresentingModalView {
//    //return viewController;    
//    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//    return app.director;
//}

//-(void)adjustAdSize {
//	//1
//	[UIView beginAnimations:@"AdResize" context:nil];
//	[UIView setAnimationDuration:0.2];
//	//2
//	CGSize adSize = [adWhirlView actualAdSize];
//	//3
//	CGRect newFrame = adWhirlView.frame;
//	//4
//	newFrame.size.height = adSize.height;
//	
//   	//5 
//    CGSize winSize = [CCDirector sharedDirector].winSize;
//    //6
//	newFrame.size.width = winSize.width;
//	//7
//	newFrame.origin.x = (self.adWhirlView.bounds.size.width - adSize.width)/2;
//    
//    //8 
//	newFrame.origin.y = 0;//(winSize.height - adSize.height);
//	//9
//	adWhirlView.frame = newFrame;
//	//10
//	[UIView commitAnimations];
//}
//
//- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlVieww {
//    //1
//    //[adWhirlView rotateToOrientation:UIInterfaceOrientationLandscapeRight];
//	//2    
//    [self adjustAdSize];
//    
//}

-(void)onEnter {
    //1
    //    viewController = [(AppDelegate *)[[UIApplication sharedApplication] delegate] viewController];
    
//    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//
//    //2
//    self.adWhirlView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
//    //3
//    self.adWhirlView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
//    
//    //4
//    [adWhirlView updateAdWhirlConfig];
//    //5
//	CGSize adSize = [adWhirlView actualAdSize];
//    //6
//    CGSize winSize = [CCDirector sharedDirector].winSize;
//    //7
//    //	self.adWhirlView.frame = CGRectMake((winSize.width/2)-(adSize.width/2),winSize.height-adSize.height,winSize.width,adSize.height);
//    self.adWhirlView.frame = CGRectMake((winSize.width/2)-(adSize.width/2),0,winSize.width,adSize.height);
//    
//    
//    //8
//	self.adWhirlView.clipsToBounds = YES;
//    //9
//    //    [viewController.view addSubview:adWhirlView];
//    [app.director.view addSubview:adWhirlView];
//    
//    //10
//    //    [viewController.view bringSubviewToFront:adWhirlView];
//    [app.director.view bringSubviewToFront:adWhirlView];
    
    //11
    
//    CGPoint origin = CGPointMake(0.0,
//                                 0.0);
//    
//    // Use predefined GADAdSize constants to define the GADBannerView.
//    self.adBanner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait origin:origin];
//    
//    // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID before compiling.
//    self.adBanner.adUnitID = kBannerAdUnitID;
//    self.adBanner.delegate = self;
//    self.adBanner.rootViewController = app.director;
//    [app.director.view addSubview:self.adBanner];
//    [self.adBanner loadRequest:[self request]];
    
    [super onEnter];
}

-(void)onExit {
    //1
//    if (self.adBanner) {
//        [self.adBanner removeFromSuperview];
//        [self.adBanner setDelegate:nil];
//        self.adBanner = nil;
//    }
	[super onExit];
}


//#pragma mark GADRequest generation
//
//- (GADRequest *)request {
//    GADRequest *request = [GADRequest request];
//    
//    // Make the request for a test ad. Put in an identifier for the simulator as well as any devices
//    // you want to receive test ads.
//    request.testDevices = @[
//                            // TODO: Add your device/simulator test identifiers here. Your device identifier is printed to
//                            // the console when the app is launched.
//                            GAD_SIMULATOR_ID,
//                            @"fcb57f4edf39255821ad45abbe51ae2c"
//                            ];
//    return request;
//}
//
//#pragma mark GADBannerViewDelegate implementation
//
//// We've received an ad successfully.
//- (void)adViewDidReceiveAd:(GADBannerView *)adView {
//    //    NSLog(@"Received ad successfully");
//}
//
//- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
//    //    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
//}



@end
